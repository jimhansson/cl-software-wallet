;;;; cl-software-wallet.asd

(asdf:defsystem #:cl-software-wallet
  :description "Perfect toolbox for writing your own bitcoin client in lisp."
  :author "Jim Hansson <jim.hansson@gmail.com>"
  :license  "GPL-3"
  :version "0.0.1"
  :depends-on ("ironclad"
	       "nibbles"
	       "trivial-utf-8" ;; could we remove this dependency somehow
	       "secure-random" ;; and this one
	       "verbose"
	       "secp256k1"
	       "cl-annot"
	       "prove")
  :pathname "src"
  :serial t

  :defsystem-depends-on (:prove-asdf)
  :pathname "src"
  :components ((:module "sip-hash"
			:components ((:file "package")
				     (:file "sip-hash"))) 
	       (:file "package")
	       (:file "contracts")
	       (:file "utils")
	       (:file "hdkey-internals")
	       (:file "hdkey")
	       (:file "scripting-opcodes")
	       (:file "scripting")
	       (:file "transaction")
	       (:file "psbt")
	       (:file "base58")
	       (:file "wif")
	       (:file "bech32")
	       (:file "shamir-wordlist")
	       (:file "shamir")
	       (:file "mnemonic-english")
	       (:file "mnemonic-internals")
	       (:file "mnemonic")
               (:file "cl-software-wallet"))
  ;;:long-description #.(uiop:read-file-string (uiop:subpathname *load-pathname* "README.md"))
  :in-order-to ((asdf:test-op
		 (asdf:test-op :secp256k1
			       :cl-software-wallet/tests))))

(asdf:defsystem #:cl-software-wallet/tests

  :description "tests"
  :author "Jim Hansson <jim.hansson@gmail.com>"
  :license "GPL-3"
  :version "0.0.1"
  :defsystem-depends-on (:prove-asdf)
  :depends-on ("cl-software-wallet"
	       "prove")
  :pathname "tests"
  :components ((:file "package")
	       (:test-file "utils-tests")
	       (:test-file "other-tests")
	       (:test-file "contracts")
	       (:test-file "scripting-tests")
	       (:test-file "bip32-test-vectors")
	       (:test-file "bip39-test-vectors")
	       (:test-file "slip39-test-vectors")
	       (:file "psbt-tests-data")
	       (:test-file "psbt-tests")
	       (:test-file "sip-hash-tests")
	       (:test-file "bech32-tests"))
  :perform (asdf:test-op (op c)
			 (progn
			   (funcall (intern #.(string :run) :prove) c))))

;; not sure this works ok could also be a 9 instead of T as compression
(defmethod asdf:perform ((o asdf:image-op) (c asdf:system))
  (uiop:dump-image (asdf:output-file o c) :executable T :compression T))

(asdf:defsystem #:cl-software-wallet/simple-tool
  ;; this is placed here as an example on how i could create binaries from this system
  :description "simple-tool"
  :author "Jim Hansson <jim.hansson@gmail.com>"
  :license "GPL-3"
  :version "0.0.1"
  :depends-on ("cl-software-wallet"
	       "net.didierverna.clon")
  :pathname "util"
  :components ((:file "package")
	       (:file "simple-tool"))
  :build-operation "asdf:program-op"
  :build-pathname "simple-tool"
  :entry-point "cl-software-wallet/simple-tool:main")

(asdf:defsystem #:cl-software-wallet/shamir-tool
  :description "Shamir secret sharing tool to split secrets and recombine them"
  :author "Jim Hansson <jim.hansson@gmail.com>"
  :license "GPL-3"
  :version "0.0.1"
  :depends-on ("cl-software-wallet"
	       "net.didierverna.clon")
  :pathname "util"
  :components ((:file "package")
	       (:file "shamir"))
  :build-operation "asdf:program-op"
  :build-pathname "shamir"
  :entry-point "cl-software-wallet/shamir-tool:main")


