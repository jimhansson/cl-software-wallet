
(in-package #:cl-software-wallet)

(deftype byte-array (size)
  `(vector (unsigned-byte 8) ,size))

(deftype var-byte-array ()
  `(vector (unsigned-byte 8)))

(defun make-byte-array (size)
  "helper to not have to spell out type and initial arguments everytime"
  (make-array size
              :element-type '(unsigned-byte 8)
              :initial-element 0))

(defmacro concatenate-byte-array (&body body)
  "lets you concatenate a bunch of byte arrays without having to
specify the type that is returned."
  `(concatenate 'var-byte-array
		,@body))

(defun sha256sum-calculate (data &optional (only-last-four 't))
  (check-type data var-byte-array)
  (let ((arr (make-byte-array 32)))
    ;; first round
    (digest-sequence :SHA256 data :digest arr)
    ;; a second round
    (digest-sequence :SHA256 arr :digest arr)
    ;; only take the first 4 bytes of the checksum
    (if only-last-four
	(subseq arr 0 4)
	arr)))

(defun sha256sum-verify (data)
  "verify that data contains a double SHA256 checksum and that it is correct, that
checksum is the four last bytes"
  (check-type data var-byte-array)
  (let ((start-of-checksum (- (length data) 4)))
    (let (;; the checksum  (last 4 bytes)
	  (checksum-in-data (subseq data start-of-checksum))
	  ;; what we have calculated the checksum to be
	  (derived-checksum (sha256sum-calculate (subseq data 0 start-of-checksum))))
      (equalp checksum-in-data derived-checksum))))

(defun sha256sum-append (data)
  "returns data with a double SHA256 checksum appended, it adds 4 bytes"
  (check-type data var-byte-array)
  (concatenate `(vector ,(array-element-type data))
               data
               (sha256sum-calculate data)))


(defun hex-string-to-octets (hex)
  (ironclad:hex-string-to-byte-array hex))

(defun octets-to-string (octets)
  ;;(check-type octets var-byte-array)
  (map 'string #'code-char octets))

(defun pprint-octets (stream object &optional colon-p at-sign-p)
  "Can be called in format calls to print byte arrays in a nice way

You need to write something like this:
  ~/cl-software-wallet:pprint-octets/ in the format string."
  (declare (ignore colon-p at-sign-p))
  (format stream (octets-to-string object)))

(defun octets-to-hex-string (octets)
  ;;(check-type octets var-byte-array)
  (labels ((to-hex (x) ; need to zero fill.
             (format nil "~2,'0X" x)))
    (apply #'concatenate 'string
           (map 'list #'to-hex octets))))

(defun pprint-octets-hex (stream object &optional colon-p at-sign-p)
  "Can be called in format calls to print byte arrays in a nice way

You need to write something like this:
  ~/cl-software-wallet:pprint-octets-hex/ in the format string."
  (declare (ignore colon-p at-sign-p))
  (format stream (octets-to-hex-string object)))

(defun hash160 (seq)
  "bitcoins famous double hash of RIPEMD-160 and SHA256"
  (digest-sequence :RIPEMD-160 (digest-sequence :SHA256 seq)))


;; TODO: more to add here.
(defconstant +bitcoin-mainnet-id-byte+ #x00)
(defconstant +bitcoin-testnet-id-byte+ #x6f)
(defconstant +namecoin-network-id-byte+ #x34)

(defun read-compact-integer (buf &optional (start 0))
  "reads a bitcoin compact integer. this is usually used just before
encoding arrays and other variable size elements in the. the main
return value is the integer, the second value is the number of bytes
consumed by that integer in buf.
protocol. https://en.bitcoin.it/wiki/Protocol_documentation#Variable_length_integer"
  (let ((first-byte (aref buf start)))
    (cond ((= first-byte #xFD)
	   (values (ub16ref/le buf (+ start 1))
		   3))
	  ((= first-byte #xFE)
	   (values (ub32ref/le buf (+ start 1))
		   5))
	  ((= first-byte #xFF)
	   (values (ub64ref/le buf (+ start 1))
		   9))
	  (t (values first-byte
		     1)))))

(defun serialize-compact-integer (integer &optional (big-endian nil))
  "https://en.bitcoin.it/wiki/Protocol_documentation#Variable_length_integer, "
  (flet ((prepend-byte (byte vec)
	   (concatenate '(vector (unsigned-byte 8)) `#(,byte) vec)))
    (cond ((< integer #xFD)
	   ;; need the n-bits or else the number 0 will end up being an empty vector
	   (integer-to-octets integer :n-bits 8 :big-endian big-endian))
	  ((<= integer #xFFFF)
	   (prepend-byte #xFD (integer-to-octets integer :big-endian big-endian)))
	  ((<= integer #xFFFFFFFF)
	   (prepend-byte #xFE (integer-to-octets integer :big-endian big-endian)))
	  (t ; TODO need to make sure it is something that will fit in 8 bytes
	   (prepend-byte #xFF (integer-to-octets integer :big-endian big-endian))))))

(defun publickey->address (pk &optional (network-id +bitcoin-mainnet-id-byte+))
  (base58-encode ; encode
   (sha256sum-append ; add checksum
    (concatenate '(vector (unsigned-byte 8)) ; add network byte
		 `#(,network-id)
		 (hash160 pk)))))

(defun shl (x width bits)
  "Compute bitwise left shift of x by 'bits' bits, represented on 'width' bits"
  (logand (ash x bits)
          (1- (ash 1 width))))

(defun shr (x width bits)
  "Compute bitwise right shift of x by 'bits' bits, represented on 'width' bits"
  (logand (ash x (- bits))
          (1- (ash 1 width))))

(defun rotl (x width bits)
  "Compute bitwise left rotation of x by 'bits' bits, represented on 'width' bits"
  (logior (logand (ash x (mod bits width))
                  (1- (ash 1 width)))
          (logand (ash x (- (- width (mod bits width))))
                  (1- (ash 1 width)))))

(defun rotr (x width bits)
  "Compute bitwise right rotation of x by 'bits' bits, represented on 'width' bits"
  (logior (logand (ash x (- (mod bits width)))
                  (1- (ash 1 width)))
          (logand (ash x (- width (mod bits width)))
                  (1- (ash 1 width)))))

(defun bit-vector->integer (bit-vector)
  "Create a positive integer from a bit-vector."
  (reduce #'(lambda (first-bit second-bit)
              (+ (* first-bit 2) second-bit))
          bit-vector))

(defun integer->bit-vector (integer &optional min-width)
  "Create a bit-vector from a positive integer."
  (labels ((integer->bit-list (int &optional accum)
             (cond ((> int 0)
                    (multiple-value-bind (i r) (truncate int 2)
                      (integer->bit-list i (push r accum))))
                   ((null accum) (push 0 accum))
                   (t accum))))
    (let ((result (coerce (integer->bit-list integer) 'bit-vector)))
      (if (and min-width (< (length result) min-width))
	  (concatenate 'bit-vector
		       (make-array (- min-width (length result))
				   :initial-element 0
				   :element-type 'bit)
		       result)
	  result))))

(defun convert-bits (bits to &optional (fill 'nil))
  "subdivides a bunch of bits into groups, wonder where this is used"
  (let* ((number-of-full-groups (floor (/ (length bits) to)))
         (number-of-groups (+ number-of-full-groups (if fill 1 0)))
         (rest (mod (length bits) to)))
    (loop for i from 1 to number-of-groups
       ;; could use a displaced array instead of subseq
       collect (subseq bits
                       (* to (- i 1))
                       (* to i)))))



