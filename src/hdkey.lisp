
(in-package #:cl-software-wallet.hdkey)

;; we are still developing this, prefer to have easy to debug.
(declaim (optimize (debug 3)))


(defun mainnet-key? (hdkey)
  "Is it a mainnet key"
  (check-type hdkey hdkey)
  (member (hdkey-type hdkey) mainnet-key-types))

(defun testnet-key? (hdkey)
  "Is it a testnet key"
  (check-type hdkey hdkey)
  (member (hdkey-type hdkey) testnet-key-types))

(defun public-key? (hdkey)
  "Is it a public HD key"
  (check-type hdkey hdkey)
  (member (hdkey-type hdkey) public-key-types))

(defun private-key? (hdkey)
  "is it a private HD key"
  (check-type hdkey hdkey)
  (member (hdkey-type hdkey) private-key-types))

;; setup different key-types
(declare-key-types bitcoin  #x0488ADE4 #x0488B21E #x04358394 #x043587CF)
(declare-key-types dogecoin #x02FAC398 #x02FACAFD #x0432A243 #x0432A9A8)
(declare-key-types litecoin #x019D9CFE #x019DA462 #x0436EF7D #x0436F6E1)
(declare-key-types jumbucks #x037A6460 #x037A689A)

(deftype key ()
  "Represents a key, depending on if the key is private or public it
will be of different sizes. a private key will be 32 bytes long and
public will be 33 bytes long."
  `(or (byte-array ,+private-key-length+)
       (byte-array ,+public-key-length+)))

(deftype CC ()
  "Represents the chain-code used in the BIP32"
  `(byte-array ,+chain-code-length+))

(deftype child-idx ()
  "the index use when deriving keys from a HDKEY in BIP32 is only
defined up to 32 bits and cant be negative. This type represent that
limit."
  `(integer 0 ,+max-child-idx+))

(defstruct hdkey
  "Represent a node in the tree of keys in the HD wallet. Everything is
(read-only t), because all the operations on this struct will create a
new struct, think of this as a sort of immutable data."
  ;; Should be one of the magic numbers that are defined as a constant.
  (type 0 :read-only t :type integer) ; maybe should use a symbol here instead
  ;; Should be only positive integer
  (depth 0 :read-only t :type (integer 0 255)) ; will we need more levels
  ;; A fingerprint of a key is a non-unique hash of a key. Should be 4 bytes.
  (master-fingerprint +master-fingerprint+ :read-only t :type (byte-array 4))
  ;; This is the child-idx used on the parent to create this key.
  (child-number 0 :read-only t)
  (chain-code 0 :read-only t :type CC)
  (key 0 :read-only t :type key))


(defmethod print-object ((obj hdkey) stream)
  (print-unreadable-object (obj stream :type t)
    (format stream "type: ~a master-fingerprint: ~a~:@_ depth: ~a child-number: ~a~:@_"
	    (hdkey-type obj) (octets-to-hex-string (hdkey-master-fingerprint obj)) (hdkey-depth obj) (hdkey-child-number obj))
    (format stream " chain-code: ~a~:@_" (octets-to-hex-string (hdkey-chain-code obj)))
    (format stream " key: ~a" (octets-to-hex-string (hdkey-key obj)))))
  
(defun getpublickey (hdkey)
  "Uses the secp256k1 library to create a public key of a private
key. this function works on hdkey"
  (if (private-key? hdkey)
      (getpublickey* (hdkey-key hdkey))
      (hdkey-key hdkey)))

(defun getpublickey* (key)
  "Uses the secp256k1 library to create a public key of a private
key. this function works on strings and not hdkey"
  (assert (= (length key) +PRIVATE-KEY-LENGTH+))
  (with-context (ctx +context-sign+)
    (let ((pubkey (ec-pubkey-create ctx key)))
      (coerce (ec-pubkey-serialize ctx pubkey +ec-compressed+)
              '(byte-array 33)))))


(defun derive (hdkey index)
  "derives a child key, works on both private and public keys. If you
give it a private key it will return a private key, if you give it a
public key it will return a public key."
  (check-type hdkey hdkey)
  (if (public-key? hdkey)
      (derive-public hdkey index)
      (derive-private hdkey index)))

(defun fingerprint (key)
  "Calculate a fingerprint of a key. This is not unique, so be
prepared to handle conflicts, where two keys have the same
fingerprint. useful as a hash function for a hash-table containing
many keys."
  (subseq (hash160 (getpublickey key)) 0 4))

(defun derive-private (hdkey idx)
  "derives a child key, will only work on private keys"
  (declare (type child-idx idx))
  (assert (< idx +max-child-idx+))
  (assert (private-key? hdkey))
  (with-context (ctx +context-sign+)
    (flet ((hardened-idx? (idx)
             (< idx (expt 2 31))))
      (let* ((key (hdkey-key hdkey))
             (cc (hdkey-chain-code hdkey))
             (hash (bip32-hash cc idx (if (hardened-idx? idx)
                                          (getpublickey* key)
                                          (concatenate '(byte-array 33) #(#x0) key)))))
        (let ((child-cc (subseq hash 32 64))
              (child-key (ec-privkey-tweak-add ctx key hash)))
          (assert (= 32 (length child-key)))
          (make-hdkey :type (hdkey-type hdkey)
                      :depth (+ 1 (hdkey-depth hdkey))
                      :master-fingerprint (fingerprint hdkey)
                      :child-number idx
                      :chain-code child-cc
                      :key (coerce child-key '(byte-array 32))))))))

(defun derive-public (hdkey idx)
  "derives a child key, will only work on public keys"
  (declare (type child-idx idx))
  (assert (< idx +max-child-idx+))
  (assert (public-key? hdkey))
  (when (> idx (expt 2 31))
    (error "we will not allow you to derive a public key from a hardened public key"))
  (with-context (ctx +context-verify+)
    (context-set-illegal-callback ctx 'illegal-callback 'nil)
    (let* ((key (hdkey-key hdkey))
           (cc (hdkey-chain-code hdkey))
           (hash (bip32-hash cc idx key))
           (pubkey (ec-pubkey-parse ctx key)))
      (let ((child-cc (subseq hash 32 64))
            (child-key (ec-pubkey-tweak-add ctx pubkey hash)))
        (make-hdkey :type (hdkey-type hdkey)
                    :depth (+ 1 (hdkey-depth hdkey))
                    :master-fingerprint (fingerprint hdkey)
                    :child-number idx
                    :chain-code child-cc
                    :key (getpublickey* child-key))))))

(defun private->public (hdkey)
  "For a private key, returns the public key"
  (assert (private-key? hdkey))
  (make-hdkey :type (private->public-key-type (hdkey-type hdkey))
              :depth (hdkey-depth hdkey)
              :master-fingerprint (hdkey-master-fingerprint hdkey)
              :child-number (hdkey-child-number hdkey)
              :chain-code (hdkey-chain-code hdkey)
              :key (getpublickey* (hdkey-key hdkey))))

(defun hardened (idx)
  "Lets you specify a hardened key index, thi is really just setting
the highest bit."
  (+ idx (expt 2 31)))

(defun derive-by-path (hdkey &rest path)
  "Given a list of integers as &rest parameter will walk down the tree
and derive keys all the way down to leaf node, and return that."
  (check-type hdkey hdkey)
  (let ((current hdkey))
    (dolist (idx path current)
      (setq current (derive current idx)))))


(defun parse-hdkey-bytes (bytes)
  "deserilize a hdkey from a byte representation that is not base58 encoded and does not have checksum"
  (let ((net-and-type (ub32ref/be bytes 0))
	(depth (aref bytes 4))
	(mstr-fingerprint (integer-to-octets (ub32ref/be bytes 5) :n-bits 32))
	(child-number (ub32ref/be bytes 9))
	(chain-code (subseq bytes 13 45))
	(key (subseq bytes 45 78))) ;; read 33 for both public and private
    (when (member net-and-type private-key-types)
      ;; there should be a null byte there if its a private key, that we don't want.
      (assert (= 0 (aref key 0)))
      (setq key (subseq key 1)))
    (make-hdkey :type net-and-type
		:depth depth
		:master-fingerprint mstr-fingerprint
		:child-number child-number
		:chain-code chain-code
		:key key)))

(defun parse-hdkey (key)
  "deserilize a hdkey from a string, according to the format in BIP32"
  (check-type key string)
  (let ((bytes (base58-decode key)))
    (assert (sha256sum-verify bytes))
    (parse-hdkey-bytes bytes)))

(defun write-hdkey-bytes (hdkey)
  "does not include the checksum"
  (concatenate '(vector (unsigned-byte 8))
	       (integer-to-octets (hdkey-type hdkey) :n-bits 32 :big-endian t)
	       (integer-to-octets (hdkey-depth hdkey) :n-bits 8 :big-endian t)
	       (hdkey-master-fingerprint hdkey)
	       (integer-to-octets (hdkey-child-number hdkey) :n-bits 32 :big-endian t)
	       (hdkey-chain-code hdkey)
	       (when (private-key? hdkey)
		 ;; a private key is 32 bytes in short format. but a public key is 33
		 ;; bytes long. we will add a extra byte here to make the same length.
		 (integer-to-octets 0 :n-bits 8 :big-endian t))
	       (hdkey-key hdkey)))

(defun write-hdkey (hdkey)
  "serilize a hdkey into a string, according to the format specified
by BIP32"
  (check-type hdkey hdkey)
  (assert (= 4 (length (hdkey-master-fingerprint hdkey))))
  (base58-encode
   (sha256sum-append
    (write-hdkey-bytes hdkey))))

(defun from-seed (seed &key (type +bitcoin-testnet-private+))
  "takes a sequence of bytes (unsigned-byte 8) as a seed and generates
a private key from that. type needs to be of one of the private types"
  (unless (member type private-key-types)
    (error "There is no good reason to create a public key from a seed!"))
  (let* ((key (ascii-string-to-byte-array "Bitcoin seed"))
         (mac (ironclad:make-hmac key 'ironclad:SHA512)))
    (update-hmac mac seed)
    (let* ((I (hmac-digest mac))
           (Il (subseq I 0 32))
           (Ir (subseq I 32 64)))
      (with-context (ctx +context-verify+)
        (ec-seckey-verify ctx Il))
      (make-hdkey :type type
                  :depth 0
                  :master-fingerprint +master-fingerprint+
                  :child-number 0
                  :chain-code Ir
                  :key Il))))

