
(in-package :cl-software-wallet.mnemonic)


(defparameter *wordlist* +english-wordlist+
  "The current wordlist used for generating and verifying
  mnemonics. at this moment we only have an english wordlist but will
  could be easily in the future.")

(defparameter *entropy-to-mnemonic-size-map*
  (pairlis '(128 160 192 224 256)
	   '(12  15  18  21  24))
  "maps between entropy-size and mnemonic-size")

(defparameter *entropy-to-checksum-size-map*
  (pairlis '(128 160 192 224 256)
	   '(4   5   6   7   8))
  "maps between entropy-size and mnemonic-size")

(defparameter *mnemonic-length-bits-size-map*
  (pairlis (map 'list #'cdr *entropy-to-mnemonic-size-map*)
	   (map 'list
		(lambda (x)
		  (+ (car (assoc x *entropy-to-checksum-size-map*))
		     (cdr (assoc x *entropy-to-checksum-size-map*))))
		(map 'list #'car *entropy-to-mnemonic-size-map*))))

(defun entropy-to-mnemonic-size (size)
  (cdr (assoc size *entropy-to-mnemonic-size-map*)))

(defun mnemonic-to-entropy-size (size)
  (car (rassoc size *entropy-to-mnemonic-size-map*)))

(defun mnemonic-word-to-index (mnemonic-word)
  (position mnemonic-word *wordlist* :test #'string=))

(defun entropy-to-bit-string (entropy entropy-size)
  (assert (entropy-to-mnemonic-size entropy-size))
  (let ((format-str (format nil "~~~A,'0B" entropy-size)))
    (format nil format-str entropy)))

(defun checksum-to-bit-string (checksum entropy-size)
  (assert (entropy-to-mnemonic-size entropy-size))
  (subseq (format nil "~256,'0B" (byte-array-to-int checksum))
          0 (/ entropy-size 32)))

(defun generate-initial-entropy (entropy-size)
  "The default function used for generating the entropy needed by
generate-mnemonic. Not really sure how good this is but it should be
better than the standard random functions, need to study this more in
depth."
  (assert (entropy-to-mnemonic-size entropy-size))
  (secure-random:bytes (bits-to-bytes entropy-size)
		       secure-random:*generator*))


(defun generate-mnemonic (&key (entropy-size 128)
                            (entropy-function #'generate-initial-entropy))
  "Creates a new mnemonic phrase, it will use the current wordlist. It
takes two arguments, the first one is the entropy size, it needs to be
between 128 and 256(inclusive) in steps of 32. the seconds argument is
the function used to generate the entropy, it should be a one argument
function that will be called with the entropy-size"
  (assert (entropy-to-mnemonic-size entropy-size))
  (let* (;; generate entropy and checksum of entropy
	 (entropy (funcall entropy-function entropy-size))
	 (checksum (sha256 entropy))
	 ;; we convert to strings with ones and zeros because leading
	 ;; zeros are important in this, using bitshifts and other
	 ;; stuff could lead us into problems with that.
         (ent-bit-string (entropy-to-bit-string entropy entropy-size))
         (cs-bit-string (checksum-to-bit-string checksum entropy-size))
	 ;; combine them both into one string, easy when they are
	 ;; strings and not numbers
         (ent+cs-bit-string (concatenate 'string
					 ent-bit-string
					 cs-bit-string)))
    ;; in the combined bit-string every 11 bits represent an index
    ;; into the word-list, so this devides the bit-string into parts
    ;; of 11 bits and lets us get those bits as a integer.
    (flet ((read-word-index-from-bit-string (bit-string index)
	     (parse-integer ent+cs-bit-string
			    :start (* index 11)
			    :end (* (+ index 1) 11)
			    :radix 2)))
      (loop :for i :below (/ (length ent+cs-bit-string) 11)
	 ;; collect the words from the word-list
	 :collect (aref *wordlist*
			(read-word-index-from-bit-string
			 ent+cs-bit-string i))
	 :into mnemonic-list
	 :finally (return mnemonic-list)))))


(defun check-words (mnemonic-list)
  (not (member nil (mapcar #'mnemonic-word-to-index mnemonic-list))))


;; this can both return false and throw errors, it should only do one of them.
(defun mnemonic? (mnemonic-list)
  "checks a list of words if they all belong to the current wordlist,
it also checks that the checksum word is correct."
  (and (mnemonic-to-entropy-size (length mnemonic-list))
       (check-words mnemonic-list)
       (let* ((ent+cs-bit-string
               (format nil "~{~11,'0B~}"
                       (mapcar #'mnemonic-word-to-index mnemonic-list)))
              (entropy
               (integer-to-octets 
                (parse-integer ent+cs-bit-string
                               :radix 2
                               :start 0
                               :end (cs-position ent+cs-bit-string))
                :n-bits (mnemonic-to-entropy-size (length mnemonic-list))))
	      (cs-bit-string (subseq ent+cs-bit-string
                                     (cs-position ent+cs-bit-string))))
	 (string= cs-bit-string
                  (checksum-to-bit-string (sha256 entropy)
                                          (cs-position ent+cs-bit-string))))))
(defun mnemonic->ints (mnemonic-list)
  ;; replace this with mnemonic-word-to-index or the reversed
  (let ((ints (map 'list #'mnemonic-word-to-index mnemonic-list)))
    (assert (notany  #'null ints) nil "There was a invalid word in the mnemonic phrase")
    ints))

;(convert-to-entropy '("abandon" "abandon" "abandon" "abandon" "abandon" "abandon" "abandon" "abandon" "abandon" "abandon" "abandon" "about"))

(defun convert-to-entropy (mnemonic-list)
  "BAD FUNCTION, you should never go that way"
  
  ;; look up all words and collect their index
  (let* ((ints (mnemonic->ints mnemonic-list))
	 ;; convert those indexs into bits
	 (bits (apply #'concatenate 'bit-vector
		      (map 'list
			   (lambda (x)
			     (integer->bit-vector x 11))
			   ints)))
	 (bytes (integer-to-octets ;; rename entropy
		 (parse-integer (format nil "~{~11,'0B~}" ints)
				:radix 2
				:start 0
				:end (car (rassoc (length mnemonic-list) *entropy-to-mnemonic-size-map*)))
		 :n-bits (mnemonic-to-entropy-size (length mnemonic-list))))
	 (hash (ironclad:digest-sequence :sha256 bytes)))

    ;; make sure we have the correct number of bits.
    (assert (= (length bits)
	       (cdr (assoc (length mnemonic-list)
			   *mnemonic-length-bits-size-map*))))
    ;; check checksum bits
    (assert (equalp (subseq bits (mnemonic-to-entropy-size (length mnemonic-list)))
		    (subseq (integer->bit-vector (aref hash 0) 8)
			    0 (cdr (assoc (mnemonic-to-entropy-size (length mnemonic-list))
					  *entropy-to-checksum-size-map*)))))
    bytes))

(defun convert-to-seed (mnemonic-list &optional (password nil password-p))
  "Takes a list of words and if they are a mnemonic phrase it will
convert them into a seed, that could be used for a BIP32 wallet.

Because of the hashing done on the list of words, we cant go from
  entropy back to mnemonic words. So there does not exist any reverse
  function to this."
  (assert (mnemonic? mnemonic-list))
  (flet ((flatten-to-utf8 (list)
       	   (trivial-utf-8:string-to-utf-8-bytes (format nil "~{~a~^ ~}" list))))
    (ironclad:byte-array-to-hex-string
     (ironclad:pbkdf2-hash-password
      (flatten-to-utf8 mnemonic-list)
      :salt (trivial-utf-8:string-to-utf-8-bytes
	     (if password-p
		 (concatenate 'string "mnemonic" password)
		 "mnemonic"))
      :digest :sha512
      :iterations 2048))))
