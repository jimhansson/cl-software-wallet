(in-package #:cl-software-wallet.script)


(defun raw-script? (list)
  (let ((ops (map 'list
		  (lambda (x) (car x))
		  opcodes)))
    (every (lambda (x)
	     (or (member x ops)
		 (arrayp x)))
	   list)))

(deftype raw-script ()
  `(or null (satisfies raw-script?)))

(deftype binary-script ()
  `(vector (unsigned-byte 8)))

(defclass <script> ()
  ())

(defclass <p2sh-script> (<script>)
  ((script-hash :initarg :script-hash)))

(defclass <multisig-script> (<script>)
  ((needed :initarg :needed)
   (of :initarg :of)))

(defmethod print-object ((script <multisig-script>) stream)
  (print-unreadable-object (script stream :type t)
    (flet ((to-hex (bytes) (ironclad:byte-array-to-hex-string bytes))
	   (fingerprint (bytes) (subseq bytes 0 4)))
      (with-slots (needed of) script
	(format stream "needed: ~a, of: ~{~a~^, ~}"
		needed
		(map 'list #'to-hex
		     (map 'list #'fingerprint of)))))))


(defun script? (script)
  (or (subtypep (type-of script) '<script>)
      (eq (type-of script) 'raw-script)
      (eq (type-of script) 'binary-script)))



(defmethod to-bytes ((script <script>))
  (error "generic method called")
  (v:info '(:cl-software-wallet :script) "Fixme"))

(defmethod to-bytes :before (something)
  (v:trace '(:cl-software-wallet :scripting) "to-bytes ~a" something))

(defmethod to-bytes ((script <p2sh-script>))
  (with-slots (script-hash) script
    (serialize-p2sh script-hash)))

(defmethod to-bytes ((script <multisig-script>))
  (with-slots (needed of) script
    (serialize-multisig needed of)))

(defvar opcodes-lookup (make-hash-table))

(loop for x across opcodes
   do (setf (gethash (car x) opcodes-lookup) x))

(defvar opcodes-byte-lookup (make-hash-table))

(loop for x across opcodes
   do (setf (gethash (nth 1 x) opcodes-byte-lookup) x))

(defun get-opcode-by-byte (byte)
  (gethash byte opcodes-byte-lookup))

(defun get-opcode-by-symbol (sym)
  (gethash sym opcodes-lookup))


(define-constant +lowest-pushdata-one+ 76)
(define-constant +lowest-pushdata-two+ 256)
(define-constant +lowest-pushdata-four+ (expt 2 16))
(define-constant +max-pushdata+ (expt 2 32))

(defun serialize-script-data (data)
  "depending on the length of the data we want in the script we need
to prepend it with different things. for those under a cetain length
we just add a byte that tells us the length directly for some longer
ones we use the OP_PUSHDATA variants. this method will serialize a
  peice of data in the correct way."
  
  (flet ((between (x l h)
	   ;; non-inclusive end
	   (<= l x (1- h)))
	 
	 (prepend-with-pushdata (pushdata-op n-bits data)
	   (concatenate 'list
			`#(,(cadr (get-opcode-by-symbol pushdata-op)))
			(ironclad:integer-to-octets (length data) :n-bits n-bits)
			data)))
    
    (let ((data-length (length data)))
      (cond ((between data-length 0 +lowest-pushdata-one+)
	     (let ((op-code (aref opcodes data-length)))
	       (concatenate 'list `#(,(cadr op-code)) data)))
	    
	    ((between data-length +lowest-pushdata-one+ +lowest-pushdata-two+)
	     (prepend-with-pushdata :OP_PUSHDATA1 8 data))
	    
	    ((between data-length +lowest-pushdata-two+ +lowest-pushdata-four+)
	     (prepend-with-pushdata :OP_PUSHDATA2 16 data))
	    
	    ((between data-length +lowest-pushdata-four+ +max-pushdata+)
	     (prepend-with-pushdata :OP_PUSHDATA4 32 data))
	
	    (t (error "can't handle that length in data"))))))


(defun serialize-script (script)
  "turns the list representing a script into the native binary
representation used in the protocol."
  (flet ((flatten (part)
	   (cond ((keywordp part)
		  (list (cadr (get-opcode-by-symbol part))))
		 ((arrayp part)
		  (serialize-script-data part)))))
    (coerce (mapcan #'flatten script) '(vector (unsigned-byte 8)))))


(defun serialize-p2pkh (pubkeyhash)
  "construct a standard p2pkh script in native binary format"
  ;; TOOD check that it is a pubkeyhash
  (serialize-script `(:OP_DUP :OP_HASH160 ,pubkeyhash :OP_EQUALVERIFY :OP_CHECKSIG)))

(defun serialize-p2pk (pubkey)
  "construct a standard p2pk script in native binary format"
  (serialize-script `(,pubkey :OP_CHECKSIG)))

(defun serialize-p2sh (hash)
  "construct a standard p2sh script in native binary format"
  (serialize-script `(:OP_HASH160 ,hash :OP_EQUAL)))

(defun serialize-multisig (needed of)
  "construct a multisig script in native binary format, of should be a
  list of all the keys"
  (serialize-script `(,(from-value (length of)) ,@of ,(from-value needed) :OP_CHECKMULTISIG)))

(defun parse-script (bytes)
  "Turns the binary script into a parsed tree structure we can execute
or that we can check or match against know structures."
  (let ((script '()))
    (do ((pos 0)
	 (end-pos (- (length bytes) 1)))
	((> pos end-pos))
      (let* ((byte (aref bytes pos))
	     (opcode (get-opcode-by-byte byte))
	     (sym (car opcode)))
	
	(unless opcode (error "could not find symbol for byte ~a" byte))

	(cond
	  ;; NA cases
	  ((and (>= byte 0) (<= byte #x4b))
	   (setq script (cons (subseq bytes (+ pos 1) (+ pos byte 1)) script))
	   (incf pos (+ byte 1)))

	  ;; pushdata cases
	  ((and (>= byte #x4c) (<= byte #x4e))
	   ;; some need a better namme
	   (let* ((diff (- byte #x4c))        ; 0 1 or 2
		  (some (expt 2 diff))        ; 1 2 or 4
		  (n-bits (* 8 some))         ; 8 16 or 32
		  (start-data (+ pos 1 some)) ; pos + 2 3 or 5
		  (size (ironclad:octets-to-integer bytes
						    :start start-data
						    :n-bits n-bits))
		  (end-data (+ start-data size))
		  (data (subseq bytes start-data end-data)))
	     (setq script (cons data script))
	     (incf pos (+ (1+ some) size))))			
					
	  ;; other operator case
	  (t
	   (setq script (cons sym script))
	   (incf pos)))))
    (coerce (reverse script) 'raw-script)))


(define-condition script-separator-warning (warning)
  ())

(define-condition script-error (error)
  ((operation :initarg :operation :reader operation))
  (:documentation "Represent an error in a script on a specific
  operation"))

(define-condition unknown-operation (script-error)
  ()
  (:documentation "We have found a operation that we don't know how to
  handle, you should handle this as a real problem and not continue."))

(define-condition disabled-operation (script-error)
  ()
  (:documentation "This condition exists to allow you to ignore the
  disabled status of opcodes, and be able to execute scripts that
  contain those opcodes. Some of the early blocks contains opcodes
  that are today disabled."))

(define-condition overflow-on-operation (script-error)
  ((a-arg :initarg :a-arg :reader a-arg)
   (b-arg :initarg :b-arg :reader b-arg))
  (:documentation "Signaled by 's32-op when it detects a
  over/underflow, normally you would let the code continue but when
  developing your own scripts it could be good not to have it allow
  this wrap-around and instead maybe rethink you script."))


(defun s32-op (op a b)
  "Implements signed 32 bits math will signal a condition but allow
for wrapping around, it will also convert result of operations that
returns bools into 1 and 0. you could say this implements the rules of
math in bitcoin."
  (let ((result (funcall op a b))
	(max-int (- (expt 2 31) 1))
	(min-int (- (expt 2 31))))
    (cond
      ((eq result t) 1)
      ((eq result nil) 0)
      ((> result max-int)
       (restart-case (error 'overflow-on-operation :a-arg a :b-arg b)
	 (continue () :report "Ignore overflow (wrap around)"
		   (+ min-int (- result max-int)))
	 (use-value (value) value)))
      ((< result min-int)
       (restart-case (error 'overflow-on-operation :a-arg a :b-arg b)
	 (continue () :report "Ignore underflow (wrap around)"
		   (- max-int (+ result min-int)))
	 (use-value (value) value)))
      (t result))))

(defun execute/step (next stack alt-stack verify-status)
  "takes a state and returns a new state after executing one step."
  
  (macrolet ((disabled-but-restartable (op)
	       ;; small macro to mark ops as disabled but restartable.
	       `(restart-case (error 'disabled-operation :operation ,op)
		  (continue () :report "Execute operation anyway."))))
    
    (flet ((find-endif-and-else (script)
	     ;; returns a copy of the script upto else including the
	     ;; else, and sublist from else and end of if. keeping
	     ;; track of levels of ifs.
	     (do ((next (cdr script) (cdr next))
		  (sub-ifs 0)
		  (upto-else '())
		  (else-found nil)
		  (end nil)
		  (else nil))
		 ((not (null end))
		  (values (nreverse upto-else) end else))

	       (unless else-found
		 (setq upto-else (cons (car next) upto-else)))
	       
	       (case (car next)
		 (:OP_IF
		  (incf sub-ifs))
		 (:OP_NOTIF
		  (incf sub-ifs))
		 (:OP_ELSE
		  (when (eq sub-ifs 0)
		    (setq else-found t)
		    (setq else next)))
		 (:OP_ENDIF
		  (when (eq sub-ifs 0)
		    (setq end next))
		  (decf sub-ifs)))))

	   ;; binary as in "it takes two arguments"
	   (binary-arith (op)
	     (let ((a (car stack))
		   (b (cadr stack))
		   (rest (cddr stack)))
	       (let ((result (s32-op op a b)))
		 (setq stack (cons result rest)
		       next (cdr next)))))

	   ;; for when I want to use a binary operation but one of the
	   ;; values are locked. things like add1 and similar ops
	   (binary-arith-fixed-b (op b)
	     (let ((a (car stack))
		   (rest (cdr stack)))
	       (setq stack (cons (s32-op op a b) rest)
		     next (cdr next)))))
      
      (let ((op (car next)))

	;; ordering of these could be of importance when it comes to
	;; speed, maybe we should have the offen used cases first?
	
	(cond ((arrayp op)
	       (setq stack (cons op stack))
	       (setq next (cdr next)))

	      ;; number operations
	      ((eq op :OP_0) (setq stack (cons 0 stack) next (cdr next)))
	      ((eq op :OP_1) (setq stack (cons 1 stack) next (cdr next)))
	      ((eq op :OP_2) (setq stack (cons 2 stack) next (cdr next)))
	      ((eq op :OP_3) (setq stack (cons 3 stack) next (cdr next)))
	      ((eq op :OP_4) (setq stack (cons 4 stack) next (cdr next)))
	      ((eq op :OP_5) (setq stack (cons 5 stack) next (cdr next)))
	      ((eq op :OP_6) (setq stack (cons 6 stack) next (cdr next)))
	      ((eq op :OP_7) (setq stack (cons 7 stack) next (cdr next)))
	      ((eq op :OP_8) (setq stack (cons 8 stack) next (cdr next)))
	      ((eq op :OP_9) (setq stack (cons 9 stack) next (cdr next)))
	      ((eq op :OP_10) (setq stack (cons 10 stack) next (cdr next)))
	      ((eq op :OP_11) (setq stack (cons 11 stack) next (cdr next)))
	      ((eq op :OP_12) (setq stack (cons 12 stack) next (cdr next)))
	      ((eq op :OP_13) (setq stack (cons 13 stack) next (cdr next)))
	      ((eq op :OP_14) (setq stack (cons 14 stack) next (cdr next)))
	      ((eq op :OP_15) (setq stack (cons 15 stack) next (cdr next)))
	      ((eq op :OP_16) (setq stack (cons 16 stack) next (cdr next)))

	      ;; equal
	      ((or (eq op :OP_EQUAL) (eq op :OP_EQUALVERIFY))
	       (let ((first (car stack))
		     (second (cadr stack))
		     (rest (cddr stack)))
		 (setq stack (cons (if (equalp first second) 1 0) rest))
		 (if (eq op :OP_EQUALVERIFY)
		     (setq next (cons :OP_VERIFY (cdr next)))
		     (setq next (cdr next)))))

	      ;; verify and return (these will mark the transaction as
	      ;; failed or valid.
	      ((eq op :OP_VERIFY)
	       ;; do the verify
	       (let ((first (car stack))
		     (rest (cdr stack)))
		 (if first
		     (setq verify-status :OK)
		     (setq verify-status :NOTOK))
		 (setq stack rest
		       next (cdr next))))

	      ((eq op :OP_RETURN)
	       (setq verify-status :NOTOK)
	       (setq next (cdr next)))


	      ((eq op :OP_IF)
	       (multiple-value-bind (upto-else endif else)
		   (find-endif-and-else next)
		 (if (eq (car stack) 1)
		     (setq next (nconc upto-else endif))
		     (if else
			 (setq next else)
			 (setq next endif)))
		 (setq stack (cdr stack))))
	      
	      ((eq op :OP_NOTIF)
	       (multiple-value-bind (upto-else endif else)
		   (find-endif-and-else next)
		 (if (not (eq (car stack) 1))
		     (setq next (nconc upto-else endif))
		     (if else
			 (setq next else)
			 (setq next endif)))
		 (setq stack (cdr stack))))
	      
	      ((eq op :OP_ELSE)
	       (setq next (cdr next)))
	      ((eq op :OP_ENDIF)
	       (setq next (cdr next)))
	      
	      ((eq op :OP_TOALTSTACK)
	       (setq alt-stack (cons (car stack) alt-stack)
		     stack (cdr stack)
		     next (cdr next)))
	      ((eq op :OP_FROMALTSTACK)
	       (setq stack (cons (car alt-stack) stack)
		     alt-stack (cdr alt-stack)
		     next (cdr next)))
	      ((eq op :OP_IFDUP)
	       (let ((top (car stack)))
		 (when (not (eq top 0))
		   (setq stack (cons top stack)))
		 (setq next (cdr next))))
	      ((eq op :OP_DEPTH)
	       (let ((depth (length stack)))
		 (setq stack (cons depth stack)
		       next (cdr next))))
	      
	      ((eq op :OP_DROP)
	       (setq stack (cdr stack)
		     next (cdr next)))
	      ((eq op :OP_DUP)
	       (setq stack (cons (car stack) stack)
		     next (cdr next)))
	      ((eq op :OP_NIP)
	       (setq stack (cons (car stack) (cddr stack))
		     next (cdr next)))
	      ((eq op :OP_OVER)
	       (setq stack (cons (cadr stack) stack)
		     next (cdr next)))
	      

	      ((eq op :OP_ADD) (binary-arith #'+))
	      ((eq op :OP_SUB) (binary-arith #'-))
	      ((eq op :OP_1ADD) (binary-arith-fixed-b #'+ 1))
	      ((eq op :OP_1SUB) (binary-arith-fixed-b #'- 1))

	      ((eq op :OP_MUL)
	       (disabled-but-restartable op)
	       (binary-arith #'*))
	      ((eq op :OP_DIV)
	       (disabled-but-restartable op)
	       (binary-arith #'/))
	      ((eq op :OP_MOD)
	       (disabled-but-restartable op)
	       (binary-arith #'mod))
	      ((eq op :OP_2MUL)
	       (disabled-but-restartable op)
	       (binary-arith-fixed-b #'* 2))
	      ((eq op :OP_2DIV)
	       (disabled-but-restartable op)
	       (binary-arith-fixed-b #'/ 2))
	      
	      ((eq op :OP_NEGATE)
	       (setq stack (cons (- (car stack)) (cdr stack))
		     next (cdr next)))
	      ((eq op :OP_ABS)
	       (setq stack (cons (abs (car stack)) (cdr stack))
		     next (cdr next)))
	      ((eq op :OP_NOT)
	       (if (eq (car stack) 0)
		   (setq stack (cons 1 (cdr stack)))
		   (setq stack (cons 0 (cdr stack))))
	       (setq next (cdr next)))
	      
	      ((eq op :OP_0NOTEQUAL)
	       (setq stack (cons (if (eq (car stack) 0) 0 1) (cdr stack))
		     next (cdr next)))
	      
	      ((eq op :OP_LSHIFT)
	       (disabled-but-restartable op)
	       (binary-arith (lambda (a b)
			       (cl-software-wallet::shl a 32 b))))
	      ((eq op :OP_RSHIFT)
	       (disabled-but-restartable op)
	       (binary-arith (lambda (a b)
			       (cl-software-wallet::shr a 32 b))))

	      ((eq op :OP_BOOLAND)
	       (binary-arith (lambda (a b) (and (not (eq a 0)) (not (eq b 0))))))
	      ((eq op :OP_BOOLOR)
	       (binary-arith (lambda (a b) (or (not (eq a 0)) (not (eq b 0))))))

	      ((eq op :OP_NUMEQUAL) (binary-arith #'=))
	      ((eq op :OP_NUMNOTEQUAL) (binary-arith (lambda (a b) (not (= a b)))))

	      ((eq op :OP_LESSTHAN) (binary-arith #'<))
	      ((eq op :OP_GREATERTHAN) (binary-arith #'>))
	      ((eq op :OP_LESSTHANOREQUAL) (binary-arith #'<=))
	      ((eq op :OP_GREATERTHANOREQUAL) (binary-arith #'>=))

	      ((eq op :OP_MIN) (binary-arith #'min))
	      ((eq op :OP_MAX) (binary-arith #'max))

	      ((eq op :OP_WITHIN)
	       (let ((x (car stack))
		     (min (cadr stack))
		     (max (caddr stack))
		     (rest (cdddr stack)))
		 (let ((res (if (and (<= min x)
				     (> max x))
				1
				0)))
		   (setq stack (cons res rest)
			 next (cdr next)))))
	      
	      ;; hashing operations
	      ((member op '(:OP_RIPEMD160 :OP_SHA1 :OP_SHA256 :OP_HASH160 :OP_HASH256))
	       (let ((input (car stack))
		     (rest (cdr stack))
		     (res '()))
		 (case op
		   (:OP_RIPEMD160
		    (setq res (ironclad:digest-sequence :RIPEMD160 input)))
		   (:OP_SHA1
		    (setq res (ironclad:digest-sequence :SHA1 input)))
		   (:OP_SHA256
		    (setq res (ironclad:digest-sequence :SHA256 input)))
		   (:OP_HASH160
		    (setq res (hash160 input)))
		   (:OP_HASH256
		    (setq res (sha256sum-calculate input 'nil))))
		 (setq stack (cons res (cdr stack))
		       next (cdr next))))
	      
	      ((or (eq op :OP_CHECKSIG) (eq op :OP_CHECKSIGVERIFY))
	       (let ((sig (car stack))
		     (pubkey (cadr stack))
		     (rest (cddr stack)))
		 (setq stack (cons 1 rest))
		 (if (eq op :OP_CHECKSIGVERIFY)
		     (setq next (cons :OP_VERIFY (cdr next)))
		     (setq next (cdr next)))))
	      
	      ((or (eq op :OP_CHECKMULTISIG) (eq op :OP_CHECKMULTISIGVERIFY))

	       (flet ((split (list count)
			(values (subseq list 0 count) (nthcdr count list))))

		 (let ((number-of-pk (car stack)))

		   ;; read PK:s
		   (multiple-value-bind (keys rest)
		       (split (cdr stack) number-of-pk)
		     (let (number-of-sigs (car rest))

		       ;; read SIG:s
		       (multiple-value-bind (sigs rest)
			   (split rest number-of-sigs)

			 ;; update stack
			 (setq stack rest)
			 
			 ;; TODO: check sigs against keys
			 		 
			 ;; consume one extra
			 (setq stack (cdr rest))
			 (if (eq op :OP_CHECKMULTISIGVERIFY)
			     (setq next (cons :OP_VERIFY (cdr next)))
			     (setq next (cdr next)))))))))

	      (t
	       (restart-case (error 'unknown-operation :operation op)
		 (continue () :report "Ignore operation, continue with next"
			   (setq next (cdr next)))))))))

  ;; return the new state.
  (values next stack alt-stack verify-status))

(defparameter *valid-sub-types-for-p2sh* '(:P2PK :P2PKH))

(defun execute (scripts &key (keep-all-steps nil) (max-iterations 10))
  "returns a list of two states, the first and the last, if
keep-all-steps is true it will also returns all the steps in between,
  meaning you get a complete step by step breakdown of the script execution."
  ;; keep calling execute/step until we have finished or reached
  ;; max-iterations
  (let ((script scripts))
    ;; loop over scripts, we shold have a restart point between
    ;; scripts. Should that restart allows us to reset the state of
    ;; the back to before the script? could that be done?
    
    (do ((i 0)
	 (j 0)
	 (result '())
	 (stack '())
	 (alt-stack '()))
	((or
	  (null script)
	  (> j max-iterations))
	 result)
      (let ((next (car script))
	    (verify-status nil))

	(setq result (cons (list next
				 stack
				 alt-stack
				 verify-status)
			   result))
	
	;; loop over single script
	(do ()
	    ((or
	      (null next)
	      (> j max-iterations)))
	  (multiple-value-bind (next-new stack-new alt-stack-new verify-status-new)
	      (execute/step next stack alt-stack verify-status)
	    (setq result
		  (cons (list next-new
			      stack-new
			      alt-stack-new
			      verify-status-new)
			result))
	    (setq next next-new)
	    (setq stack stack-new)
	    (setq alt-stack alt-stack-new)
	    (setq verify-status verify-status-new))
	  (v:debug '(:cl-software-wallet :scripting) "Stepping")
	  (incf j))

	(if (eq (script-type (car script)) :P2SH)
	    ;; we could use a signal to mark this transition.
	    (progn
	      ;; there are more conditions we should check here before
	      ;; accepting this as a P2SH transition.
	      (v:debug '(:cl-software-wallet :scripting) "Changing to redeem script")
	      (let* ((redeem-script-bytes (car (last (car scripts))))
		     (redeem-script (parse-script redeem-script-bytes)))
		(setq script (cons redeem-script (cdr script)))))
	    (setq script (cdr script)))
	
	(if (car script)
	    (setq result (cons :SCRIPT-CHANGE result)))
	(incf i)))))

(defun to-value (op)
  "Just turns OP_0-16 into values"
  ;;could be reused in execute/step
  ;; may need to be extended to convert more things to numbers
  (cond
    ((eq op :OP_0) 0)
    ((eq op :OP_1) 1)
    ((eq op :OP_2) 2)
    ((eq op :OP_3) 3)
    ((eq op :OP_4) 4)
    ((eq op :OP_5) 5)
    ((eq op :OP_6) 6)
    ((eq op :OP_7) 7)
    ((eq op :OP_8) 8)
    ((eq op :OP_9) 9)
    ((eq op :OP_10) 10)
    ((eq op :OP_11) 11)
    ((eq op :OP_12) 12)
    ((eq op :OP_13) 13)
    ((eq op :OP_14) 14)
    ((eq op :OP_15) 15)
    ((eq op :OP_16) 16)
    (t (error "Unhandled case in cl-software-wallet.script:to-value"))))

(defun from-value (value)
  "the reverse of to-value"
  (case value
    ((0) :OP_0)
    ((1) :OP_1)
    ((2) :OP_2)
    ((3) :OP_3)
    ((4) :OP_4)
    ((5) :OP_5)
    ((6) :OP_6)
    ((7) :OP_7)
    ((8) :OP_8)
    ((9) :OP_9)
    ((10) :OP_10)
    ((11) :OP_11)
    ((12) :OP_12)
    ((13) :OP_13)
    ((14) :OP_14)
    ((15) :OP_15)
    ((16) :OP_16)
    (otherwise (error "Unhandled case in cl-software-wallet.script:from-value"))))

(defun script-type (script)
  "Takes a parsed script and returns a symbol that represent the type
of script, if it is a type we dont know about it will return nil"
  (cond
    ((and
      (= (length script) 3)
      (eq (car script) :OP_HASH160)
      ;; should check lenght of cadr it should be 20 bytes
      (eq (caddr script) :OP_EQUAL))
     :P2SH)
    ((and
      ;; add more checks here.
      (eq (car (last script)) :OP_CHECKMULTISIG))
     :MULTISIG)
    (t nil)))

(defun parse-and-classify-script (bytes)
  (let* ((parsed (parse-script bytes))
	 (type (script-type parsed)))
    (cond ((eq type :P2SH)
	   (make-instance '<p2sh-script> :script-hash (cadr parsed)))
	  ((eq type :MULTISIG)
	   (let* ((number-of-keys (to-value (first parsed)))
		  (keys (subseq parsed 1 (1+ number-of-keys))))
	     (make-instance '<multisig-script> :needed (to-value (nth (1+ number-of-keys) parsed)) :of keys)))
	  (t
	   ;;(format t "Unknown script type ~%")
	   parsed))))

