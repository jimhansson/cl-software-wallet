(in-package :cl-software-wallet.psbt)


(defvar +psbt-magic-constant+ #x70736274)

(defvar +logging-categories+ '(:cl-software-wallet.psbt :psbt))

;; things have chaged so we need to update with more types here.
(defvar +global-map-key-list+ '((#(0) . :UNSIGNED-TX)
				(#(1) . :GLOBAL-XPUB)))

(defvar +input-map-key-list+  '((#(0) . :INPUT-NON-WITNESS-UTXO)
				(#(1) . :INPUT-WITNESS-UTXO)
				(#(2) . :INPUT-PARTIAL-SIG)
				(#(3) . :INPUT-SIGHASH-TYPE)
				(#(4) . :INPUT-REDEEM-SCRIPT)
				(#(5) . :INPUT-WITNESS-SCRIPT)
				(#(6) . :INPUT-BIP32-DERIVATION)
				(#(7) . :INPUT-FINAL-SCRIPTSIG)
				(#(8) . :INPUT-FINAL-SCRIPTWITNESS)))

(defvar +output-map-key-list+ '((#(0) . :OUTPUT-REDEEM-SCRIPT)
				(#(1) . :OUTPUT-WITNESS-SCRIPT)
				(#(2) . :OUTPUT-BIP32-DERIVATION)))

(defun begins-with (term col)
  "small function to test if two things hare equalp up until the
shortest of the two. does actually work as a :test argument in assoc:s
for example."
  (let ((length (min (length term) (length col))))
    (equalp (subseq term 0 length)
	    (subseq col 0 length))))


(defstruct sig
  (bytes #() :read-only t))

(defmethod print-object ((obj sig) stream)
  (print-unreadable-object (obj stream :type t)
    (format stream "~a" (octets-to-hex-string (sig-bytes obj)))))

(defstruct pk
  (bytes #() :read-only t))

(defmethod print-object ((obj pk) stream)
  (print-unreadable-object (obj stream :type t)
    (let ((fingerprint (octets-to-hex-string (subseq (pk-bytes obj) 0 4))))
      (format stream "fingerprint: ~a" fingerprint))))

(defstruct bip32-derivation
  (fingerprint nil :read-only t :type (vector (unsigned-byte 8)))
  (path '()  :read-only t))

(defmethod print-object ((obj bip32-derivation) stream)
  (print-unreadable-object (obj stream :type t)
    (let ((path (map 'list
		     (lambda (x)
		       (if (>= x #x80000000)
			   (format nil "'~a" (- x #x80000000))
			   (format nil "~a" x)))
		     (bip32-derivation-path obj))))
      (format stream "fingerprint ~a path: ~{~a~^/~}"
	      (octets-to-hex-string (bip32-derivation-fingerprint obj))
	      path))))
  
(defun read-bip32-derivation (buf)
  "buf should contain a 4 byte fingerprint, followed by a number of 4
bytes path indexs"
  (labels ((divisable-by-four  (n)
	     (= (rem n 4) 0))
	   (read-path-element (path-buf index)
	     ;; read one path element
	     (let ((i (* index 4)))
	       (octets-to-integer (subseq path-buf i (+ i 4)) :big-endian nil)))
	   (read-path (path-buf)
	     (loop
		for index from 0 to (- (floor (length path-buf) 4) 1)
		collect (read-path-element path-buf index))))
    (assert (divisable-by-four (length buf)) nil "Buffer was not of
  correct size to read a bip32-derviation from")
    ;; first 4 bytes are the fingerprint
    (make-bip32-derivation :fingerprint (subseq buf 0 4)
			   ;; rest is the path
			   :path (read-path (subseq buf 4)))))


(defgeneric construct-map-key (key-type key))

(defmethod construct-map-key (key-type key)
  key)

(defmethod construct-map-key ((key-type (eql :INPUT-PARTIAL-SIG)) key)
  (make-pk :bytes key))

(defmethod construct-map-key ((key-type (eql :INPUT-BIP32-DERIVATION)) key)
  (make-pk :bytes key))

(defmethod construct-map-key ((key-type (eql :OUTPUT-BIP32-DERIVATION)) key)
  (make-pk :bytes key))

(defmethod construct-map-key ((key-type (eql :GLOBAL-XPUB)) key)
  (cl-software-wallet.hdkey:parse-hdkey-bytes key))


(defun classify-script (bytes)
  (cl-software-wallet.script:parse-and-classify-script bytes))

(defgeneric construct-map-value (key-type val))

(defmethod construct-map-value (key-type val)
  val)

(defmethod construct-map-value ((key-type (eql :INPUT-PARTIAL-SIG)) val)
  (make-sig :bytes val))

(defmethod construct-map-value ((key-type (eql :UNSIGNED-TX)) val)
  (read-tx val 0))

(defmethod construct-map-value ((key-type (eql :INPUT-NON-WITNESS-UTXO)) val)
  (read-tx val 0))

(defmethod construct-map-value ((key-type (eql :GLOBAL-XPUB)) val)
  (read-bip32-derivation val))

(defmethod construct-map-value ((key-type (eql :OUTPUT-BIP32-DERIVATION)) val)
  (read-bip32-derivation val))

(defmethod construct-map-value ((key-type (eql :INPUT-BIP32-DERIVATION)) val)
  (read-bip32-derivation val))

(defmethod construct-map-value ((key-type (eql :INPUT-WITNESS-UTXO)) val)
  (read-tx-out val 0))

(defmethod construct-map-value ((key-type (eql :INPUT-WITNESS-SCRIPT)) val)
  (classify-script val))

(defmethod construct-map-value ((key-type (eql :INPUT-REDEEM-SCRIPT)) val)
  (classify-script val))

(defmethod construct-map-value ((key-type (eql :INPUT-FINAL-SCRIPTSIG)) val)
  (classify-script val))

(defmethod construct-map-value ((key-type (eql :OUTPUT-WITNESS-SCRIPT)) val)
  (classify-script val))

(defmethod construct-map-value ((key-type (eql :OUTPUT-REDEEM-SCRIPT)) val)
  (classify-script val))

(defun key-type (val key-type-list)
  "function that will inspect val to see if it begins with something that could be found
in key-type-list"
  (let ((key-type (assoc val key-type-list :test #'begins-with)))
    (unless key-type
      (v:error +logging-categories+ "key-type, could not find matching key for ~a in ~a" val key-type-list))
    (list (cdr key-type)
	  (construct-map-key (cdr key-type)
			     (subseq val (length (car key-type)) (length val))))))

(defun read-map-record (buf start key-type-list)
  (declare (optimize (debug 3)))	
  (v:trace +logging-categories+ "read-map-record")
  (if (= (aref buf start) 0)
      (list)				;  used by collect-until-null
      (multiple-value-bind (key-length consumed)
	  (read-compact-integer buf start)
	(multiple-value-bind (value-length consumed2)
	    (read-compact-integer buf (+ start consumed key-length))
	  (let ((key-type-key (key-type (subseq buf
						(+ start consumed)
						(+ start consumed key-length))
					key-type-list))
		(val (subseq buf
			     (+ start consumed key-length consumed2)
			     (+ start consumed key-length consumed2 value-length))))
	    (v:trace +logging-categories+ "read-map-record  ~a, ~a, ~%" key-length value-length)
	    (list key-type-key
		  (construct-map-value (car key-type-key) val)
		  (+ consumed consumed2 key-length value-length)))))))

(defun collect-until-null (function initial-value)
  "Collects INITIAL-VALUE and the results of repeatedly applying FUNCTION to
   INITIAL-VALUE into a list.  When the result is NIL, iteration stops."
  (if initial-value
      (cons initial-value
            (collect-until-null function (funcall function initial-value)))))

(defun read-map (buf start key-type-list)
  (declare (optimize (debug 3) (safety 3)))
  (v:trace +logging-categories+ "read-map at: ~a" start)
  (let ((position start))
    (flet ((read-next (val)
	     ;; use read-map-record and update position if we get a record
	     (let ((record (read-map-record buf position key-type-list)))
	       (v:trace +logging-categories+ "read-map-record returned ~a" record)
	       (when record
		 (incf position (nth 2 record))
		 `(,(car record) . ,(cadr record))))))
      (let ((map (collect-until-null #'read-next  (read-next nil))))
	(assert (= (aref buf position) #x00))
	(incf position)
	;;(v:trace +logging-categories+ "read-map returned ~a ~a" map (- position start))
	(values map
		;; also return how may bytes we have consumed
		(- position start))))))

(defun read-list (buf start expected-count key-type-list)
  (declare (optimize (debug 3) (safety 3)))
  (v:trace +logging-categories+ "read-list at: ~a, will try to read: ~a" start expected-count)
  (if (not (= expected-count 0))
      (let* ((current-idx start)
	     (maps (loop for x from 1 upto expected-count
		      collect
			(multiple-value-bind (input-map consumed-bytes)
			    (read-map buf current-idx key-type-list)
			  (incf current-idx consumed-bytes)
			  input-map))))
	(values
	 maps
	 (- current-idx start)))
      (values () 0)))

(defun global-map (psbt)
  (nth 0 psbt))

(defun input-list (psbt)
  (nth 1 psbt))

(defun output-list (psbt)
  (nth 2 psbt))

(defun only-unique-p (lst &key (test #'equalp))
  "predicate that checks a list to see that it only contains unique elements"
  (or (null lst)
      (and (not (member (car lst) (cdr lst) :test test))
	   (only-unique-p (cdr lst)))))

(defun remove-key-type (x)
  "removes the key-type from an entry in psbt maps, used internaly in
find-by-key-type and find-by-key, turns ((:key-type A) . B) into (A . B)"
  `(,(car (cdr (car x))) . ,(cdr x)))

(defun find-by-key-type (key-type map)
  (v:trace +logging-categories+ "find-by-key-type")	
  (flet ((match-against-key-type (x)
	   (equalp (caar x) key-type)))
    (let ((val (map 'list
		    #'remove-key-type
		    (remove-if-not
		     #'match-against-key-type
		     map))))
      (v:trace +logging-categories+ "find-by-key-type returns ~a" val)
      val)))

(defun find-by-key (key-type key map)
  (v:trace +logging-categories+ "find-by-key")
  (flet ((match-against-key-and-type (x)
	   (equalp (car x) `(,key-type ,key))))
    (map 'list
	 #'remove-key-type
	 (remove-if-not
	  #'match-against-key-and-type
	  map))))

(defun check-global-map (map)
  (assert (eq (caaar map) :UNSIGNED-TX) (map) "Missing unsigned transaction")
  (assert (equalp (nth 1 (caar map)) '#()) (map) "Unsigned transaction should have a nil key"))

(defun check-key-length (item length)
  (let* ((key-tuple (car item))
	 (key (cadr key-tuple)))
    (assert (= (length key) length) (key length) "Invalid length of key")))

(defmacro check-item-value-type (item value-type)
  `(check-type (cdr ,item) ,value-type "Value is of wrong type"))

(defmacro check-item-key-type (item key-type)
  `(check-type (cadar ,item) ,key-type "Key is of wrong type"))

(defmacro check-pk-key-length (item)
  `(let ((pk (cadar ,item)))
     (assert (eq (length (pk-bytes pk)) 33) (pk) "Invalid length of PK")))

(defgeneric check-item-by-type (type item))

(defmethod check-item-by-type (type item)
  (v:error +logging-categories+ "check-item-by-type called on a unknown type ~a" type))

(defmethod check-item-by-type ((type (eql :UNSIGNED-TX)) item)
  ;; make sure the tx-in scripts have not been filled in, we are
  ;; supposed to take that from other places in this PSBT.
  (let ((trans (cdr item)))
    ;;(format t "trans ~a~%" trans)
    (with-slots (tx-in) trans
      (dolist (input tx-in)
	;;(format t "input ~a~%" input)
	(with-slots (script) input
	  ;;(format t "script ~a~%" script)
	  (assert (equalp script #()) nil "script in input of unsigned-tx is filled in"))))))

(defmethod check-item-by-type ((type (eql :INPUT-NON-WITNESS-UTXO)) item)
  (check-key-length item 0))

(defmethod check-item-by-type ((type (eql :INPUT-PARTIAL-SIG)) item)
  (check-item-key-type item pk)
  (check-pk-key-length item))

(defmethod check-item-by-type ((type (eql :INPUT-REDEEM-SCRIPT))item)
  (check-key-length item 0))

(defmethod check-item-by-type ((type (eql :INPUT-SIGHASH-TYPE)) item)
  (check-key-length item 0))

(defmethod check-item-by-type ((type (eql :INPUT-WITNESS-UTXO)) item)
  (check-key-length item 0))

(defmethod check-item-by-type ((type (eql :INPUT-WITNESS-SCRIPT)) item)
  (check-key-length item 0))

(defmethod check-item-by-type ((type (eql :INPUT-FINAL-SCRIPTSIG)) item)
  (check-key-length item 0))

(defmethod check-item-by-type ((type (eql :INPUT-BIP32-DERIVATION)) item)
  (check-item-key-type item pk)
  (check-pk-key-length item))

(defmethod check-item-by-type ((type (eql :INPUT-FINAL-SCRIPTWITNESS)) item)
  (check-key-length item 0))

(defmethod check-item-by-type ((type (eql :OUTPUT-REDEEM-SCRIPT)) item)
  (check-key-length item 0))

(defmethod check-item-by-type ((type (eql :OUTPUT-WITNESS-SCRIPT)) item)
  (check-key-length item 0))

(defmethod check-item-by-type ((type (eql :OUTPUT-BIP32-DERIVATION)) item)
  (check-item-key-type item pk)
  (check-pk-key-length item))

(defmethod check-item-by-type ((type (eql :GLOBAL-XPUB)) item)
  ;; todo
  )

(defun check-psbt (psbt)
  "Checks that the PSBT is well formated"
  (v:trace +logging-categories+ "check-psbt")
  ;; every psbt should have an unsigned tx. and it should be not be keyed
  (check-global-map (global-map psbt))
  ;; check that every item in the global map adheres to rules.
  (dolist (item (global-map psbt))
    (check-item-by-type (caar item) item))
  (let ((inputs (input-list psbt))
	(outputs (output-list psbt)))
    ;; check the inputs
    (dolist (input inputs)
      (dolist (item input)
	(check-item-by-type (caar item) item))
      (let ((keys (map 'list #'car input)))
	;; check for duplicated keys.
	(assert (only-unique-p keys) (input) "Duplicated keys")))

    ;; check the outputs
    (dolist (output outputs)
      (dolist (item output)
	(check-item-by-type (caar item) item))
      (let ((keys (map 'list #'car output)))
	;; check for duplicated keys.
	(assert (only-unique-p keys) (output) "Duplicated keys"))))
  psbt)



(defun in-count-of (tx)
  (with-slots (tx-in) tx
    (length tx-in)))

(defun out-count-of (tx)
  (with-slots (tx-out) tx
    (length tx-out)))

(defun parse-psbt (vec)
  (declare (optimize (debug 3) (safety 3)))
  (v:trace +logging-categories+ "parse-psbt")
  ;; check first bytes
  (assert (= +psbt-magic-constant+ (ub32ref/be vec 0)))
  (assert (= #xFF (aref vec 4)))
  (let ((current-idx 5))
    (multiple-value-bind (global-map consumed-bytes)
	(read-map vec current-idx +global-map-key-list+)
      (check-global-map global-map)
      (incf current-idx consumed-bytes)
      (v:debug +logging-categories+ "global-map-size: ~a, current-idx: ~a" (length global-map) current-idx)
      (let ((inputs-in-tx (in-count-of (cdar (find-by-key-type :UNSIGNED-TX global-map))))
	    (outputs-in-tx (out-count-of (cdar (find-by-key-type :UNSIGNED-TX global-map)))))
	(v:debug +logging-categories+ "inputs-in-tx: ~a, outputs-in-tx: ~a" inputs-in-tx outputs-in-tx)
	;; All the time I spent on this code thinking that the arrays
	;; of maps was prepended with an iteger telling me how many
	;; items was in the list. but no that was not the right way,
	;; the transaction in the global map was the thing that I
	;; should have looked at. I can never get back that time :-(
	(multiple-value-bind (inputs consumed-bytes)
	    (read-list vec current-idx inputs-in-tx +input-map-key-list+)
	  (incf current-idx consumed-bytes)
	  (assert (eq (length inputs) inputs-in-tx) nil "Number of inputs does not match transaction")
	  (v:debug +logging-categories+ "inputs-size: ~a, current-idx: ~a" (length inputs) current-idx)
	  (v:sync)
	  (multiple-value-bind (outputs consumed-bytes)
	      (read-list vec current-idx outputs-in-tx +output-map-key-list+) 
	    (incf current-idx consumed-bytes)
	    (assert (eq (length outputs) outputs-in-tx) nil "Number of outputs does not match transaction")
	    (v:debug +logging-categories+ "outputs-size: ~a, current-idx: ~a" (length outputs) current-idx)
	    (v:sync)
	    (check-psbt
	     (list global-map
		   inputs
		   outputs))))))))


(defgeneric serialize-value (value))

(defmethod serialize-value (value)
  (declare (optimize (debug 3) (safety 3)))
  value)

(defmethod serialize-value ((value list))
  (declare (optimize (debug 3) (safety 3)))
  (apply #'concatenate 'vector
	 (loop for x in value
	    collect (serialize-value x))))

(defmethod serialize-value ((value <tx>))
  (declare (optimize (debug 3) (safety 3)))
  (cl-software-wallet:to-bytes value))

(defmethod serialize-value ((value <script>))
  (declare (optimize (debug 3) (safety 3)))
  (to-bytes value))

(defmethod serialize-value ((value <tx-out>))
  (declare (optimize (debug 3) (safety 3)))
  (to-bytes value))

(defgeneric serialize-key (key))

(defmethod serialize-key (key)
  (if (vectorp key)
      key
      (error "dont know how to serialize key ~a~%" key)))

(defmethod serialize-key ((key vector))
  key)

(defmethod serialize-key ((key pk))
  (with-slots (bytes) key
    bytes))

(defmethod serialize-key ((key cl-software-wallet.hdkey:hdkey))
  (cl-software-wallet.hdkey:write-hdkey-bytes key))

(defmethod serialize-value ((value bip32-derivation))
  (apply #'concatenate '(vector (unsigned-byte 8))
	 (bip32-derivation-fingerprint value)
	 (map 'list (lambda (x) 
		      (integer-to-octets x :n-bits 32 :big-endian nil))
	      (bip32-derivation-path value))))

(defmethod serialize-value ((value sig))
  (with-slots (bytes) value
    bytes))

(defun write-item (item key-map)
  (declare (optimize (debug 3) (safety 3)))
  (let* ((key-type (caar item))
	 (encoded-key-type (car (rassoc key-type key-map)))
	 (key (cadar item))
	 (serialized-key (serialize-key key))
	 (key-length 
	  (serialize-compact-integer (+ (length encoded-key-type)
					(length serialized-key))))
	 (value (cdr item))
	 (serialized-value (serialize-value value))
	 (value-length (serialize-compact-integer (length
						   serialized-value))))
    
    ;;(format t "write-item  ~a, ~a, ~a ~%" key-type (byte-array-to-hex-string serialized-key) (byte-array-to-hex-string serialized-value))

    (concatenate 'vector
		 key-length
		 encoded-key-type
		 serialized-key
		 value-length
		 serialized-value)))

(defun write-map (map key-map)
  (declare (optimize (debug 3) (safety 3)))
  (concatenate 'vector 
	       (apply #'concatenate 'vector
		      (loop for x in map
			 collect (write-item x key-map)))
	       #(#x0)))

(defun as-bytes (something)
  (declare (optimize (debug 3) (safety 3)))
  (integer-to-octets something))

(defun write-psbt (psbt)
  (declare (optimize (debug 3) (safety 3)))
  (check-psbt psbt)
  (concatenate '(vector (unsigned-byte 8))
	       (as-bytes +psbt-magic-constant+)
	       #(#xFF)
	       (write-map (global-map psbt) +global-map-key-list+)
	       (apply #'concatenate 'vector
		      (loop for x in (input-list psbt)
			 collect (write-map x +input-map-key-list+)))
	       (apply #'concatenate 'vector
		      (loop for x in (output-list psbt)
			 collect (write-map x +output-map-key-list+)))))

