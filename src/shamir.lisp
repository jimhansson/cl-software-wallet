
(in-package #:cl-software-wallet.shamir)

(defconstant +SECRET-INDEX+ 255)
(defconstant +DIGEST-INDEX+ 254)

(defconstant +KDF-ROUND-COUNT+ 4)
(defconstant +KDF-BASE-ITERATION-COUNT+ 10000)
(defconstant +DIGEST-LENGTH-BYTES+ 4)

(defvar precomp-exp nil)
(defvar precomp-log nil)

(deftype share-int ()
  `(integer 0 ,(expt 2 10)))

(deftype share-ints ()
  '(vector share-int))

(deftype 4bit-integer ()
  `(integer 0 ,(expt 2 4)))

(deftype 5bit-integer ()
  `(integer 0 ,(expt 2 5)))

(deftype 15bit-integer ()
  `(integer 0 ,(expt 2 15)))

;; slip-39 implmementation
(defstruct share
  "Represent a part of a shared secret

    The identifier (id) field is a random 15-bit value which is the
    same for all shares and is used to verify that the shares belong
    together; it is also used as salt in the encryption of the master
    secret.

    The iteration exponent (e) field indicates the total number of
    iterations to be used in PBKDF2. The number of iterations is
    calculated as 10000×2e.

    The group index (GI) field3 is the x value of the group share.
    The group threshold (Gt) field3 indicates how many group shares
    are needed to reconstruct the master secret. The actual value is
    encoded as Gt = GT − 1, so a value of 0 indicates that a single
    group share is needed (GT = 1), a value of 1 indicates that two
    group shares are needed (GT = 2) etc.

    The group count (g) indicates the total number of groups. The
    actual value is encoded as g = G − 1.

    The member index (I) field3 is the x value of the member share in
    the given group.

    The member threshold (t) field3 indicates how many member shares
    are needed to reconstruct the group share. The actual value is
    encoded as t = T − 1.

    The padded share value (ps) field corresponds to a list of the SSS
    part's fk(x) values (see the diagram above), 1 ≤ k ≤ n. Each fk(x)
    value is encoded as a string of eight bits in big-endian
    order. The concatenation of these bit strings is the share
    value. This value is left-padded with '0' bits so that the length
    of the padded share value in bits becomes the nearest multiple of
    10.

    The checksum (C) field is an RS1024 checksum (see below) of the
    data part of the share (that is id || e || GI || Gt || g || I || t
    || ps). The customization string (cs) of RS1024 is 'shamir'.


This structure is then converted into a mnemonic code by splitting it
up into 10-bit segments with each becoming an index into a word list
containing exactly 1024 words (see below). Big-endian bit order is
used in all conversions. The entropy4 of the master secret MUST be at
least 128 bits and its length MUST be a multiple of 16 bits. All
implementations MUST support master secrets of length 128 bits and 256
bits:

Security 	Padded share value length 	Total share length
128 bits 	130 bits     200 bits           20 words
256 bits 	260 bits     330 bits           33 words

This construction yields a beneficial property where the random
identifier and the iteration exponent transform into the first two
words of the mnemonic code, so the user can immediately tell whether
the correct shares are being combined, i.e. they have to have the same
first two words. Moreover, the third word encodes the group index,
group threshold and part of the group count. Since the group threshold
and group count are constant, all shares belonging to the same group
start with the same three words.
"
    
  (identifier 1 :type 15bit-integer)
  (exponent 0 :type 5bit-integer)
  (group-index 0 :type 4bit-integer)
  (group-threshold 1 :type 4bit-integer)
  (group-count 1 :type 4bit-integer)
  (member-index 1 :type 4bit-integer)
  (member-threshold 1 :type 4bit-integer)
  (share-value (make-array 16 :element-type '(unsigned-byte 8) :initial-element #x0)))

(defgeneric recover-secret (threshold x &rest shares)
  (:documentation "Input: threshold T, a list of m share-index/share-value pairs [(x1,
  y1), ... , (xm, ym)]

Output: the shared secret S")
  (:method-combination dbc))

(defgeneric combine-group-shares (shares)
  (:documentation "Combines a list of shares that belong to the same
  group into a group secret that can later be combine with other group
  secrets.")
  (:method-combination dbc))

(defgeneric combine-shares (shares passphrase)
  (:documentation "Takes a list of SHARES and a PASSPHRASE and returns
  the master secret that those shares represent. Shares need to be the
  required number of shares to pass the threshold and not more.")
  (:method-combination dbc))

(defgeneric mnemonic->ints (words wordlist)
  (:documentation "Turns an array of words into an array of ints
  containing the position of the words in wordlist")
  (:method-combination dbc))

(defgeneric ints->mnemonic (ints wordlist)
  (:documentation "Turns an array of integers into an array of words
  taking the word at the position of integers in wordlist")
  (:method-combination dbc))

(defgeneric ints->share (ints)
  (:documentation "Construct a share from a array of ints")
  (:method-combination dbc))

(defgeneric share->ints (share)
  (:documentation "Turns a share into an array of ints")
  (:method-combination dbc))

(defgeneric share->mnemonic (share)
  (:documentation "")
  (:method-combination dbc))

(defgeneric mnemonic->share (mnemonic)
  (:documentation "")
  (:method-combination dbc))

(defgeneric split-secret (threshold N S)
  (:documentation "Splits S into N parts where threshold parts are
  needed to restore S.")
  (:method-combination dbc))

(defmethod print-object ((obj share) stream)
  (print-unreadable-object (obj stream :type t)
    (let ((id (share-identifier obj))
	  (e (share-exponent obj))
	  (GI (share-group-index obj))
	  (Gt (share-group-threshold obj))
	  (g (share-group-count obj))
	  (MI (share-member-index obj))
	  (Mt (share-member-threshold obj))
	  (val (share-share-value obj)))
      (format stream
	      "id: ~a, e: ~a, GI: ~a, Gt: ~a, g: ~a, MI: ~a, Mt: ~a, ~
               value: ~/cl-software-wallet:pprint-octets-hex/"
	      id e GI Gt g MI Mt val))))

(defmethod ints->share ((ints array))
  "takes an array of ints and turns it into share, todo this we first
turn it into a bit array. then we subdivide it into the parts of the
  share."
  (v:trace '(:shamir) "ints->share ints=~a" ints)
  (let ((bits (apply #'concatenate 'bit-vector
		     (map 'list
			  (lambda (x)
			    (integer->bit-vector x 10))
			  ints))))
    (let* ((chksum-begin (- (length bits) 30))
	   (data-begin (case (length ints)
			 (20 (- chksum-begin 128))
			 (33 (- chksum-begin 256))
			 (59 (- chksum-begin 512))
			 (otherwise (error "Unhandled case int int->share"))))
	   (nr-bytes (case (length ints)
		       (20 16)
		       (33 32)
		       (59 64)
		       (otherwise (error "Unhandled case int int->share"))))
	   (share-value
	    (loop for x upto (- nr-bytes 1)
	       collect (bit-vector->integer (subseq bits
						    (+ data-begin (* x 8))
						    (+ data-begin (* x 8) 8))))))
      (assert (< 40 data-begin))
      (make-share :identifier (bit-vector->integer          (subseq bits 0 15))
		  :exponent (bit-vector->integer            (subseq bits 15 20))
		  :group-index (bit-vector->integer         (subseq bits 20 24))
		  :group-threshold (+ (bit-vector->integer  (subseq bits 24 28)) 1)
		  :group-count (+ (bit-vector->integer      (subseq bits 28 32)) 1)
		  :member-index (bit-vector->integer        (subseq bits 32 36))
		  :member-threshold (+ (bit-vector->integer (subseq bits 36 40)) 1)
		  :share-value (coerce share-value 'var-byte-array)))))

(defmethod share->ints (share)
  "Converts a share into an array of ints, adding a checksum at the end."
  (v:trace '(:shamir) "share->ints share=~a" share)
  (let* ((share-value-bits 
	  (apply #'concatenate 'bit-vector
		 (map 'list
		      (lambda (x)
			(integer->bit-vector x 8))
		      (share-share-value share))))
	 (data
	  (concatenate
	   'bit-vector
	   (integer->bit-vector (share-identifier share) 15)
	   (integer->bit-vector (share-exponent share) 5)
	   (integer->bit-vector (share-group-index share) 4)
	   (integer->bit-vector (- (share-group-threshold share) 1) 4)
	   (integer->bit-vector (- (share-group-count share) 1) 4)
	   (integer->bit-vector (share-member-index share) 4)
	   (integer->bit-vector (- (share-member-threshold share) 1) 4)
	   ;; add padding bits depending on length.
	   (case (length share-value-bits)
	     (128 (make-array 2 :element-type 'bit :initial-element 0))
	     (256 (make-array 4 :element-type 'bit :initial-element 0))
	     (512 (make-array 8 :element-type 'bit :initial-element 0))
	     (t (assert nil nil "Unhandled length share->ints")))
	   share-value-bits)))
    (let ((ints
	   (loop
	      for x below (length data) by 10
	      collect (bit-vector->integer (subseq data x (+ x 10))))))
      (concatenate 'share-ints
		   ints
		   (rs1024-create-checksum "shamir" ints)))))

(defmethod share->mnemonic ((share share))
  (ints->mnemonic (share->ints share) +wordlist+))

(defmethod mnemonic->share ((mnemonic list))
  (ints->share (mnemonic->ints mnemonic +wordlist+)))

;; below this we follow the SLIP-39 in naming of functions.

(defun precompute-exp-log (size)
  (let ((poly 1)
	(precomp-exp (make-array (- size 1) :initial-element 0))
	(precomp-log (make-array size :initial-element 0)))

    (dotimes (i (- size 1))
      (setf (elt precomp-exp i) poly)
      (setf (elt precomp-log poly) i)

      ;; Multiply poly by the polynomial x + 1.
      (setq poly (logxor (ash poly 1) poly))

      ;; Reduce poly by x^8 + x^4 + x^3 + x + 1
      (unless (= (logand poly #x100) 0)
	(setq poly (logxor poly #x11b))))
    (values precomp-exp precomp-log)))

(multiple-value-bind (exp log)
    (precompute-exp-log 256)
  (setq precomp-exp exp)
  (setq precomp-log log))

(defun interpolate (x &rest shares)
  (v:trace '(:shamir) "interpolate x=~a, shares=~:{ (~a: ~/cl-software-wallet:pprint-octets-hex/) ~}"
	  x shares)
  
  ;; early return
  ;; we should also check that
  ;; - we only have one share,
  ;; - that that share has the x coord we are looking for.
  
  (let ((x-cords (remove-duplicates
		  (map 'vector #'car shares)))
	(log-prod 0)
	(log-basis-eval 0))

    ;; make surt the x-cords of everyone is unique
    (assert (= (length x-cords) (length shares)))
    ;; todo, check that all the shares have data that is of the same length

    ;; here we should have the early return again

    ;; Logarithm of the product of (x_i - x) for i = 1, ... , k
    (setq log-prod
	  (loop for share in shares
	     sum (elt precomp-log (logxor (car share) x))))

    (let ((result (make-array (length (cadar shares))
			      :initial-element #x0
			      :element-type '(unsigned-byte 8))))
      ;;(v:warn '(:shamir) "result ~a, log-prod ~a" result log-prod)
      (dolist (share shares)
	;;(v:warn '(:shamir) share)
	;; The logarithm of the Lagrange basis polynomial evaluated at
	;; x.
	
	(setq log-basis-eval
	      (mod
	       (- log-prod
		  (elt precomp-log (logxor (car share) x))
		  (loop for other in shares
		     sum (elt precomp-log (logxor (car share) (car other)))))
	       255))

	;;(v:info '(:shamir) "log-basis-eval ~a" log-basis-eval)

	(flet ((foobar (part current)
		 (assert (= (length part) (length current)))
		 (let ((result (make-array (length part)
					   :initial-element #x0
					   :element-type
					   '(unsigned-byte 8))))
		   (loop
		      for i upto (- (length part) 1)
		      for x across part
		      for y across current
		      do (setf (elt result i) ;; updatera result
			       (logxor y ;; med xor av vad som var förut
				       (if (= x 0) ;; och vad som vi finner i exp-table
					   0
					   (elt precomp-exp
						(mod (+ (elt precomp-log
							     x)
							log-basis-eval)
						     255))))))
		   result)))
	  (setq result (foobar (cadr share) result))))
      
      ;;(v:warn '(:shamir) "recovered secret ~a" result)
      result)))


(defun create-digest (random-data shared-secret)
  "Create a digest for index 254"
  (let ((hmac (ironclad:make-hmac random-data 'ironclad:SHA256)))
    (ironclad:update-hmac hmac shared-secret)
    (hmac-digest hmac)))

(defmethod split-secret (threshold N S)
  "we seems to need this just to get the method lookup to work"
  (v:info '(:shamir) "split-secret SHOULD NOT BE CALLED ~
                      threshold(~a)=~a, parts(~a)=~a, master-secret(~a)=~a"
	  (type-of threshold) threshold
	  (type-of N) N
	  (type-of S) S)
 (assert nil nil "Method lookup failed on split-secret"))


(let ((prng (ironclad:make-prng :OS)))
  (defun random-bytes (nr-bytes)
    ;;(ironclad:list-all-prngs)
    (let ((ironclad:*PRNG* prng))
      (ironclad:random-data nr-bytes))))

(defmethod split-secret ((threshold integer)
			 (N integer)
			 S)
  "Input: threshold T, number of shares N, secret S

Output: shares y1, ... , yN for share indices 0, ... , N − 1

1. Check the following conditions:
      0 < T ≤ N ≤ 16
      The length of S in bits is at least 128 and a multiple of 16.

   If any of these conditions is not satisfied, then abort.

2. If T is 1, then let yi = S for all i, 1 ≤ i ≤ N, and return.

3. Let n be the length of S in bytes. Generate R ∈ GF(256)n−4 randomly
with uniform distribution and let D be the concatenation of the first
4 bytes of HMAC-SHA256(key=R, msg=S) with the n − 4 bytes of R.

4. Let y1, ... , yT−2 ∈ GF(256)n be generated randomly, independently
with uniform distribution.

5. For i such that T − 2 < i ≤ N compute yi = Interpolation(i − 1,
{(0, y1), ... , (T − 3, yT−2), (254, D), (255, S)}).

The source of randomness used to generate the values in steps 3 and 4
above MUST be suitable for generating cryptographic keys."
  (v:trace '(:shamir) "split-secret threshold=~a, parts=~a, ~
                      master-secret=~/cl-software-wallet:pprint-octets-hex/"
	   threshold N S)
  (setq S (coerce S 'var-byte-array))
  (when (= threshold 1)
    ;; if threshold is only 1 we do not split the secret,
    (return-from split-secret
      (loop for i from 1 upto N collect `(,i ,S))))

  (let* ((random-share-count (- threshold 2))
	 (shares (loop
		    repeat random-share-count
		    for x = 0 then (1+ x)
		    collect `(,x ,(random-bytes (length S)))))

	 (random-part (random-bytes (- (length S)
				       +DIGEST-LENGTH-BYTES+)))
	 (digest (subseq (create-digest random-part S) 0 4))
	 (base-shares (concatenate 'list
				   shares
				   `((,+DIGEST-INDEX+ ,(concatenate 'vector digest random-part)))
				   `((,+SECRET-INDEX+ ,S)))))
    (v:debug '(:shamir) "split-secret random-share-count=~a, N=~a"
	     random-share-count N)
    
    (concatenate 'list
		 shares
		 (loop
		    repeat (- N random-share-count)
		    for i = random-share-count then (1+ i)
		    collect `(,i ,(apply #'interpolate i base-shares))))))

(defmethod recover-secret (threshold x &rest shares)
  "Input: threshold T, a list of m share-index/share-value pairs [(x1,
  y1), ... , (xm, ym)]

If T is 1, then let S = y1 and return, else
Compute S = Interpolation(255, [(x1, y1), ... , (xm, ym)]).
Output: the shared secret S"
  
  (v:trace '(:shamir) "recover-secret t=~a, x=~a, shares=~a" threshold x shares)
    
  (when (= threshold 1)
    (return-from recover-secret (cadar shares)))

  (apply #'interpolate +SECRET-INDEX+ shares))

(defun generate-member-shares (identifier exponent
			       group-idx group-threshold group-count
			       member-threshold member-count secret)
  "creates the member shares for a group"
  (v:trace '(:shamir) "generate-member-shares threshold=~a, groups=~a, ~
                       master-secret=~/cl-software-wallet:pprint-octets-hex/"
	  member-threshold member-count secret)
  (let ((secrets (split-secret member-threshold member-count secret))
	(member-idx -1))
    (loop
       for member-secret in secrets
       collect (make-share
		:identifier identifier
		:exponent exponent
		:group-index group-idx
		:group-threshold group-threshold
		:group-count group-count
		:member-index (incf member-idx)
		:member-threshold member-threshold
		:share-value (cadr member-secret)))))


(defmethod generate-shares (group-threshold (groups list)
			    master-secret (passphrase string)
			    exponent
			    &key (identifier nil))
  "Input: group threshold GT, list of member thresholds T1, ... , TG
  and group sizes N1, ... , NG, master secret MS, passphrase P,
  iteration exponent e

Output: list of shares

1. If Ti = 1 and Ni > 1 for any i, then abort.

2. Generate a random 15-bit value id. (if not any has been given, if
it has been given we are trying to extend a group of keys we allready
have and might have to take extra precation)

3. Compute the encrypted master secret EMS = Encrypt(MS, P, e, id).

4. Compute the group shares s1, ... , sG = SplitSecret(GT, G, EMS).

5. For each group share si, 1 ≤ i ≤ G, compute the member shares si,1,
... , si,Ni = SplitSecret(Ti, Ni, si).

6. For each i and each j, 1 ≤ i ≤ G, 1 ≤ j ≤ Ni, return (id, e, i − 1,
GT − 1, j − 1, Ti − 1, si,j)."
  (v:trace '(:shamir) "generate-shares threshold=~a, groups=~a, ~
                       master-secret=~a, passphrase=~a, exponent=~a"
	  group-threshold groups master-secret passphrase exponent)
  (unless identifier
    ;; don't need to crypto secure random here.
    (setq identifier (random (expt 2 15))))
  (let* ((ms-bytes (hex-string-to-octets master-secret))
	 (ems (encrypt ms-bytes identifier passphrase exponent))
	 (groups-secrets (split-secret group-threshold (length groups) ems))
	 (shares '()))
    (v:debug '(:shamir) "ems=~/cl-software-wallet:pprint-octets-hex/ ~
                         groups-secrets ~:{~* ~/cl-software-wallet:pprint-octets-hex/~}"
	    ems groups-secrets)
    (assert (= (length groups) (length groups-secrets)))
    
    (dotimes (grp-idx (length groups))
      (let* ((grp-secret (nth grp-idx groups-secrets))
	     (grp-threshold (car (nth grp-idx groups)))
	     (grp-count (cdr (nth grp-idx groups)))
	     (member-shares (generate-member-shares identifier exponent
						    grp-idx group-threshold (length groups)
						    grp-threshold grp-count (cadr grp-secret))))
	(setq shares (concatenate 'list member-shares shares))))
    
    ;;(v:info '(:shamir) "shares ~a" shares)
    (reverse shares)))

(defmethod combine-group-shares ((shares list))
  (v:trace '(:shamir) "combine-group-shares")
  (let ((result
	 (apply #'recover-secret
		(share-member-threshold (car shares))
		0 ;; magic-number
		(map 'list
		     (lambda (g)
		       `(,(share-member-index g) ,(share-share-value g)))
		     shares))))
    (v:debug '(:shamir) "group result ~a" result)
    result))


(defmethod combine-shares ((shares list) (passphrase string))
  "Input: list of shares, passphrase P

Output: master secret MS

1. Check the following conditions:

   - The checksum of each share MUST be valid. Implementations SHOULD
     NOT implement correction beyond potentially suggesting to the
     user where in the mnemonic an error might be found, without
     suggesting the correction to make5.

   - All shares MUST have the same identifier id, iteration exponent
     e, group threshold GT, group count G and length. The value of G
     MUST be greater than or equal to GT.

   - Let GM be the number of pairwise distinct group indices among the
     given shares. Then GM MUST be equal to GT.

   - All shares with a given group index GIi, 1 ≤ i ≤ GM, MUST have
     the same member threshold Ti, their member indices MUST be
     pairwise distinct and their count Mi MUST be equal to Ti.

   - The length of the padding of the share value in bits, which is
     equal to the length of the padded share value in bits modulo 16,
     MUST NOT exceed 8 bits.

   - All padding bits MUST be 0

   - The length of each share value MUST be at least 128 bits.

    Abort if any check fails.

2. Let si = RecoverSecret([(Ii,1, si,1), ... , (Ii,Mi, si,Mi)]), where
   Ii,j and si,j are the member-index/share-value pairs of the shares
   with group index GIi.


3. Let EMS = RecoverSecret([(GI1, s1), ... , (GIGM, sGM)])

4. Return MS = Decrypt(EMS, P, e, id)."
  (v:trace '(:shamir) "combine-shares")
  (flet ((list-of-groups (list)
	   (remove-duplicates
	    (sort
	     (map 'list #'share-group-index list)
	     #'<)))
	 (find-group-shares (group-idx shares)
	   (remove-if-not (lambda (share)
			    (= group-idx (share-group-index share)))
			  shares)))
    
        
    (let* ((group-indexes (list-of-groups shares))
	   (group-secrets
	     (loop for g-idx in group-indexes
		collect `(,g-idx
			  ,(combine-group-shares
			    (find-group-shares g-idx shares))))))

      (ironclad:byte-array-to-hex-string
       (coerce 
	(decrypt
	 (apply #'recover-secret
		(share-group-threshold (car shares))
		0 ;; magic number
		group-secrets) 		
	 (share-identifier (car shares)) 
	 passphrase
	 (share-exponent (car shares)))
	'var-byte-array)))))

;; (D)encryption of master secret
(defun F (i id passphrase R e)
  "Feistel network function for the luby-rackoff construct, also does
password-strengthening by doing PBKDF2 schema with SHA-256"
  (v:trace '(:shamir) "F i: ~a id: ~a passphrase: ~a R: ~a" i
	   (ironclad:byte-array-to-hex-string (ironclad:integer-to-octets id))
	   passphrase (ironclad:byte-array-to-hex-string (coerce R 'var-byte-array)))
  (assert (< i 255))
  (let ((password
	 (concatenate-byte-array
	   (ironclad:integer-to-octets i :n-bits 8)
	   (ironclad:ascii-string-to-byte-array passphrase)))
	(salt
	 (concatenate-byte-array
	   (ironclad:ascii-string-to-byte-array "shamir")
	   (ironclad:integer-to-octets id)
	   R))
	(iterations (/ (ash +KDF-BASE-ITERATION-COUNT+ e)
		       +KDF-ROUND-COUNT+))
	(kdf (ironclad:make-kdf 'ironclad:PBKDF2 :digest 'ironclad:sha256)))
    (ironclad:derive-key kdf password
			 salt iterations
			 (length R))))


(defun xor-bytes (a b)
  (let ((bytes (make-array (length a) :element-type '(unsigned-byte 8))))
    (dotimes (x (length a))
      (setf (elt bytes x) (logxor (elt a x) (elt b x))))
    bytes))

(defun luby-rackoff-construct (data id passphrase e func rounds)
  "The master secret is encrypted using a wide-blocksize pseudorandom
  permutation based on the Luby-Rackoff construction. It consists of
  a four round Feistel network with the key derivation function
  PBKDF2 as the round function. This scheme is invertible, which
  means that the creator of the shares can choose the master secret,
  making it possible to migrate a BIP-32 wallet from BIP-39 mnemonics
  to the new secret sharing scheme.

  data is split into two parts (left,right), 
  func is called with the right part of data and the other inputs,
  except rounds, we expect that two be a list of numbers either
  decreasing or increasing, it defines the Feistel network rounds. and
  should be increasing for encryption and descreasing for decryption."
  (let ((data-middle (/ (length data) 2)))
    (let ((L (subseq data 0 data-middle))
	  (R (subseq data data-middle))
	  (old-R nil)
	  (old-L nil))
      (loop for i in rounds
	 do (progn
	      ;; could use PSETQ instead
	      (setq fres (funcall func i id passphrase R e))
	      (setq old-R R)
	      (setq old-L L)
	      (setq R (xor-bytes old-L fres))
	      (setq L old-R)))
      (concatenate 'vector R L))))

(defun encrypt (ms id passphrase e)
  (v:trace '(:shamir) "encrypt ms=~X, id=~a, passphrase=~X"
	   ms id passphrase)
  (luby-rackoff-construct ms id passphrase e #'F '(0 1 2 3)))

(defun decrypt (ems id passphrase e)
  (v:trace '(:shamir) "decrypt ems=~X, id=~a, passphrase=~X"
	   ems id passphrase)
  (luby-rackoff-construct ems id passphrase e #'F '(3 2 1 0)))

;; checksum functions

;; we have polymod in both bech32 and in shamir, it could be replaced
;; with a generic version in util instead.
(defun rs1024-polymod (values)
  "A reed solomon checksum implementation over 1024(10-bits values),
this function could be used both in creating the checksum and
verifiying it with the appropiate wrappers"
  (v:trace '(:shamir) "rs1024-polymod")
  (let ((last-chk 1))
    (loop
       ;; Starting values
       with GEN = #(#xe0e040   #x1c1c080  #x3838100  #x7070200  #xe0e0009
		    #x1c0c2412 #x38086c24 #x3090fc48 #x21b1f890 #x3f3f120)
       ;;initially (setq last-chk  1)
       ;; iteration data.
       for v across values
       ;; immediates
       for b = (ash last-chk -20)
       for chk = (logxor (ash (logand last-chk #xFFFFF) 10) v)
       ;; do work to calculate new last-chk
       do (progn
	    (format t "~10b ~s ~s~%" b chk last-chk)
	    (dotimes (i 10)
	      ;; we may not update last-chk if b contains only zeros
	      (setq last-chk chk)
	      (when (logbitp i b)
		(setq last-chk (logxor chk (aref GEN i))))))
       ;; return result
       finally (return last-chk))))

(defun rs1024-verify-checksum (cs data)
  "Wrapper around rs1024-polymod to verify a checksum. we expect the
checksum to be part of data."
  (= (rs1024-polymod (concatenate 'vector
				  (map 'vector #'char-code cs)
				  data))
     1))

(defun rs1024-create-checksum (cs data)
  "Wrapper around rs1024-polymod to create a checksum."
  (let* ((values (concatenate 'vector
			      (map 'vector #'char-code cs)
			      data))
	 (polymod (logxor (rs1024-polymod (concatenate 'vector
						       values
						       #(0 0 0)))
			  1))) ;; clears one bit, why?
    (loop
       for i below 3
       collect (logand (ash polymod (- (* 10 (- 2 i))))
		       1023))))

(defmethod mnemonic->ints ((words list) (wordlist list))
  "translates a list words into a list of ints depending on the
position within wordlist."
  (flet ((to-int (w)
	   ;; may return nil
	   (position w wordlist :test 'string=)))
    (let ((ints (map 'vector #'to-int words)))
      ;; check that we did not get any nils
      (assert (notany #'null ints) nil "One of the words was not in wordlist")
      ints)))

(defmethod ints->mnemonic ((ints array) (wordlist list))
  "turns a list of ints into a list of words from wordlist depending
on position in list."
  (v:trace '(:shamir) "ints->mnemonic ~a" ints)
  (flet ((to-word (i)
	   (elt wordlist i)))
    (let ((result (map 'list #'to-word ints)))
      (v:debug '(:shamir) "mnemonics ~a" result)
      result)))

(defun groups (shares)
  "returns a list of (index, thresholds and sizes) of the groups"
  (let ((group-max-index (make-hash-table)))

    ;; collect a list of max member index within every group
    (dolist (s shares)
      (when (> (share-member-index s)
	     (gethash (share-group-index s)  group-max-index -1))
	(setf (gethash (share-group-index s) group-max-index)
	      (share-member-index s))))

    
    (remove-duplicates
     
     (sort ;; sort by group index

      (map 'list ;; extract information
	   (lambda (s)
	     (list
	      (share-group-index s)
	      (share-member-threshold s)
	      (+ 1 (gethash (share-group-index s) group-max-index))))
	   shares)
      #'<
      :key #'car)
     
     :key #'car)))


;;;; CONTRACTS
;; needed to enable contracts annontations
(cl-annot:enable-annot-syntax)

(flet ((all-equal (func list)
	 (apply #'= (map 'list func list))))

  @precondition :same-identifier-on-shares
  (defmethod combine-shares ((shares list) passphrase)
      "all shares should have the same identifier"
      (all-equal #'share-identifier shares))

  @precondition :same-exponent-on-shares
  (defmethod combine-shares ((shares list) passphrase)
      "they should all share the same exponent"
      (all-equal #'share-exponent shares))
  
  @precondition :same-group-threshold
  (defmethod combine-shares ((shares list) passphrase)
      "They should all share the same group-threshold"
      (all-equal #'share-group-threshold shares))

  @precondition :same-group-count
  (defmethod combine-shares ((shares list) passphrase)
      "They should all share the smae group-count"
      (all-equal #'share-group-count shares))
  
  @precondition :same-threshold
  (defmethod combine-group-shares ((shares list))
      "7. all members within group should have the same member threshold."
      (all-equal #'share-member-threshold shares)))


@precondition :threshold
(defmethod combine-group-shares ((shares list))
  "6. The number of member shares within group should be bigger than the
    member threshold."
  (= (length shares)
     (share-member-threshold (car shares))))

@precondition :unique-member-indexes
(defmethod combine-group-shares ((shares list))
  "Make sure we have unique member shares, and have not entered the same
share twice."
  (= (length
      (remove-duplicates
       (sort (map 'list #'share-member-index shares) #'<)))
     (length shares)))

@precondition :share-value-longer-than-128-FIXME
(defmethod combine-group-shares ((shares list))
  t)

@precondition :zero-padding-bits-FIXME
(defmethod combine-group-shares ((shares list))
  t)

@precondition :group-threshold-less-than-group-count
(defmethod combine-shares ((shares list) passphrase)
  "Group threshold need to be less than group count"
  (<= (share-group-threshold (car shares))
      (share-group-count (car shares))))

@precondition :only-zero-padding-bits-FIXME
(defmethod combine-shares ((shares list) passphrase)
  "FIXME The padding bits in value should only be zeros"
  t)

@postcondition :digest-match
(defmethod recover-secret (threshold x &rest shares)
  "Compute D = Interpolation(254, [(x1, y1), ... , (xm, ym)]).

Let R be the last n − 4 bytes of D. If the first 4 bytes of
HMAC-SHA256(key=R, msg=S) are equal to the first 4 bytes of D, then
return S, otherwise abort."
  (let ((digest-share  (apply #'interpolate +DIGEST-INDEX+ shares)))
    (let* ((digest (subseq digest-share 0 +DIGEST-LENGTH-BYTES+))
	   (random-part (subseq digest-share +DIGEST-LENGTH-BYTES+))
	   (digest-calculated (subseq (create-digest random-part (cl-software-wallet:results))
				      0 +DIGEST-LENGTH-BYTES+)))
      (equalp digest digest-calculated))))

@precondition :threshold-is-not-bigger-than-parts
(defmethod split-secret ((threshold integer) (N integer) S)
  "threshold should be at least 1 and not more than 16, and it need to
lower than N"
  (and
   (< 0 threshold)
   (<= threshold N 16)))

@precondition :secret-less-64-bytes
(defmethod split-secret (threshold N (s array))
  "S should be at most 32 bytes"
  (and
   (typep S 'array)
   (<= (length S) 64)))

@postcondition :returns-a-list
(defmethod split-secret (threshold N s)
  "Should return a list."
  (typep (results) 'list))

@precondition :valid-checksum
(defmethod ints->share ((ints array))
  "checks that the checksum is valid"
  (rs1024-verify-checksum "shamir" ints))

@precondition :valid-padding
(defmethod ints->share ((ints array))
  "checks that the ints have correct padding"
  (let ((padding-bits (integer->bit-vector (aref ints 4) 10)))
    (case (length ints)
      ;; 2 bits pad
      (20 (and (= 0 (bit padding-bits 0))
	       (= 0 (bit padding-bits 1))))
      ;; 4 bits pad
      (33 (and (= 0 (bit padding-bits 0))
	       (= 0 (bit padding-bits 1))
	       (= 0 (bit padding-bits 2))
	       (= 0 (bit padding-bits 3))))
      ;; 8 bits padding
      (59 (and (= 0 (bit padding-bits 0))
	       (= 0 (bit padding-bits 1))
	       (= 0 (bit padding-bits 2))
	       (= 0 (bit padding-bits 3))
	       (= 0 (bit padding-bits 4))
	       (= 0 (bit padding-bits 5))
	       (= 0 (bit padding-bits 6))
	       (= 0 (bit padding-bits 7))))
      (otherwise nil))))

@postcondition :returns-a-share
(defmethod ints->share ((ints array))
  "check that we return a share"
  (typep (results) 'share))

@postcondition :returns-a-int-array
(defmethod share->ints (share)
  "We return an array of ints."
  (typep (results) `(vector (integer 0 ,(expt 2 10)))))

@precondition :length-of-mnemonic
(defmethod mnemonic->ints ((words list) wordlist)
  "WORDS need to be either 20 or 33 words"
  (or (= (length words) 20)
      (= (length words) 33)
      (= (length words) 59)))

@precondition :ints-should-be-less-than-length-of-wordlist
(defmethod ints->mnemonic ((ints array) (wordlist list))
  "All ints in INTS need to be less than size of WORDLIST"
  (flet ((less-than-wordlist (i)
	   (< i (length wordlist))))
    (every #'less-than-wordlist ints)))

