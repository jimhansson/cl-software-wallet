
(in-package #:cl-software-wallet.hdkey)

;; we are still developing this, prefer to have easy to debug.
(declaim (optimize (debug 3)))


;; we could hide these within a let that extends over the defun:s and the defmacro, could
;; also hide this in another internal file so it is not somewhere the user would look
;; directly, but only when it is necessary.
(defparameter mainnet-key-types '() "Used in mainnet-key?, filled by declare-key-types")
(defparameter testnet-key-types '() "Used in testnet-key?, filled by declare-key-types")
(defparameter private-key-types '() "Used in private-key?, filled by declare-key-types")
(defparameter public-key-types  '() "Used in public-key?, filled by declare-key-types")

(defparameter private->public-key-types '()
  "mapping of private too public key-types, filled by declare-key-types")

(defun private->public-key-type (priv)
  (let ((pair (assoc priv private->public-key-types)))
    (unless (cdr pair)
      (error "could not find matching public key type."))
    (cdr pair)))


(defmacro declare-key-types (network &optional
                                       (mainnet-private nil) (mainnet-public nil)
                                       (testnet-private nil) (testnet-public nil))
  "Used to easy setup the constants used to identify different kind of keys of different
        networks, this will allow the predicates like mainnet-key? and public-key? to work.
  this also creates a predicate for testing if a key belong to this network"
  
  ;; builds a predicate like <network>-key?
  (let ((predicate-name (intern (concatenate 'string
                                             (write-to-string network)
                                             (symbol-name :-key?))))
        ;; all the key types the predicate should match on
        (keys (remove 'nil (list mainnet-private mainnet-public
                                 testnet-private testnet-public))))
    (remove 'nil
            (list 'progn
                  
                  ;; this is in here because we want this to be run at the rigth time, we
                  ;; need to have it be part of what we return.
                  (if mainnet-private
                      `(setq private->public-key-types (acons ,mainnet-private
                                                              ,mainnet-public
                                                              private->public-key-types))
                      nil)
                  (if testnet-private
                      `(setq private->public-key-types (acons ,testnet-private
                                                              ,testnet-public
                                                              private->public-key-types))
                      nil)
                  
                  
                  `(defun ,predicate-name (hdkey)
                     "Predicate to test if key belong to this network"
                     (check-type hdkey hdkey)
                     (member (hdkey-type hdkey) ',keys))
                  
                  `(export ',predicate-name)
                  
                  ;; the let:s within these if statments could be either a macrolet or flet
                  (if mainnet-private
                      (let* ((name (intern (concatenate 'string (symbol-name :+) (write-to-string network) (symbol-name :-mainnet-private+)))))
                        `(progn (defconstant ,name ,mainnet-private)
                                (export ',name)
                                (push ,mainnet-private mainnet-key-types)
                                (push ,mainnet-private private-key-types))))
                  (if mainnet-public
                      (let ((name (intern (concatenate 'string (symbol-name :+) (write-to-string network) (symbol-name :-mainnet-public+)))))
                        `(progn (defconstant ,name ,mainnet-public)
                                (export ',name)
                                (push ,mainnet-public mainnet-key-types)
                                (push ,mainnet-public public-key-types))))
                  (if testnet-private
                      (let ((name (intern (concatenate 'string (symbol-name :+) (write-to-string network) (symbol-name :-testnet-private+)))))
                        `(progn (defconstant ,name ,testnet-private)
                                (export ',name)
                                (push ,testnet-private testnet-key-types)
                                (push ,testnet-private private-key-types))))
                  (if testnet-public
                      (let ((name (intern (concatenate 'string (symbol-name :+) (write-to-string network) (symbol-name :-testnet-public+)))))
                        `(progn (defconstant ,name ,testnet-public)
                                (export ',name)
                                (push ,testnet-public testnet-key-types)
                                (push ,testnet-public public-key-types))))))))


(defconstant +private-key-length+ 32)
(defconstant +public-key-length+ 33)
(defconstant +chain-code-length+ 32)

(defconstant +max-child-idx+ (expt 2 32))

(defparameter +master-fingerprint+ (make-byte-array 4)
  "Defined as a parameter because of SBCL insistance on being a pain in the ***")

(defun bip32-hash (cc idx data)
  (declare (type CC cc)
	   (type child-idx idx))
  ;; TODO: reuse mac object by reinit.
  (assert (= 33 (length data)))
  (check-type cc CC) ; we also declare this!
  (let ((mac (make-hmac cc 'ironclad:SHA512))
	(num (integer-to-octets idx :big-endian 't :n-bits 32))
	(total-length (+ 4 (length data))))
    (update-hmac mac (concatenate `(byte-array ,total-length) data num))
    (hmac-digest mac)))
