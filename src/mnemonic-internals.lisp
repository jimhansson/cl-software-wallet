
(in-package #:cl-software-wallet.mnemonic)


(defun sha256 (byte-sequence)
  (ironclad:digest-sequence :sha256 byte-sequence))

(defun bits-to-bytes (bits)
  (/ bits 8))

(defun byte-array-to-int (byte-array)
  (parse-integer (ironclad:byte-array-to-hex-string byte-array)
                 :radix 16))

(defun int-to-byte-array (int)
  (ironclad:integer-to-octets int))

(defun cs-position (ent+cs-bit-string)
  (* (/ (length ent+cs-bit-string) 33) 32))
