;;;; package.lisp


(defpackage #:cl-software-wallet
  (:documentation "Contains functions for implementing a software-based bitcoin wallet. it
	uses libsecp256k1 from bitcoin-core to do all the cryptographic functions and	ironclad.")
  (:use #:cl
	#:ironclad
	#:secp256k1)
  (:shadowing-import-from #:cl #:null)
  (:export #:publickey->address
	   #:sha256sum-verify
	   #:sha256sum-calculate
	   #:sha256sum-append
	   #:base58-encode
	   #:base58-decode
	   #:wif-encode
	   #:wif-decode
	   #:hash160
	   ;; transaction
	   #:<tx>
	   #:<tx-in>
	   #:<tx-out>
	   #:tx-version
	   #:tx-in
	   #:tx-out
	   #:tx-witness
	   #:script
	   #:tx-lock-time
	   #:read-tx
	   #:read-tx-out
	   #:to-bytes
	   ;; these below are good candidates for a utility library
	   ;; contracts
	   #:dbc
	   #:with-contracts-enabled
	   #:with-contracts-disabled
	   #:enabled-contracts
	   #:enable-contracts
	   #:results
	   #:+by-default-check-precondition+
	   #:+by-default-check-postcondition
	   #:+by-default-check-invariant+
	   #:+by-default-match-all-conditions+ ;; might be removed
	   #:precondition
	   #:postcondition
	   #:invariant
	   ;; end of contracts
	   #:byte-array
	   #:var-byte-array
	   #:make-byte-array
	   #:concatenate-byte-array
	   #:hex-string-to-octets
	   #:octets-to-string
	   #:octets-to-hex-string
	   #:pprint-octets
	   #:pprint-octets-hex
	   #:converts-bits
	   #:bit-vector->integer
	   #:integer->bit-vector
	   #:read-compact-integer
	   #:serialize-compact-integer))

(defpackage #:cl-software-wallet.script
  (:documentation "functions for parsing and serializing scripts from/into bytes. hope to
	one day have an interpreter and verifier also.")
  (:use #:cl
	#:cl-software-wallet)
  (:export #:serialize-script
	   #:serialize-p2pkh
	   #:serialize-p2pk
	   #:parse-script
	   #:execute
	   #:execute/step
	   #:script-error
	   #:disabled-operation
	   #:unknown-operation
	   #:<script>
	   #:parse-and-classify-script))


(defpackage #:cl-software-wallet.hdkey
  (:documentation "Contains an implementation of BIP32(Hierarchical Deterministic Wallets)")
  (:nicknames "bip32")
  (:use #:cl
	#:ironclad
	#:secp256k1
	#:cl-software-wallet)
  (:shadowing-import-from #:cl #:null)
  (:export #:hdkey
	   #:hdkey-type
	   #:hdkey-depth
	   #:hdkey-master-fingerprint
	   #:hdkey-child-number
	   #:hdkey-chain-code
	   #:hdkey-key
	   #:getpublickey
	   #:private-key?
	   #:public-key?
	   #:mainnet-key?
	   #:testnet-key?
	   #:bitcoin-key?
	   #:litecoin-key?
	   #:dogecoin-key?
	   #:jumbucks-key?
	   #:derive
	   #:derive-private
	   #:derive-public
	   #:private->public
	   #:derive-by-path
	   #:from-seed
	   #:hardened
	   #:write-hdkey
	   #:write-hdkey-bytes
	   #:parse-hdkey
	   #:parse-hdkey-bytes
	   #:+bitcoin-mainnet-private+
	   #:+bitcoin-mainnet-public+
	   #:+bitcoin-testnet-private+
	   #:+bitcoin-testnet-public+
	   #:+dogecoin-mainnet-private+
	   #:+dogecoin-mainnet-public+
	   #:+dogecoin-testnet-private+
	   #:+dogecoin-testnet-public+
	   #:+jumbucks-mainnet-private+
	   #:+jumbucks-mainnet-public+
	   #:+litecoin-mainnet-private+
	   #:+litecoin-mainnet-public+
	   #:+litecoin-testnet-private+
	   #:+litecoin-testnet-public+))

(defpackage #:cl-software-wallet.mnemonic
  (:documentation "functions for creating and parsing BIP39 mnemonics.")
  (:nicknames "bip39")
  (:use #:cl
	#:ironclad
	#:cl-software-wallet)
  (:shadowing-import-from #:cl #:null)
  (:export #:english-wordlist
	   #:*wordlist*
	   #:generate-mnemonic
	   #:mnemonic?
	   #:convert-to-seed))

(defpackage #:cl-software-wallet.shamir
  (:documentation "")
  (:nicknames "slip39")
  (:use #:cl
	#:ironclad
	#:cl-software-wallet
	#:cl-software-wallet.mnemonic)

  (:shadowing-import-from #:cl #:null)
  
  (:export #:mnemonic->ints
	   #:ints->mnemonic
	   #:ints->share
	   #:share->ints
	   #:mnemonic->share
	   #:share->mnemonic

	   #:groups
	   #:groups-thresholds
	   #:groups-sizes

	   #:combine-shares
	   #:generate-shares

	   #:share
	   #:share-identifier
	   #:share-group-threshold
	   #:share-exponent
	   #:share-group-index
	   #:share-group-count
	   #:share-member-index
	   #:share-member-threshold
	   #:share-share-value))

(defpackage #:cl-software-wallet.psbt
  (:nicknames "psbt" "bip174")
  (:use #:cl
	#:cl-software-wallet
	#:cl-software-wallet.script
	#:ironclad)
  (:shadowing-import-from #:cl #:null)
  (:export #:global-map
	   #:input-list
	   #:output-list
	   #:find-by-key-type
	   #:find-by-key
	   #:parse-psbt
	   #:write-psbt))

(defpackage #:cl-software-wallet.bech32
  (:documentation "")
  (:nicknames "bip173")
  (:use #:cl
	#:cl-software-wallet)
  (:export #:bech32-hrp-expand
	   #:bech32-polymod
	   #:bech32-encode
	   #:bech32-decode))

(defpackage #:cl-software-wallet.golomb
  (:documentation "")
  (:nicknames "golomb")
  (:use #:cl
	#:cl-software-wallet))
