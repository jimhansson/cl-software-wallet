(in-package #:cl-software-wallet.bech32)

;; this implement functions for BIP-173:

(defvar *bech32-alphabet* "qpzry9x8gf2tvdw0s3jn54khce6mua7l")

(defun bech32-char-to-value (chr)
  (position chr *bech32-alphabet*))

(defun bech32-checksum-verify (hrp data checksum)
  (= (bech32-polymod (concatenate 'vector
                                  (bech32-hrp-expand hrp)
                                  data
                                  checksum))
     1))

;; we have polymod in both bech32 and in shamir, it could be replaced
;; with a generic version in util instead.
(defun bech32-polymod (data)
  (let ((gen #(#x3b6a57b2 #x26508e6d #x1ea119fa #x3d4233dd #x2a1462b3))
        (chk 1))
    (loop for v across data
       do (let ((b (ash chk -25)))
            (setq chk (logxor (ash (logand chk #x1ffffff) 5) v))
            (dotimes (i 5)
              (unless (= 0 (logand (ash b (- 0 i)) 1))
                (setq chk (logxor chk (aref gen i))))))
       finally (return chk))))

(defun bech32-hrp-expand (s)
  (declare (optimize (debug 3)))
  (concatenate '(vector (unsigned-byte 8))
               (loop for x across s collect (ash (char-code x) -5))
               #(#x0)
               (loop for x across s collect (logand (char-code x) 31))))

(defun bech32-encode (hrp data)
  (flet ((to-8-bits (bits)
           (let ((fill-bits (make-array ( - 8 (length bits))
                                        :element-type 'bit
                                        :initial-element 0)))
             (concatenate '(bit-vector) fill-bits bits)))
	 
         (to-5-bits (bits)
           (let ((number-of-groups (floor (/ (length bits) 5)))
                 (rest (mod (length bits) 5)))
             (loop
                for i from 1 to number-of-groups
                collect (subseq bits (* 5 (- i 1)) (* 5 i))))))
    (let* ((bits
            (apply #'concatenate 'bit-vector
                   (map 'list #'to-8-bits
                        (map 'list #'integer->bit-vector
                             data))))
           (bits2 (to-5-bits bits)))
      (map 'string
           (lambda (x)
             (elt *bech32-alphabet* (bit-vector->integer x)))
           bits2))))

(defun bech32-decode (str)
  (labels ((lower-case-or-digit? (chr)
             (or (lower-case-p chr) (digit-char-p chr))))
    (declare (optimize (debug 3)))
    (assert (< (length str) 91) nil "bech32 does not support longer lines than 90")
    (assert (every #'lower-case-or-digit? str) nil "string does not contain characters within valid range, only digits and lower case accepted"))
  ;; when 1 is allowed within the Human readable part, the seperator
  ;; is the last 1 within the string.
  (let* ((separator-position (position #\1 str :from-end t))
         ;; the last 6 characters are the checksum.
         (checksum-position (- (length str) 6))
         ;; devide into the different parts.
         (human-part (subseq str 0 separator-position))
         (data-part (subseq str (+ separator-position 1) checksum-position))
         (checksum-part (subseq str checksum-position)))
    (assert (>= separator-position 1) nil "The HRP of bech32 is to short")
    (assert (< separator-position (- (length str) 6)))
    (assert (> (length data-part) 0) nil "Data part is empty in bech32")
    (assert (member human-part '("tb" "bc") :test #'string=) nil "Invalid HRP in bech32")
    (labels ((to-bits (c)
               ;; turns a character into a 5 bit array, using the *bech32-alphabet*
               (let* ((c-value (position c *bech32-alphabet*))
                      (c-bits (integer->bit-vector c-value))
                      (fill-bits (make-array (- 5 (length c-bits))
                                             :element-type 'bit
                                             :initial-element 0)))
                 (let ((arr (concatenate '(bit-vector) fill-bits c-bits)))
                   #+assert (assert (= 5 (length arr)))
                   arr)))
             (bits->bytes (bits-list)
               (let* ((bits (apply #'concatenate '(bit-vector) bits-list))
                      (number-of-bytes (floor (/ (length bits) 8)))
                      (rest (mod (length bits) 8)))
                 (assert (<= rest 4) nil "we have to much padding 0")
                 (assert (= (reduce #'+ (subseq bits (* number-of-bytes 8))) 0) nil "non zero pads")
                 (loop ;; turn bits into bytes
                    for i from 1 to number-of-bytes
                    collect
                      (subseq bits (* 8 (- i 1)) (* 8 i))))))
      (let* ((data-bits-list (loop for x across data-part
                                collect (to-bits x)))
             (bits-as-integers (loop for x in data-bits-list
                                  collect (bit-vector->integer x)))
             (checksum-list (loop for x across checksum-part
                               collect (to-bits x)))
             (checksum-as-integers (loop for x in checksum-list
                                      collect (bit-vector->integer x))))
        (assert (bech32-checksum-verify human-part bits-as-integers checksum-as-integers)
                nil "Invalid checksum in bech32.")
        (list human-part
	      ;; the first bit-vector is the winess version and should be handled in a special way.
	      (coerce (map 'vector #'bit-vector->integer (bits->bytes (cdr data-bits-list)))
		      '(vector (unsigned-byte 8)))
	      ;; here we return the witness version
	      (car data-bits-list)
	      checksum-part)))))


