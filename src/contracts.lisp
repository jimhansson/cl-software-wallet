
(in-package :cl-software-wallet)

;;; notes about compiling flags.
;;; maybe we should do the reverse instead and let user push disable
;;; flags onto cl:*features*, so the default is active and users of
;;; the code have to explicit disable it. Quid pro quo takes that
;;; route.
(eval-when (:compile-toplevel :load-toplevel :execute)
  (pushnew :dbc-precondition-checks cl:*features*)
  (pushnew :dbc-postcondition-checks cl:*features*)
  (pushnew :dbc-invariant-checks cl:*features*))

(defparameter +debug-contracts+ nil)

;;; these parameters setts the default values for the arguments for
;;; the dbc method combination, you can change the globally if you
;;; know that you are not maybe using invariants at all.
(defparameter +by-default-check-precondition+ t)
(defparameter +by-default-check-postcondition+ t)
(defparameter +by-default-check-invariant+ t)

;;; or if we should do all that matches, when set to true we will run
;;; all contracts that are matching. This allows for a style where you
;;; can have different contracts validating different parameters of a
;;; method. Not used at this point, take a look on old dbc
;;; method-combination to find out how to implement.
(defparameter +by-default-match-all-conditions+ t)

;;; CONDITIONS
;;; some different conditions we will throw
(define-condition contract-violation-error (error)
  ((method-fn :reader method-fn
	   :initarg :method-fn)
   (contract :reader contract
	      :initarg :contract))
  (:report (lambda (condition stream)
	     (format stream
		     "Contract violation in ~A on contract ~A."
		     (method-fn condition)
		     (contract condition)))))

(define-condition precondition-error (contract-violation-error) ()
  (:report (lambda (condition stream)
	     (format stream
		     "Pre contract violation in ~A on contract ~A."
		     (method-fn condition)
		     (contract condition)))))

(define-condition postcondition-error (contract-violation-error) ()
  (:report (lambda (condition stream)
	     (format stream
		     "Post contract violation in ~A on contract ~A."
		     (method-fn condition)
		     (contract condition)))))

(define-condition invariant-error (contract-violation-error) ()
  (:report (lambda (condition stream)
	     (format stream
		     "Invariant contract violation in ~A on contract ~A."
		     (method-fn condition)
		     (contract condition)))))

(define-condition pre-invariant-error (invariant-error) ()
  (:report (lambda (condition stream)
	     (format stream
		     "Pre invariant contract violation in ~A on contract ~A."
		     (method-fn condition)
		     (contract condition)))))

(define-condition post-invariant-error (invariant-error) ()
  (:report (lambda (condition stream)
	     (format stream
		     "Post invariant contract violation in ~A on contract ~A."
		     (method-fn condition)
		     (contract condition)))))

(define-condition creation-invariant-error (invariant-error) ()
  (:report (lambda (condition stream)
	     (format stream
		     "Creation invariant contract violation in ~A on contract ~A."
		     (method-fn condition)
		     (contract condition)))))

;;; LOCAL VARIABLES
;;; These are used to turn on or off contracts on a global, the
;;; with-contracts-enabled/disabled methods are just turning this on
;;; and off. Could be set when you know that just locally for this
;;; part You dont want to run contracts. 
(defvar +check-preconditions-p+ t)
(defvar +check-postconditions-p+ t)
(defvar +check-invariants-p+ t)

(defvar +inside-contract-p+ nil "Used internally to signal when
we are executing contract code and avoid running other contracts.")

(defvar %results () "Used to store the result of the main function and
is returned by the results function so you can inspect the result in
the postconditions.")

;;; UTILITY FUNCTIONS
;;;
(defun enabled-contracts ()
  "Returns a list of arguments suitable to APPLYing to ENABLE-CONTRACTS."
  (list :invariants
        #+dbc-invariant-checks-disabled nil
        #-dbc-invariant-checks-disabled +check-invariants-p+
        :preconditions
        #+dbc-precondition-checks-disabled nil
        #-dbc-precondition-checks-disabled +check-preconditions-p+
        :postconditions
        #+dbc-postcondition-checks-disabled nil
        #-dbc-postcondition-checks-disabled +check-postconditions-p+))

(defun enable-contracts (&key
			   (invariants nil invp)
			   (preconditions nil prep)
			   (postconditions nil postp))
  "Enables or disables each contract type that is provided. If none is provided,
   no change is made."
  (when invp  (setf +check-invariants-p+ invariants))
  (when prep  (setf +check-preconditions-p+ preconditions))
  (when postp (setf +check-postconditions-p+ postconditions)))

(defun disable-contracts ()
  "A shorthand for disabling all contracts."
  (enable-contracts :invariants nil :preconditions nil :postconditions nil))

(defmacro with-contracts-enabled
    ((&rest args &key invariants preconditions postconditions) &body body)
  "Enables/disables contracts for the extent of this form, restoring them to
   their prior values upon exit."
  (declare (ignore invariants preconditions postconditions))
  (let ((enabled-contracts (gensym "ENABLED-CONTRACTS-")))
    `(let ((,enabled-contracts (enabled-contracts)))
       (unwind-protect
            (progn (enable-contracts ,@args)
                   ,@body)
         (apply #'enable-contracts ,enabled-contracts)))))

(defmacro with-contracts-disabled (() &body body)
  "A shorthand for disabling all contracts for the extent of this form."
  (let ((enabled-contracts (gensym "ENABLED-CONTRACTS-")))
    `(let ((,enabled-contracts (enabled-contracts)))
       (unwind-protect
            (progn (disable-contracts)
                   ,@body)
         (apply #'enable-contracts ,enabled-contracts)))))

;; we should make this only available in postconditions
(defun results ()
  (values-list %results))

;; stolen from annot
(defun macrop (symbol)
  "Return non-nil if SYMBOL is a macro."
  (and (symbolp symbol)
       (macro-function symbol)
       t))

(defun progn-form-last (progn-form)
  "Return the last form of PROGN-FORM which should be evaluated at
last. If macro forms seen, the macro forms will be expanded using
MACROEXPAND-UNTIL-NORMAL-FORM."
  (let ((progn-form (macroexpand-until-normal-form progn-form)))
    (if (and (consp progn-form)
             (eq (car progn-form) 'progn))
        (progn-form-last (car (last progn-form)))
        progn-form)))

(defun definition-form-type (definition-form)
  "Return the type of DEFINITION-FORM."
  (let* ((form (progn-form-last definition-form))
         (type (when (consp form)
                 (car form))))
    type))

(defun macroexpand-until-normal-form (form)
  "Expand FORM until it brecomes normal-form."
  (if (and (consp form)
           (macrop (car form))
           (let ((package (symbol-package (car form))))
             (and package
                  (member package
                          (list (find-package :cl)
                                #+clisp (find-package :clos))))))
      (values form nil)
      (multiple-value-bind (new-form expanded-p)
          (macroexpand-1 form)
        (if (or (not expanded-p) (null new-form))
            (values form nil)
            (values (macroexpand-until-normal-form new-form) t)))))

(defun progn-form-replace-last (last progn-form)
  "Replace the last form of PROGN-FORM with LAST. If LAST is a
function, the function will be called with the last form and used for
replacing. If macro forms seen, the macro forms will be expanded using
MACROEXPAND-UNTIL-NORMAL-FORM."
  (let ((progn-form (macroexpand-until-normal-form progn-form)))
    (if (and (consp progn-form)
             (eq (car progn-form) 'progn))
        `(,@(butlast progn-form)
            ,(progn-form-replace-last last (car (last progn-form))))
        (if (functionp last)
            (funcall last progn-form)
            last))))
;; end theft from annot


;; fix these so it checks that we are using dbc method combination
(annot:defannotation precondition (contract-name definition-form)
    (:arity 2 :inline t)
  "Creates a pre-condition on a method by name CONTRACT-NAME"
  (progn-form-replace-last
   (lambda (definition-form)
     (case (definition-form-type definition-form)
       ((defmethod)
	;; TODO check that it has right method combination
        (destructuring-bind (def fname arg . body)
            definition-form
          `(,def ,fname :precondition ,contract-name ,arg ,@body)))
       (t (error "pre-condition not supported: ~a"
                 definition-form))))
   definition-form))

(annot:defannotation postcondition (contract-name definition-form)
    (:arity 2 :inline t)
  "Creates a post-condition on a method by name CONTRACT-NAME"
  (progn-form-replace-last
   (lambda (definition-form)
     (case (definition-form-type definition-form)
       ((defmethod)
	;; TODO check that it has right method combination
        (destructuring-bind (def fname arg . body)
            definition-form
          `(,def ,fname :postcondition ,contract-name ,arg ,@body)))
       (t (error "post-condition not supported: ~a"
                 definition-form))))
   definition-form))


(annot:defannotation invariant (contract-name definition-form)
    (:arity 2 :inline t)
  "Creates a invariant-condition on a method by name CONTRACT-NAME"
  (progn-form-replace-last
   (lambda (definition-form)
     (case (definition-form-type definition-form)
       ((defmethod)
	;; TODO check that it has right method combination
        (destructuring-bind (def fname arg . body)
            definition-form
          `(,def ,fname :invariant ,contract-name ,arg ,@body)))
       (t (error "invariant-condition not supported: ~a"
                 definition-form))))
   definition-form))

(define-method-combination dbc
    ;; there is a problem with this contract, what is the order when
    ;; specialisers are the same.
    (&key
     (precondition-check +by-default-check-precondition+)
     (postcondition-check +by-default-check-postcondition+)
     (invariant-check +by-default-check-invariant+))

  ;; collect different method fragments into buckets
  ((methods * :required t))
  (:generic-function gf)
  (loop with after
     for method in methods
     for (qualifier) = (method-qualifiers method)
     if (eq qualifier :around) collect method into around
     else if (eq qualifier :before) collect method into before
     else if (eq qualifier :after) do (push method after)
     else if (eq qualifier :precondition) collect method into precondition
     else if (eq qualifier :postcondition) collect method into postcondition
     else if (eq qualifier :invariant) collect method into invariant
     else collect method into primary
     finally (flet ((get-qualifiers (methods ignored)
		      (map 'list
			   (lambda (qualifiers)
			     (remove ignored qualifiers))
			   (map 'list #'method-qualifiers methods))))
	       (when +debug-contracts+
		 (format t "generic-function ~a ~%" gf)
		 (if primary
		     (format t "primary ~a ~%" primary)
		     (format t "NO PRIMARY FUNCTION FOUND~%"))
		 (when around (format t "around: ~a ~%" around))
		 (when before (format t "before: ~a ~%" before))
		 (when after (format t "after: ~a ~%" after))
		 (when precondition (format t "preconditions: ~{~a ~}~%"
					    (get-qualifiers precondition :precondition)))
		 (when postcondition (format t "postconditions: ~{~a ~}~%"
					     (get-qualifiers postcondition :postcondition)))
		 (when invariant (format t "invariants: ~{~a ~}~%"
					 (get-qualifiers invariant :invariant)))
		 (format t "------------------------- ~%")))
       (return
	 (if (and (not primary)
		  (not around)
		  (not before)
		  (not after))
	     (progn
	       ;; this declaim might be to wide and hide other style errors need to look into that
	       (declaim (sb-ext:muffle-conditions style-warning))
	       `(progn
		  ;; If we dont have any of the main methods, then we
		  ;; should not do any of the contracts either.
		  ;; should dump the argument types here.
		  (method-combination-error "No primary method for generic function. ~a" ,gf)))
	     ;; else we return the combined functions
	     (flet ((call-methods (methods)
		      (mapcar (lambda (method) `(call-method ,method)) methods))
		    
		    (check-contracts (contracts error-type)
		      ;; could we return the setq of inside-contracts here?
		      (loop for contract in contracts
			 collect `(restart-case
				      (unless (call-method ,contract)
					(error ',error-type
					       :method-fn ,gf
					       :contract (second (method-qualifiers ,contract))
					       :description (second (method-qualifiers ,contract))))
				    (continue ()
				      :report "Ignore contract violation.")))))
	       
	       (let* ((inner-form
		       (if (or before after)
			   `(multiple-value-prog1
				(progn
				  ,@(call-methods before)
				  (call-method ,(first primary) ,(rest primary)))
			      ,@(call-methods (reverse after)))		       
			   `(call-method ,(first primary) ,(rest primary))))
		      
		      (outer-form
		       (if around
			   `(call-method ,(first around) (,@(rest around) (make-method ,inner-form)))
			   inner-form))

		      #+:dbc-precondition-checks
		      (pre-form
		       `(progn
			  (when (and ,precondition-check
				     +check-preconditions-p+
				     (not +inside-contract-p+))
			    (let ((+inside-contract-p+ t))
			      ,@(check-contracts (reverse precondition) 'precondition-error)))
			  ,outer-form))
		      #-:dbc-precondition-checks
		      (pre-form outer-form)

		      #+:dbc-postcondition-checks
		      (post-form
		       `(let ((%results (multiple-value-list ,pre-form)))
			  (when (and ,postcondition-check
				     +check-postconditions-p+
				     (not +inside-contract-p+))
			    (let ((+inside-contract-p+ t))		    
			      ,@(check-contracts (reverse postcondition) 'postcondition-error)))
			  (results)))
		      #-:dbc-postcondition-checks
		      (post-form pre-form)

		      #+:dbc-invariant-checks
		      (invariant-form
		       `(progn
			  (when (and ,invariant-check
				     +check-invariants-p+
				     (not +inside-contract-p+))
			    (let ((+inside-contract-p+ t))
			      ,@(check-contracts (reverse invariant) 'invariant-error)))
			  (let ((%results (multiple-value-list ,post-form)))
			    (when (and ,invariant-check
				       +check-invariants-p+
				       (not +inside-contract-p+))
			      (let ((+inside-contract-p+ t))
				,@(check-contracts (reverse invariant) 'invariant-error)))
			    (results))))
		      #-:dbc-invariant-checks
		      (invariant-form post-form))

		 invariant-form))))))



#||
;; this whould recquire adding closer-mop, do i want to do that?
(use-package :closer-mop)

(defun compose (&rest functions)
  "Compose FUNCTIONS right-associatively, returning a function"
  #'(lambda (x)
      (reduce #'funcall functions
              :initial-value x
              :from-end t)))

(defmethod documentation :around ((x standard-generic-function) doc-type)
  (declare (ignore doc-type))
  (format nil "~@[~A~]~@[~&guarantees:~%* ~A~]"
          (call-next-method)
          (let ((method (find-if (lambda (method)
                                   (and (eq :guarantee
                                            (car (method-qualifiers method)))
                                        (every (lambda (specializer)
                                                 (eq (find-class t)
                                                     specializer))
                                               (method-specializers method))))
                                 (generic-function-methods x))))
            (when method (second (method-qualifiers method))))))

(defmethod documentation :around ((x standard-method) doc-type)
  (declare (ignore doc-type))
  (let ((applicable-methods
         (compute-applicable-methods-using-classes (method-generic-function x)
                                                   (method-specializers x))))
    (let ((precondition (find :require applicable-methods
                              :key (compose #'car #'method-qualifiers)))
          (postconditions (remove :guarantee applicable-methods
                                  :test-not #'eq
                                  :key (compose #'car #'method-qualifiers))))
      (format nil "~@[~A~]~@[~&requires:~%* ~A~]~@[~&guarantees:~{~&* ~A~}~]"
              (call-next-method)
              (when precondition (second (method-qualifiers precondition)))
              (remove nil
                      (mapcar (compose #'second #'method-qualifiers)
                              postconditions))))))
||#

#||


||#
