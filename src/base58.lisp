
(in-package #:cl-software-wallet)

(defparameter +base58-alphabet+
  "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz")
(defparameter +base58-len+
  (length +base58-alphabet+))

(declaim (optimize (debug 3)))

(defun divmod (number divisor)
  (values (floor (/ number divisor))
          (mod number divisor)))

(defun base58-encode (bytes)
  (check-type bytes (vector (unsigned-byte 8)))
  ;; make sure we return a string
  (the string ; maybe not needed because of the concatenate
       ;; the code is dependant on functions defined for strings. so we turn bytes into str.
       (let ((str (octets-to-string bytes)))
	 (let ((value 0) 
	       (rstr (reverse str))
	       (output (make-string-output-stream))
	       (npad 0)) ; The number of leading zeroes that are to be removed
	   
	   (loop for i from 0 to (1- (length str)) do
		(setf value (+ value (* (char-code (elt rstr i))
					(expt 256 i)))))
	   
	   (loop while (>= value +base58-len+) do
		(multiple-value-bind (new-value mod) (divmod value +base58-len+)
		  (setf value new-value)
		  (write-char (elt +base58-alphabet+ mod) output)))
	   (write-char (elt +base58-alphabet+ value) output)
	   
	   ;; Count the leading zeroes
	   (loop for char across str do 
		(if (char-equal char #\Nul)
		    (incf npad)
		    (return)))
	   
	   (concatenate 'string
			(coerce (loop for i from 1 to npad collecting #\1) 'string)
			(reverse (get-output-stream-string output)))))))


(defun base58-decode (str &optional length)
  (check-type str string)
  (let ((value 0)
	(rstr (reverse str))
	(output (ironclad:make-octet-output-stream))
	(npad 0))
    
    (loop for i from 0 to (1- (length str)) do
	 (setf value (+ value (* (position (elt rstr i) +base58-alphabet+)
				 (expt +base58-len+ i)))))
    
    (loop while (>= value 256) do
	 (multiple-value-bind (new-value mod) (divmod value 256)
	   (setf value new-value)
	   (write-byte mod output)))
    (write-byte value output)
    
    (loop for char across str do ;; Count the leading ones
	 (if (char-equal char #\1)
	     (incf npad)
	     (return)))
    
    (setf output (concatenate '(vector (unsigned-byte 8))
			      (loop for i from 1 to npad collecting #\Nul)
			      (reverse (ironclad:get-output-stream-octets output))))
    
    (if (and length (not (eql length (length output))))
				nil
				output)))
