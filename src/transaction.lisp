

(in-package #:cl-software-wallet)


(defvar *mydefun-indent* 0)
(defvar *mydefun-cats* '())

(defmacro mydefun (name lambda-list body)
  (flet ((make-keyword (name) (intern
			       (if (stringp name)
				   name
				   (symbol-name name))
			       "KEYWORD")))
	 
    (let* ((pkg-cat (make-keyword (package-name *package*)))
	   (name-cat (make-keyword name))
	   (cats (list pkg-cat name-cat)))
      `(defun ,name ,lambda-list
	 (let ((*mydefun-cats* '(,@cats)))
	   (v:trace ',cats "Entered with arguments ~a" ,@lambda-list)
	   (incf *mydefun-indent* 2)
	   (let ((result ,body))
	     (decf *mydefun-indent* 2)
	     (v:trace ',cats "Returned ~a" result)
	     result))))))

(defmacro dbg (fmt &rest args)
  `(funcall #'v:debug *mydefun-cats* ,fmt ,@args))

(defmacro trc (fmt &rest args)
  `(funcall #'v:trace *mydefun-cats* ,fmt ,@args))

(defclass <tx> ()
  ((tx-version :initform '())
   (tx-in :initform '())
   (tx-out :initform '())
   (tx-witness :initform nil)
   (tx-lock-time :initform 0)))


(defclass <tx-in> ()
  (previous
   script
   sequence))


(defclass <tx-out> ()
  ((value :initform 0)
   (pk-script :initform '())))

(defmethod print-object ((obj <tx>) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (tx-version tx-in tx-out tx-witness) obj
      (format stream "version: ~a in: ~a out: ~a witness: ~a"
	      tx-version (length tx-in) (length tx-out) (length tx-witness)))))

(defmethod print-object ((obj <tx-in>) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (previous script sequence) obj
      (format stream "previous: ~a script: ~a sequence: ~a"
	      (octets-to-hex-string previous)
	      (octets-to-hex-string script)
	      sequence))))

(defmethod print-object ((obj <tx-out>) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (value pk-script) obj
      (format stream "value: ~a pk-script: ~a"
	      value pk-script))))


;; reuse this in read-tx, and write one for tx-in
(mydefun read-tx-out (vec offset)
  (let ((out (make-instance '<tx-out>))
	(idx offset))
    (with-slots (value pk-script) out
      (setf value (octets-to-integer vec :start idx :end (+ idx 8)))
      (incf idx 8)
      (multiple-value-bind (val consumed)
	  (read-compact-integer vec idx)
	(incf idx consumed)
	(setf pk-script (cl-software-wallet.script:parse-and-classify-script (subseq vec idx (+ idx val))))
	(incf idx val)))
    (values out (- idx offset))))


(mydefun read-tx (vec offset)
  (let ((tx (make-instance '<tx>))
	(idx offset))
    (with-slots (tx-version witness tx-in tx-out tx-witness tx-lock-time)
	tx
      (setf tx-version (subseq vec idx (+ idx 4)))
      (incf idx 4)
      (dbg "version: ~a" tx-version)
      (let ((witness-marker (octets-to-integer vec :start idx :end (+ idx 2)))
	    (tx-in-count 0)
	    (tx-out-count 0))
	(if (= witness-marker 0001)
	    (progn (setf tx-witness t)
		   (incf idx 2))
	    (progn (setf tx-witness nil)))

	(multiple-value-bind (val consumed)
	    (read-compact-integer vec idx)
	  (incf idx consumed)
	  (setf tx-in-count val))
	(trc "expect to read ~a inputs" tx-in-count)
	
	(let ((ins '()))
	  (loop for i from 1 upto tx-in-count
	     do (let ((in (make-instance '<tx-in>)))
		  (with-slots (previous script sequence) in
		    (setf previous (subseq vec idx (+ idx 36)))
		    (incf idx 36)
		    (multiple-value-bind (val consumed)
			(read-compact-integer vec idx)
		      (incf idx consumed)
		      (setf script (subseq vec idx (+ idx val)))
		      (incf idx val))
		    (setf sequence (octets-to-integer vec :start idx :end (+ idx 4)))
		    (incf idx 4))
		  (setf ins (push in ins))))
	  ;; need the reverse to preserve ordering between (de)serialization
	  (setf tx-in (reverse ins)))
	(trc "read ~a inputs, consumed bytes after ~a" (length tx-in) (- idx offset))
	(assert (eq tx-in-count (length tx-in)) nil "could not read the same number of inputs as it said it should have")
	
	(multiple-value-bind (val consumed)
	    (read-compact-integer vec idx)
	  (incf idx consumed)
	  (setf tx-out-count val))
	(trc "expect to read ~a outputs" tx-out-count)

	(let ((outs '()))
	  (loop for i from 1 upto tx-out-count
	     do (let ((out (make-instance '<tx-out>)))
		  (with-slots (value pk-script) out
		    (setf value (octets-to-integer vec :start idx :end (+ idx 8)))
		    (incf idx 8)
		    (multiple-value-bind (val consumed)
			(read-compact-integer vec idx)
		      (incf idx consumed)
		      (setf pk-script (subseq vec idx (+ idx val)))
		      (incf idx val)))
		  (setf outs (push out outs))))
	  ;; need the reverse to preserve ordering between (de)serialization
	  (setf tx-out (reverse outs)))
	(trc "read ~a outputs, consumed bytes after ~a" (length tx-out) (- idx offset))
	(assert (eq tx-out-count (length tx-out)) nil "Could not read the same number of outputs as it said it should have")

	(when tx-witness
	  (trc "Expect to read ~a witness" tx-in-count)
	  (let ((wits '()))
	    (dotimes (in tx-in-count)

	      (let ((wit '()))
		
		(multiple-value-bind (val consumed)
		    (read-compact-integer vec idx)
		  (incf idx consumed)
		  (trc "expect to read ~a witness components" val)

		  ;; read the witness components
		  (loop for i from 1 upto val
		     do (multiple-value-bind (val2 consumed2)
			    (read-compact-integer vec idx)
			  (incf idx consumed2)
			  (setf wit (push (subseq vec idx (+ idx val2)) wit))
			  (incf idx val2))))
		;; append them as one item on the witness list
		(setf wits (push (reverse wit) wits))))
	    ;; reverse and add to the object
	    (setf tx-witness (reverse wits))
	    (trc "read ~a witness" (length tx-witness))))
	(setq tx-lock-time (octets-to-integer vec :start idx :n-bits 32))
	(incf idx 4)))
    
    (trc "consumed ~a bytes" (- idx offset))
    tx))

(defgeneric to-bytes (something))

(defmethod to-bytes :around (something)
  (coerce (call-next-method) '(vector (unsigned-byte 8))))

(defmethod to-bytes ((tx-in <tx-in>))
  (declare (optimize (debug 3) (safety 3)))
  (with-slots (previous script sequence) tx-in
    (assert (= (length previous) 36))
    ;;(format t "to-bytes tx-in ~%")
    (concatenate 'vector
		 previous
		 (serialize-compact-integer (length script))
		 script
		 (integer-to-octets sequence :n-bits 32))))

(defmethod to-bytes ((tx-out <tx-out>))
  (declare (optimize (debug 3) (safety 3)))
  (with-slots (value pk-script) tx-out
    ;;(format t "to-bytes tx-out ~%")
    (let ((serialized-script (cond
			       ((arrayp pk-script)
				pk-script)
			       ((listp pk-script)
				(cl-software-wallet.script:serialize-script pk-script))
			       (t (to-bytes pk-script)))))
      (concatenate 'vector
		   (integer-to-octets value :n-bits 64)
		   (serialize-compact-integer (length serialized-script))
		   serialized-script))))

(defmethod to-bytes :around ((tx <tx>))
  (declare (optimize (debug 3) (safety 3)))
  (flet ((make-keyword (name) (intern
			       (if (stringp name)
				   name
				   (symbol-name name))
			       "KEYWORD")))
    
    (let* ((pkg-cat (make-keyword (package-name *package*)))
	   (name-cat :to-bytes)
	   (cats (list pkg-cat name-cat)))
      (let ((*mydefun-cats* cats))
	(v:trace cats "Entered")
	(let ((result (call-next-method)))
	  (v:trace cats "Returned: ~a" result)
	  result)))))

(defmethod to-bytes ((tx <tx>))
  (declare (optimize (debug 3) (safety 3)))
  (with-slots (tx-version tx-in tx-out tx-witness tx-lock-time) tx
    (assert (= (length tx-version) 4))
    (trc "tx version ~a, witness ~a" tx-version (not (null tx-witness)))
    
    (let ((inputs (apply #'concatenate 'vector
			(loop for x in tx-in
			   collect (to-bytes x))))
	  (outputs (apply #'concatenate 'vector
			  (loop for x in tx-out
			     collect (to-bytes x))))
	  (witness (apply #'concatenate 'vector
			  (loop for witness in tx-witness
			     collect
			       (apply #'concatenate 'vector
				      (serialize-compact-integer (length witness))
				      (loop for witness-comp in witness
					 collect
					   (concatenate 'vector
							(serialize-compact-integer (length witness-comp))
							witness-comp)))))))
      (trc "input bytes ~a, output bytes ~a, witness bytes ~a" (length inputs) (length outputs) (length witness))
      (trc "inputs  ~a" inputs)
      (trc "outputs ~a" outputs)
      (trc "witness ~a" witness)
      (concatenate 'vector
		   tx-version
		   (if tx-witness
		       #(#x00 #x01)
		       #())
		   
		   (serialize-compact-integer (length tx-in))
		   inputs
		   
		   (serialize-compact-integer (length tx-out))
		   outputs
		   
		   (if tx-witness
		       witness
		       #())
		   (integer-to-octets tx-lock-time :n-bits 32)))))
  

