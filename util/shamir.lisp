(in-package #:cl-software-wallet/shamir-tool)


(defparameter +interactive-mode+ t
  "If we should do interactive mode")

;; ------------------------- UTILITIES -----------------------------------
(defun split-by (string chr)
  (loop for start = 0 then (1+ finish)
     for finish = (position chr string :start start)
     collecting (subseq string start finish)
     until (null finish)))

(define-condition required-option (error)
  ((name :initarg :name :reader name)
   (prompt :initarg :prompt :reader prompt)))

(define-condition malformed-option (error)
  ((name :initarg :name :reader name)
   (prompt :initarg :prompt :reader prompt)
   (current-value :initarg :current-value :reader current-value)))

(defmacro with-redo (name &body body)
  `(macrolet ((redo (name)
                `(go ,name)))
     (tagbody
        ,name
        ,@body)))

(defun get-option (long-name prompt validator &key (required nil))
  (let ((option (net.didierverna.clon:getopt :long-name long-name)))
    ;; if missing but required
    (when (and required (null option))
      (restart-case
	(progn
	  (signal 'required-option :name long-name :prompt prompt))
	(use-value-required-option (value)
	  (setq option value))))
    
    (with-redo validation
	(restart-case
	    ;; if malformed
	    (when validator
	      (when (not (funcall validator option))
		(signal 'malformed-option :name long-name
			:prompt prompt :current-value option)))
	  (use-value-malformed-option (value)
	    (setq option value)
	    (redo validation))))
    option))

(defun get-required-option (long-name prompt validator)
  (get-option long-name prompt validator :required t))

(defun ask-for-value-and-invoke-restart (prompt restart)
  (format *query-io* "~a: " prompt)
  (finish-output *query-io*)
  (let ((value (read-line *query-io*)))
    (when restart
      (invoke-restart restart value)
      (format *query-io* "Could not find restart point.~%")
      (finish-output *query-io*)
      value)))

(defun required-option-handler (opt)
  (when +interactive-mode+
    (let ((restart (find-restart 'use-value-required-option)))
      (ask-for-value-and-invoke-restart (prompt opt) restart))))

(defun malformed-option-handler (opt)
  (if +interactive-mode+
      (let ((restart (find-restart 'use-value-malformed-option)))
	(ask-for-value-and-invoke-restart (prompt opt) restart))
      ;; if not interactive just print the name of parameter
      (progn
	(format *query-io* "Malformed option: ~a~%" (name opt))
	(finish-output *query-io*))))


;; ------------------------- split COMMAND -----------------------
(defparameter +split-synopsis+
  (net.didierverna.clon:defsynopsis (:make-default nil :postfix "something else")
    (text :contents "Splits a secret(in hex format) into different parts where a threshold number ~
                     of parts are required to recover the secret.")
    (flag :short-name "h"
	  :long-name "help"
	  :description "Help about the words-to-key command.")
    (stropt :short-name "s"
	    :long-name "secret"
	    :description "Secret you want to split, it should be in hex format and ~
                          be either 128 or 256 bits long")
    (stropt :short-name "p"
	    :long-name "password"
	    :default-value ""
	    :description "Optional password protecting the secret")
    (lispobj :long-name "group-count"
	     :description "number of groups"
	     :typespec '(integer 0 16)
	     :default-value 1)
    (lispobj :long-name "group-threshold"
	     :description "threshold for groups"
	     :typespec '(integer 0 16)
	     :default-value 1)))

(defun split-key ()
  (labels ((secret-validator (secret)
	     ;; TODO: validate it is a hex string
	     (or (= (length secret) 32)
		 (= (length secret) 64))))
    
    (handler-bind
	((required-option #'required-option-handler)
	 (malformed-option #'malformed-option-handler)
	 (t (lambda (e)
	      (format *query-io* "Unhandled error ~a~%" e)
	      (finish-output *query-io*))))
      
      (let* ((secret (get-required-option "secret"
					 "Enter the secret: "
					 #'secret-validator))
	     (password (get-option "password"
				   "Enter password: "
				   nil))
	     (group-count (get-required-option "group-count"
					       "Enter the number of groups you want: "
					       nil))
	     (group-thres (get-required-option "group-threshold"
					       "Number of groups that are needed to unlock secret: "
					       nil))
	     (groups (input-groups group-count)))
	(split-key2 secret password group-count group-thres groups)))))

(defun split-key2 (secret password group-count group-thres groups)
  "will do the actual split, requires all parameters to be ready and
is also used by convert command."
  (declare (ignore group-count))
  (v:debug '(:shamir :tool) "will split the secret ~a~%" secret)
  (v:debug '(:shamir :tool) "using the password ~a~%" password)
  (let ((shares
	 (cl-software-wallet.shamir:generate-shares group-thres
						    groups
						    secret
						    (if password
							password "")
						    ;;fix exponent and identifier
						    0)))
    (dolist (share shares)
	  (format *query-io* "Key: ~a~%" (share->mnemonic share))
	  (format *query-io* "---------------------------------------------------~%"))
	(finish-output *query-io*)
    shares))

(defun input-groups (group-count)
  "ask the user for parameters for a number of groups, return a list
of pairs with threshold and size of group"
  (let ((groups '()))
    (dotimes (g-idx group-count)
      (let ((g-size nil)
	    (g-thres nil))
	(format *query-io* "Enter group size for group ~a: " g-idx)
	(finish-output *query-io*)
	(setq g-size (parse-integer (read-line *query-io*)))
	(format *query-io* "Enter group threshold for group ~a: " g-idx)
	(finish-output *query-io*)
	(setq g-thres (parse-integer (read-line *query-io*)))
	(push `(,g-thres . ,g-size) groups)))
    (reverse groups)))




;; ------------------------- convert COMMAND ------------------------------
(defparameter +convert-wallet-synopsis+
  (net.didierverna.clon:defsynopsis (:make-default nil :postfix "something else")
    (text :contents "Convert a BIP39 seed phrase to a SLIP39 devided seed phrase")
    (flag :short-name "h" :long-name "help" :description "Shows help about the convert command")
    (lispobj :long-name "group-count"
	     :description "number of groups"
	     :typespec '(integer 0 16)
	     :default-value 1)
    (lispobj :long-name "group-threshold"
	     :description "threshold for groups"
	     :typespec '(integer 0 16)
	     :default-value 1)
    (lispobj :short-name "p"
	     :long-name "password"
	     :default-value nil
	     :description "Optional password protecting the secret")))

(defun convert-bip39-to-seed (words password)
  (v:debug '(:shamir :tool) "converting bip39 words into seed.")
  (octets-to-hex-string
   (convert-to-seed words
		    password)))

(defun ask-for-bip39-password ()
  (format *query-io* "Enter the password for the BIP39 mnemonic: ")
  (finish-output *query-io*)
  (read-line *query-io*))

(defun prompt-for-bip39-mnemonic ()
    (format *query-io* "Enter key (or ABORT): ")
    (finish-output *query-io*)
    (let ((word-list (uiop:split-string (read-line *query-io*) :separator " ")))
      
      (when (string= "ABORT" (car word-list))
	(net.didierverna.clon:exit 1))
      word-list))

(defun convert-seed-phrase ()
  "This is used to convert a bip39 seed phrase into a slip39 phrase"
  (handler-bind
      ((required-option #'required-option-handler)
       (malformed-option  #'malformed-option-handler))
    
    (let* ((bip39-words (prompt-for-bip39-mnemonic))
	   (bip39-pass (ask-for-bip39-password))
	   (seed (convert-bip39-to-seed bip39-words bip39-pass))
	   (group-count
	    (get-required-option "group-count"
				 "Enter the number of groups you want: "
				 nil))
	   (group-thres (get-required-option "group-threshold"
					     "Number of groups that are needed to unlock secret: "
					     nil))
	   (groups (input-groups group-count))
	   (password (get-option "password"
				 "Enter password: "
				 nil)))
      (v:debug '(:shamir :tool) "calling split-key2")
      (split-key2 seed password group-count group-thres groups))))

;; testcases
;; abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon about
;; ------------------------- recombine COMMAND ------------------------------
(defparameter +recombine-synopsis+
  (net.didierverna.clon:defsynopsis (:make-default nil :postfix "something more")
      (text :contents "recombine keys into the secret")
    (flag :short-name "h"   :long-name "help" :description "help about the derive command")
    (stropt :short-name "k" :long-name "key"  :description "Master key to begin derivation from.")
    (stropt :short-name "p" :long-name "password" :description "password.")))


(let ((key-identifiers nil))

  (defun prompt-for-key ()
    "After prompt-for-key has been called once with a valdi key, it
will not allow keys with other identifiers. call reset-prompt-for-key
    to reset it if needed."
    (when key-identifiers
      (format *query-io* "only accept keys that begins with ~a ~a~%" (car key-identifiers) (cadr key-identifiers)))
    (format *query-io* "Enter key (or ABORT): ")
    (finish-output *query-io*)
    (let ((word-list (uiop:split-string (read-line *query-io*) :separator " ")))
      
      (when (string= "ABORT" (car word-list))
	(net.didierverna.clon:exit 1))
      
      (cond
	;; once first key have been given we don't allow keys from other sets.
	((and key-identifiers
	      (string= (car key-identifiers)
		       (car word-list))
	      (string= (cadr key-identifiers)
		       (cadr word-list)))
	 (mnemonic->share word-list))
	((null key-identifiers)
	 ;; first key, set the key-identifiers
	 (setq key-identifiers `(,(car word-list) ,(cadr word-list)))
	 (mnemonic->share word-list))
	(t
	 ;; else we return nil to signal this was invalid
	 nil))))
  
  (defun reset-prompt-for-key ()
    (setf key-identifiers nil)))

(defun complete-keyset (keys)
  (flet ((group-index-and-thres (share)
	   `(,(share-group-index share) .
	      ,(share-member-threshold share))))
    ;; we know that we only need 16 here because that is the maximum of groups
    (let ((group-count (make-array 16 :element-type 'integer :initial-element 0))
	  ;; builds an assoc list of group idx and group threshold sorted on group-idx
	  (group-thres (remove-duplicates (sort (map 'list #'group-index-and-thres keys) #'< :key #'car) :key #'car)))
      (dolist (key keys)
	(incf (aref group-count (share-group-index key))))
      (if (not (null keys))
	  (and
	   ;; check if we have enough keys for every group
	   (not (some #'not
		      (map 'list (lambda (g)
				   (<= (cdr g)
				       (aref group-count (car g))))
			   group-thres)))
	   ;; check that we have enought keys for group threshold
	   (<=
	    (share-group-threshold (car keys))
	    (length group-thres)))
	  nil))))

(defun recombine-key ()
  (handler-bind
      ((required-option #'required-option-handler)
       (malformed-option #'malformed-option-handler))
    
    (let ((password (get-option "password"
				"Enter password: "
				nil)))
      
      (format *query-io* "Recovering a secret is done by entering keys that consist ~%~
                            of a series of words. All words in one key should be on ~%~
                            the same line seperated by spaces. After pressing enter ~%~
                            the next key should be entered.~%")

	
      (flet ((valid-and-new-key-p (key old-keys)
	       ;;checks a key is valid and that we dont allready have it
	       (and (equal (type-of key)
			   'share)
		    (not (member key old-keys)))))
	
	(format *query-io* "secret: ~a~%"
		(combine-shares
		 ;; this will keep on asking the user for keys until
		 ;; we have the necessary keys collecting them into a
		 ;; list and returning that.
		 (loop
		    while (not (complete-keyset keys) )
		    for key = (prompt-for-key)
		    if (valid-and-new-key-p key keys)
		      collect key into keys
		      and do (format *query-io* "Key has been added to the list of keys ~%")
		    else
		       do (format *query-io* "Invalid key ignored~%")
		    end
		    finally (return (if (listp keys) keys (list keys))))
		 (if password password ""))))
      
      (finish-output *query-io*))))

;; testcases
;; duckling enlarge academic academic agency result length solution fridge kidney coal piece deal husband erode duke ajar critical decision keyboard
;; TREZOR

;; shadow pistol academic always adequate wildlife fancy gross oasis cylinder mustang wrist rescue view short owner flip making coding armed
;; shadow pistol academic acid actress prayer class unknown daughter sweater depict flip twice unkind craft early superior advocate guest smoking
;; TREZOR  


;; 512 bits test case
;; academic corner academic academic academic phrase crush typical cinema column gums quarter party strategy clothes findings trash dictate finance desert year senior smirk python building infant devote veteran wrap ancient scout miracle garden together lying drove paces thumb exceed subject desktop rebound voice item argue upstairs sprinkle shelter diminish observe funding album quick sister focus cradle review premium hush
;; NO password
;; secret should be c55257c360c07c72029aebc1b53c05ed0362ada38ead3e3e9efa3708e53495531f09a6987599d18264c1e1c92f2cf141630c7a3c4ab7c81b2f001698e7463b04
;;

;; ------------------------- MAIN CMD ------------------------------------

(net.didierverna.clon:defsynopsis (:postfix "cmd [OPTIONS]")
  (text :contents "Available commands are: split recombine convert

Use 'cmd --help to get command-specific help")
  (flag :short-name "h" :long-name "help" :description "Print this help and exit.")
  (switch :short-name "d" :long-name "debug" :description "Turns on debugging information.")
  (switch :short-name "i" :long-name "interactive" :description "Turns on interactive mode where it will ask for missing things.")
  (flag :short-name "v" :long-name "version" :description "Print version number and exit."))

(defun parse-cmd-cmdline ()
  "finds the command and calls the setup function for that command"
  
  (make-context
   :synopsis (cond
	       ((string= (first (net.didierverna.clon:remainder)) "split")
		+split-synopsis+)
	       ((string= (first (net.didierverna.clon:remainder)) "recombine")
		+recombine-synopsis+)
	       ((string= (first (net.didierverna.clon:remainder)) "convert")
		+convert-wallet-synopsis+)
	       (t
		(format t "Unknown command.~%")
		(net.didierverna.clon:exit 1)))
   :cmdline (net.didierverna.clon:remainder))
  
  ;; every command should have a help option
  (when (net.didierverna.clon:getopt :short-name "h")
    (net.didierverna.clon:help)
    (net.didierverna.clon:exit))
  
  (let ((progname (net.didierverna.clon:progname)))
    (v:debug '(:shamir :tool) "Command name: ~a~%" progname)
    
    (cond ((string= progname "split")
	   (split-key))
	  
	  ((string= progname "recombine")
	   (recombine-key))

	  ((string= progname "convert")
	   (convert-seed-phrase))
	  (t ;; unknown command
	   (format *query-io* "unhandled case ~a~%" progname)))))


(defun parse-main-cmdline ()
  (v:debug '(:shamir :tool) "parsing command line")
  "parses the main options that are common to all commands"
  (make-context) ;; for command-line parsing

  (when (get-option "debug" nil nil)
    (setf (v:repl-level) :debug)
    (v:debug '(:shamir :tool) "Debugging enabled"))
  
  ;; show help and exit  
  (when (or (get-option "help" nil nil)
	    (not (net.didierverna.clon:cmdline-p)))
    (net.didierverna.clon:help)
    (net.didierverna.clon:exit))

  (when (get-option "interactive" nil nil)
    (v:debug '(:shamir :tool) "Enabling interactive mode")
    (setq +interactive-mode+ 't))
  
  ;; show version and exit
  (when (get-option "version" nil nil)
    (format *query-io* "version: ~a~%" "0.0.0")
    (net.didierverna.clon:exit))
  
  ;; if we cant find any command exit
  (unless (net.didierverna.clon:remainder)
    (format *query-io* "Missing command.~%")
    (net.didierverna.clon:exit 1))
  
  (parse-cmd-cmdline))


(defun main ()
  ;; will should bind a handler around this that hides the warnings
  (v:debug '(:shamir :tool) "initing secp256k1 library")
  (secp256k1:init)
  (terpri)

  (v:debug '(:shamir :tool) "setting up error handlers")
  ;; main error handling
  (handler-bind
      ((required-option #'required-option-handler)
       (malformed-option #'malformed-option-handler)
       (t (lambda (e)
	    (format *query-io* "Unhandled error ~a~%" e)
	    (finish-output *query-io*))))    
    (progn
      (parse-main-cmdline)))

  ;; exit
  (finish-output *query-io*)
  (net.didierverna.clon:exit))


;; how do we fix this, need to setup init-hooks also.
;; break out this into own file that fixes it.
(defun kill-threads-when-save-and-die ()
  (format t "killing threads. ")
  (verbose:stop verbose:*global-controller*)
  (handler-case 
      (bt:join-thread (verbose:thread verbose:*global-controller*))
    (t (_)
      (declare (ignore _)))))

(setq sb-ext:*save-hooks* (list #'kill-threads-when-save-and-die))

