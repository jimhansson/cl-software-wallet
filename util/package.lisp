(defpackage #:cl-software-wallet/simple-tool
	(:documentation "example on how to produce binaries using the library")
	(:use #:cl
	      #:net.didierverna.clon
	      #:cl-software-wallet)
	(:export #:main))

(defpackage #:cl-software-wallet/shamir-tool
	(:documentation "example on how create a application that can
	handle shamir secret sharing SLIP-39 wallets.")
	(:use #:cl
	      #:net.didierverna.clon
	      #:cl-software-wallet.shamir
	      #:cl-software-wallet)
	(:import-from :cl-software-wallet.mnemonic :convert-to-seed)
	(:export #:main))
