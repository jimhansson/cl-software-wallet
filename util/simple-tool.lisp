(in-package #:cl-software-wallet/simple-tool)




(net.didierverna.clon:defsynopsis (:postfix "cmd [OPTIONS]")
  (text :contents "Available commands are: derive

Use 'cmd --help to get command-specific help")
  (flag :short-name "h" :long-name "help" :description "Print this help and exit.")
  (switch :short-name "d" :long-name "debug" :description "Turns on debugging information.")
  (switch :short-name "i" :long-name "interactive" :description "Turns on interactive mode where it will ask for missing things.")
  (flag :short-name "v" :long-name "version" :description "Print version number and exit."))

(defparameter +interactive-mode+ nil
  "If we should do interactive mode")


;; ------------------------- UTILITIES -----------------------------------
(defun split-by (string chr)
  (loop for start = 0 then (1+ finish)
     for finish = (position chr string :start start)
     collecting (subseq string start finish)
     until (null finish)))

(define-condition required-option (error)
  ((name :initarg :name :reader name)
   (prompt :initarg :prompt :reader prompt)))

(define-condition malformed-option (error)
  ((name :initarg :name :reader name)
   (prompt :initarg :prompt :reader prompt)
   (current-value :initarg :current-value :reader current-value)))

(defmacro with-redo (name &body body)
  `(macrolet ((redo (name)
                `(go ,name)))
     (tagbody
        ,name
        ,@body)))

(defun get-option (long-name prompt validator &key (required nil))
  (let ((option (net.didierverna.clon:getopt :long-name long-name)))
    ;; if missing but required
    (when (and required (null option))
      (restart-case
	(progn
	  (signal 'required-option :name long-name :prompt prompt))
	(use-value-required-option (value)
	  (setq option value))))
    
    (with-redo validation
	(restart-case
	    ;; if malformed
	    (when validator
	      (when (not (funcall validator option))
		(signal 'malformed-option :name long-name :prompt prompt :current-value option)))
	  (use-value-malformed-option (value)
	    (setq option value)
	    (redo validation))))
    option))

(defun get-required-option (long-name prompt validator)
  (get-option long-name prompt validator :required t))


;; ------------------------- WORDS-TO-KEY COMMAND -----------------------
(defparameter +words-to-key-synopsis+
  (net.didierverna.clon:defsynopsis (:make-default nil :postfix "something more")
    (text :contents "Turns a series of words into a xprv or tprv key.")
    (flag :short-name "h" :long-name "help" :description "Help about the words-to-key command.")
    (flag :short-name "s" :long-name "slip" :description "use the slip-32 format instead of bip-32.")
    (stropt :short-name "w" :long-name "words" :description "a list of space separated words." )
    (stropt :short-name "l" :long-name "language" :description "language of word-list to use." :default-value "english")))


(defun words-to-key (words passphrase language)
  (cond ((string= language "english")
	 (setq cl-software-wallet.mnemonic:*wordlist*
	       cl-software-wallet.mnemonic:english-wordlist))
	(t
	 (error "invalid word-list")))
  (let* ((entropy
	  (if passphrase
	      (cl-software-wallet.mnemonic:convert-to-seed words passphrase)
	      (cl-software-wallet.mnemonic:convert-to-seed words)))
	 (hdkey (cl-software-wallet.hdkey:from-seed (ironclad:hex-string-to-byte-array entropy)))
	 (base58coded (cl-software-wallet.hdkey:write-hdkey hdkey)))
    (format *query-io* "~a~%" base58coded)
    (finish-output *query-io*)))


(defun words-to-key-setup ()
  (flet ()
    (let ((words (get-required-option "words" "List of words" nil))
	  (language (get-option "language" "Language of word-list" nil))) 
      (words-to-key (split-by words #\Space) nil language))))


;; ------------------------- DERIVE COMMAND ------------------------------
(defparameter +derive-synopsis+
  (net.didierverna.clon:defsynopsis (:make-default nil :postfix "something more")
    (text :contents "Derive a subkey")
    (flag :short-name "h"   :long-name "help" :description "help about the derive command")
    (flag :short-name "s"   :long-name "slip" :description "use the slip-32 format instead of bip-32")
    (stropt :short-name "k" :long-name "key"  :description "Master key to begin derivation from.")
    (stropt :short-name "p" :long-name "path" :description "Derivation path to be used.")))


(defun derive-key (key-str path)
  (format t "deriving key, with path ~a~%" path)
  (let* ((key nil)
	 (subkey nil))
    (setq key (cl-software-wallet.hdkey:parse-hdkey key-str))
    (setq subkey (apply #'cl-software-wallet.hdkey:derive-by-path key path))
    (format *query-io* "~a~%" (cl-software-wallet.hdkey:write-hdkey subkey))
    (finish-output *query-io*)))


(defun derive-key-setup ()
  "finds the options for the derive command"
  (labels ((to-path-integer  (p)
	     ;; we need to harden them if they begin with '
	     (if (equal (aref p 0) #\')
		 (cl-software-wallet.hdkey:hardened (parse-integer (subseq p 1)))
		 (parse-integer p)))

	   (valid-match (start end length)
	     (and
	      (not (null start))
	      (not (null end))
	      (= 0 start)
	      (= end length)))
	   
	   (path-validator (p)
	     ;; check if we can match the whole st
	     (format t "checking path ~a ~%" p)
	     (if (stringp p)
		 (multiple-value-bind (start end)
		     (ppcre:scan "'?\\d+[/'?\\d+]*" p)
		   (valid-match start end (length p)))
		 nil))
	   
	   (key-validator (k)
	     (format t "checking key~%")
	     (if (stringp k)
		 (multiple-value-bind (start end)
		     (ppcre:scan "([xprv]|[xpub])[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]+" k)
		   (valid-match start end (length k)))
		 nil)))
    
    (let ((path (get-required-option "path"
				     "Derivation path"
				     #'path-validator))
	  (key (get-required-option "key"
				    "Key"
				    #'key-validator)))
      (derive-key
       key
       (map 'list
	    #'to-path-integer
	    (split-by path #\/))))))


;; ------------------------- MAIN CMD ------------------------------------
(defun parse-cmd-cmdline ()
  "finds the command and calls the setup function for that command"

  (make-context
   :synopsis (cond ((string= (first (net.didierverna.clon:remainder)) "derive")
		    +derive-synopsis+)
		   ((string= (first (net.didierverna.clon:remainder)) "words-to-key")
		    +words-to-key-synopsis+)
		   (t
		    (format t "Unknown command.~%")
		    (net.didierverna.clon:exit 1)))
   :cmdline (net.didierverna.clon:remainder))

  ;; every command should have a help option
  (when (net.didierverna.clon:getopt :short-name "h")
    (net.didierverna.clon:help)
    (net.didierverna.clon:exit))

  (let ((progname (net.didierverna.clon:progname)))
    (format *query-io* "Command name: ~a~%" progname)

    (cond ((string= progname "derive")
	   (derive-key-setup))
	  
	  ((string= progname "words-to-key")
	   (words-to-key-setup))
	  
	  (t ;; unknown command
	   (format *query-io* "unhandled case ~a~%" "foo")))))


(defun parse-main-cmdline ()
  "parses the main options that are common to all commands"
  (make-context) ;; for command-line parsing
  
  ;; show help and exit  
  (when (or (get-option "help" nil nil)
	    (not (net.didierverna.clon:cmdline-p)))
    (net.didierverna.clon:help)
    (net.didierverna.clon:exit))

  (when (get-option "interactive" nil nil)
    (setq +interactive-mode+ 't))
  
  ;; show version and exit
  (when (get-option "version" nil nil)
    (format *query-io* "version: ~a~%" "0.0.0")
    (net.didierverna.clon:exit))
  
  ;; if we cant find any command exit
  (unless (net.didierverna.clon:remainder)
    (format *query-io* "Missing command.~%")
    (net.didierverna.clon:exit 1))
  
  (parse-cmd-cmdline))


(defun main ()
  ;; will should bind a handler around this that hides the warnings
  (secp256k1:init)
  (terpri)
  (labels ((ask-for-value-and-invoke-restart (prompt restart)
	     (format *query-io* "~a: " prompt)
	     (finish-output *query-io*)
	     (let ((value (read-line *query-io*)))
	       (when restart
		 (invoke-restart restart value)
		 (format *query-io* "Could not find restart point.~%")
		 (finish-output *query-io*)
		 value)))
	   
	   (required-option-handler (opt)
	     (when +interactive-mode+
	       (let ((restart (find-restart 'use-value-required-option)))
		 (ask-for-value-and-invoke-restart (prompt opt) restart))))
	   
	   (malformed-option-handler (opt)
	     (if +interactive-mode+
	       (let ((restart (find-restart 'use-value-malformed-option)))
		 (ask-for-value-and-invoke-restart (prompt opt) restart))
	       ;; if not interactive just print the name of parameter
	       (progn
		 (format *query-io* "Malformed option: ~a~%" (name opt))
		 (finish-output *query-io*)))))
    
    ;; main error handling
    (handler-bind
	((required-option #'required-option-handler)
	 (malformed-option #'malformed-option-handler)
	 (t (lambda (e)
	      (format *query-io* "Unhandled error ~a~%" e)
	      (finish-output *query-io*))))    
      (progn
	(parse-main-cmdline))))

  ;; exit
  (finish-output *query-io*)
  (net.didierverna.clon:exit))


;; how do we fix this, need to setup init-hooks also.
;; break out this into own file that fixes it.
(defun kill-threads-when-save-and-die ()
  (format t "killing threads. ")
  (verbose:stop verbose:*global-controller*)
  (handler-case 
      (bt:join-thread (verbose:thread verbose:*global-controller*))
    (t (_)
      (declare (ignore _)))))

(setq sb-ext:*save-hooks* (list #'kill-threads-when-save-and-die))
