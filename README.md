# cl-software-wallet
### _Jim Hansson <jim.hansson@gmail.com>_

This library will implement the main alogritms that has todo with a
bitcoin wallets.


## Systems
* cl-software-wallet : main package with the most common things.
* cl-software-wallet/tests : contains tests for the main system.

## Packages

## Tests

The package contains a lot of tests and will also run tests of other
packages that it depends on to run them you only have to do
(asdf:test-system :cl-software-wallet) the tests are located in a
subsystem called :cl-software-wallet/tests and all tests should be
there to not polute the main system.

## License

GPL-3

## Status of BIPs(and SLIPs)

* BIP-32 : mostly done and working
* BIP-39 : mostly done and working
* BIP-144 : have not been tested
* BIP-158 : WIP
* BIP-173 : WIP
* BIP-174 : PSBTs WIP
* SLIP-39 : working, but need cleanup


## Tools

This repository also contains some small tools that could be used but
are mostly here as an example of what could be built with this
library. The tools are

* simple-tool : HD wallet derivation
* shamir : tool for Shamir secret sharing and recombining.


## notes

if i decide to use bit-smasher, i could also remove my implementation
of base58, because bit-smasher depends on a implementation of base58
called cl-base58. today bit-smasher is used in the implementation of
golomb code sets.

Maybe should try to isolate the usage of ironclad into one part of the
code and create my own wrappers around it.


