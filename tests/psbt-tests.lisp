
(in-package #:cl-software-wallet/tests/psbt)


;; Here we have tests for the PSBT functions, if data used in the
;; testcase is long it should be placed in psbt-test-data.lisp instead
;; as a global variable. that file will be read first, mark those
;; variables with * before and after to make it clear that they are
;; defined in another file.

(defmacro psbt-test (data &body body)
  "Wraps testing of one psbt in a piece of code that will do the
deserialization and make sure everthing is ok, at the end it will also
try to serialize it again to make sure we get the same as we started
with. this means you should not use this if you are modifying the psbt
in your test."
  (let ((test-name (symbol-name data))
	(*print-length* 2048)
	(*print-array* 2048))
    `(prove:subtest ,test-name
       (let* ((bytes (hex-string-to-byte-array ,data))
	      (psbt (parse-psbt bytes)))
	 (v:debug '(:cl-software-wallet.psbt :psbt :tests) "PSBT under test: ~x" psbt)
	 (v:sync)
	 (prove:isnt psbt nil "Parsing did return something")
	 ,@body
	 (let* ((serialized (write-psbt psbt))
		(serialized-hex (ironclad:byte-array-to-hex-string serialized)))
	   (declare (ignorable serialized serialized-hex))
	   (assert (equalp serialized-hex ,data))
	   (prove:is serialized-hex ,data :test #'equalp "re-serialization gives us the same things as we started with"))))))

(defun test-global-transaction (psbt)
  "Checks that we have a unsigned-tx and that its key is correct, will
also pull out the intresting parts of that transaction. will return
  the version, inputs, outputs, witness and lock-time of transaction"
  (let ((global (global-map psbt)))
    (prove:is (length (find-by-key-type :UNSIGNED-TX global)) 1 "Length of global list")
    (prove:is (caar (find-by-key-type :UNSIGNED-TX global)) #() :test #'equalp "Key of transaction is zero")
    (prove:is (type-of (cdar (find-by-key-type :UNSIGNED-TX global))) 'cl-software-wallet:<tx> :test #'equalp "And it is a transaction object")
    (let ((trans (cdar (find-by-key-type :UNSIGNED-TX global))))
      (with-slots (tx-version tx-in tx-out tx-witness tx-lock-time) trans
	(values tx-version
		tx-in
		tx-out
		tx-witness
		tx-lock-time)))))

(prove:subtest "PSBT tests"
  
  (prove:subtest "Find functions for maps"

    (prove:subtest "begins-with"
      (prove:ok (cl-software-wallet.psbt::begins-with "sdf" "sdf)"))
      (prove:ok (not (cl-software-wallet.psbt::begins-with "sadf " "asdf")))
      (prove:ok (cl-software-wallet.psbt::begins-with "sd" "sdf")))
    
    (prove:subtest "find-by-key-type"
      (prove:ok (find-by-key-type :UNSIGNED-TX
				  '(((:UNSIGNED-TX #()) . "foobar"))))
      (prove:is
       (length (find-by-key-type :UNSIGNED-TX '(((:UNSIGNED-TX #())    . "foobar")
						((:UNSIGNED-TX #(3 2)) . "foobar"))))
       2
       "Can we get both matching keys by type")
      (prove:ok
       (find-by-key-type :UNSIGNED-TX '(((:UNSIGNED-TX #())    . "foobar")
					((:UNSIGNED-TX #(3 2)) . "foobar"))))
      (prove:is
       (length (find-by-key-type :UNSIGNED-TX '(((:UNSIGNED-TX #())    . "foobar")
						((:USIGNED-TX #())     . "foobar")
						((:UNSIGNED-TX #(3 2)) . "foobar"))))
       2
       "But not non-matching keys")
      (prove:ok
       (find-by-key-type :UNSIGNED-TX '(((:UNSIGNED-TX #())    . "foobar")
					((:USIGNED-TX #())     . "foobar")
					((:UNSIGNED-TX #(3 2)) . "foobar"))))
      (prove:is (find-by-key-type :UNSIGNED-TX '(((:NSIGNED-TX #())    . "foobar")))
		nil))
    (prove:subtest "find-key"
      (prove:ok (find-by-key :UNSIGNED-TX #()  '(((:UNSIGNED-TX #())  . "FOOBAR"))))
      (prove:is (find-by-key :UNSIGNED-TX #()  '(((:UNSIGNED-TX #(3)) . "FOOBAR"))) nil)
      (prove:is (find-by-key :UNSIGNED-TX #(3) '(((:UNSIGNED-TX #())  . "foobar"))) nil)
      (prove:ok (find-by-key :UNSIGNED-TX #(3) '(((:UNSIGNED-TX #(3)) . "foobar"))))
      (prove:is (find-by-key :UNSIGNED-TX #()  '(((:NSIGNED-TX #())   . "foobar"))) nil)
      (prove:is (find-by-key :UNSIGNED-TX #(3) '(((:NSIGNED-TX #(3))  . "foobar"))) nil)
      (prove:is (find-by-key :UNSIGNED-TX #(3) '(((:NSIGNED-TX #())   . "foobar"))) nil)))
  
  (prove:subtest "Parsing tests"
    
    (psbt-test *with-one-p2pkh-input-outputs-are-empty*
      (prove:isnt (global-map psbt) nil "we have something in the global map")
      (prove:isnt (input-list psbt) nil "We have something in the input list")
      (let ((global (global-map psbt)))
	(prove:ok (find-by-key :UNSIGNED-TX #() global) "we have an unsigned-tx")
	(prove:is (length global) 1 "And nothing else"))
      (prove:is (length (input-list psbt)) 1 "the length of input list should be 1")
      (let ((input (car (input-list psbt))))
	(prove:isnt (find-by-key :INPUT-NON-WITNESS-UTXO #() input) nil "Check that we have one non-witness-utxo")
	(prove:is (length input) 1 "And nothing else")))
    
    (psbt-test *with-one-p2pkh-input-and-one-p2sh-p2wpkh-input-first-input-is-signed-and-finalized-outputs-are-empty*
      (prove:isnt (global-map psbt) nil "We have something in the global map position")
      (prove:isnt (input-list psbt) nil "We have something in the input list position")
      (multiple-value-bind (version ins outs witness lock-time)
	  (test-global-transaction psbt)
	(declare (ignorable version ins outs witness lock-time))
	(let ((ilist (input-list psbt)))
	  (prove:is (length ilist) 2 "We should have 2 item in the input list")
	  (prove:is (length ilist) (length ins) "And that is the same as inputs in the transaction")
	  (prove:is (car (car (car (car ilist)))) :INPUT-FINAL-SCRIPTSIG "First input is signed and finalized")
	  (let ((second (cadr ilist))
		(first (car ilist)))
	    (prove:is (length first) 1)
	    (prove:ok (find-by-key-type :INPUT-FINAL-SCRIPTSIG first))
	    (prove:is (length second) 2)
	    (prove:ok (find-by-key-type :INPUT-WITNESS-UTXO second))
	    (prove:ok (find-by-key-type :INPUT-REDEEM-SCRIPT second))))))
    
    (psbt-test *with-one-p2pkh-input-which-has-a-non-final-scriptsig-and-has-a-sighash-type-specified-ouputs-are-empty*
      (prove:isnt (global-map psbt) nil "We have something in the global map position")
      (prove:isnt (input-list psbt) nil "We have something in the input list position")
      (multiple-value-bind (version ins outs witness lock-time)
	  (test-global-transaction psbt)
	(declare (ignorable version ins outs witness lock-time))
	(let* ((ilist (input-list psbt))
	       (input (car ilist)))
	  (prove:is (length ilist) 1)
	  (prove:is (length input) 2)
	  (prove:ok (find-by-key :INPUT-SIGHASH-TYPE #() input))
	  (prove:is (cdr (car (find-by-key :INPUT-SIGHASH-TYPE #() input))) #(1 0 0 0) :test #'equalp)
	  (prove:ok (find-by-key :INPUT-NON-WITNESS-UTXO #() input)))))

    (psbt-test *with-one-p2pkh-input-and-one-p2psh-p2wpkh-input-both-with-non-final-scriptsig-p2sh-p2wpkh-inputs-redeem-script-is-available-outputs-filled*
      (prove:isnt (global-map psbt) nil "We have something in the global map position")
      (prove:isnt (input-list psbt) nil "We have something in the input list position")
      (let ((ilist (input-list psbt))
	    (olist (output-list psbt)))
	(when (prove:is (length ilist) 2  "We have two inputs")
	  (let ((first-input (car ilist))
		(second-input (cadr ilist)))
	    (prove:is (length first-input) 1)
	    (prove:ok (find-by-key-type :INPUT-NON-WITNESS-UTXO first-input))
	    (prove:is (length second-input) 2)
	    (prove:ok (find-by-key-type :INPUT-WITNESS-UTXO second-input))
	    (prove:ok (find-by-key-type :INPUT-REDEEM-SCRIPT second-input))))
	(when (prove:is (length olist) 2 "We have two outputs")
	  (let ((first-output (car olist))
		(second-output (cadr olist)))
	    (prove:ok (find-by-key-type :OUTPUT-BIP32-DERIVATION first-output) "first output has a bip32 derivation")
	    (prove:ok (find-by-key-type :OUTPUT-BIP32-DERIVATION second-output) "second output has a bip32 derivation")))))

    (psbt-test *with-one-p2sh-p2wsh-input-of-a-2-of-2-multisig-redeemscript-witnessscript-and-keypaths-are-available*
      (prove:isnt (global-map psbt) nil "We have something in the global map position")
      (prove:isnt (input-list psbt) nil "We have something in the input list position")
      (let ((gmap (global-map psbt)))
	(prove:ok (find-by-key :UNSIGNED-TX #() gmap)))
      (let ((ilist (input-list psbt)))
	(prove:is (length ilist) 1)
	(let ((imap (car ilist)))
	  (prove:ok (find-by-key :INPUT-WITNESS-UTXO #() imap))
	  (prove:ok (find-by-key-type :INPUT-PARTIAL-SIG imap))
	  (prove:ok (find-by-key :INPUT-REDEEM-SCRIPT #() imap))
	  (prove:ok (find-by-key :INPUT-WITNESS-SCRIPT #() imap))
	  (prove:is (length (find-by-key-type :INPUT-BIP32-DERIVATION imap)) 2))))

    
    (psbt-test *with-one-P2WSH-input-of-a-2-of-2-multisig-witnessScript-keypaths-and-global-xpubs-are-available-Contains-no-signatures-Outputs-filled*)

    
    (prove:subtest "with-unknown-types-in-the-inputs"
      ;; this should fire a condition with a restart for ignoring.
      (prove:fail "needs more work"))


    (psbt-test *with-PSBT_GLOBAL_XPUB*)
    
    (prove:subtest "fails signer checks"
      (prove:subtest "a witness utxo is provided for an non-witness input"
	(let* ((data (hex-string-to-byte-array *a-witness-utxo-is-provided-for-an-non-witness-input*))
	       (psbt (parse-psbt data)))
	  (v:debug '(:cl-software-wallet.psbt :psbt :tests) "PSBT under test: ~a" psbt)))
      
      (prove:subtest "redeemscript with a non-witness utxo does not match the scriptpubkey"
	(let* ((data (hex-string-to-byte-array *redeemscript-with-a-non-witness-utxo-does-not-match-the-scriptpubkey*))
	       (psbt (parse-psbt data)))
	  (v:debug '(:cl-software-wallet.psbt :psbt :tests) "PSBT under test: ~a" psbt)))
      
      (prove:diag "add more here")))


  (prove:subtest "Testing parsing of invalid PSBT"
    (prove:diag "We expect that these should give an error.")
    ;; When we get things working we need to remeber to remove them from this ignore
    ;; declaration. or else we will get warnings.
    (prove:is-error (parse-psbt *filled-in-scriptsig-in-unsigned-tx*) 'error) ;; FIXME
    (prove:is-error (parse-psbt *network-transaction*) 'error)
    (prove:is-error (parse-psbt *missing-outputs*) 'error) ;; FIXME: does not give correct error
    (prove:is-error (parse-psbt *inputs-and-outputs-but-no-unsigned-tx*) 'error)
    (prove:is-error (parse-psbt *duplicate-keys-in-input*) 'error)
    (prove:is-error (parse-psbt *invalid-global-transaction-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-input-witness-utxo-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-pubkey-length-for-input-partial-signature-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-redeemscript-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-witness-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-bip32-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-non-witness-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-final-scriptsig-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-final-witness-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-pubkey-in-output-bip32-derivation-paths-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-input-sighash-type-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-output-redeemscript-typed-key*) 'error)
    (prove:is-error (parse-psbt *invalid-output-witness-script-typed-key*) 'error)) ;; FIXME: does not give correct error
  
  (prove:subtest "PSBT Functional tests"
    (let ((extended-private-key "tprv8ZgxMBicQKsPd9TeAdPADNnSyH9SSUUbTVeFszDE23Ki6TBB5nCefAdHkK8Fm3qMQR6sHwA56zqRmKmxnHk37JkiFzvncDqoKmPWubu7hDF")
	  (seed "cUkG8i1RFfWGWy5ziR11zJ5V4U4W3viSFCfyJmZnvQaUsd1xuF3T"))
      (declare (ignore seed))
      (prove:diag (format nil "For these test we will use this private key ~a" extended-private-key))))

  (prove:subtest "simple serialisation and deserialization test"
    (let* ((hexdata "70736274ff0100750200000001268171371edff285e937adeea4b37b78000c0566cbb3ad64641713ca42171bf60000000000feffffff02d3dff505000000001976a914d0c59903c5bac2868760e90fd521a4665aa7652088ac00e1f5050000000017a9143545e6e33b832c47050f24d3eeb93c9c03948bc787b32e1300000100fda5010100000000010289a3c71eab4d20e0371bbba4cc698fa295c9463afa2e397f8533ccb62f9567e50100000017160014be18d152a9b012039daf3da7de4f53349eecb985ffffffff86f8aa43a71dff1448893a530a7237ef6b4608bbb2dd2d0171e63aec6a4890b40100000017160014fe3e9ef1a745e974d902c4355943abcb34bd5353ffffffff0200c2eb0b000000001976a91485cff1097fd9e008bb34af709c62197b38978a4888ac72fef84e2c00000017a914339725ba21efd62ac753a9bcd067d6c7a6a39d05870247304402202712be22e0270f394f568311dc7ca9a68970b8025fdd3b240229f07f8a5f3a240220018b38d7dcd314e734c9276bd6fb40f673325bc4baa144c800d2f2f02db2765c012103d2e15674941bad4a996372cb87e1856d3652606d98562fe39c5e9e7e413f210502483045022100d12b852d85dcd961d2f5f4ab660654df6eedcc794c0c33ce5cc309ffb5fce58d022067338a8e0e1725c197fb1a88af59f51e44e4255b20167c8684031c05d1f2592a01210223b72beef0965d10be0778efecd61fcac6f79a4ea169393380734464f84f2ab300000000000000")
	   (data (hex-string-to-byte-array hexdata))
	   (psbt (parse-psbt data)))
      (prove:is (ironclad:byte-array-to-hex-string (write-psbt psbt)) hexdata))))
