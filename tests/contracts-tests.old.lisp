

(in-package :cl-software-wallet)


;; old test code, 

(annot:enable-annot-syntax)

(defgeneric foo (bar)
  (:method-combination dbc))


@precondition :foobar
(defmethod foo (bar) ())

@postcondition :foobar
(defmethod foo ((v integer)))

@invariant :foo-base
(defmethod foo (bar))



(defgeneric dbc2-test (foo bar)
  (:method-combination dbc))

(defmethod dbc2-test (foo bar)
  (format t "dbc2-test (foo bar) ~%"))

(defmethod dbc2-test ((foo string) bar)
  (format t "dbc2-test ((foo string) bar) ~%"))

(defmethod dbc2-test ((foo string) (bar string))
  (format t "dbc2-test ((foo string) (bar string)) ~%")
  (call-next-method))

(defmethod dbc2-test (foo (bar integer))
  (format t "dbc2-test (foo (bar integer)) ~%"))

(defmethod dbc2-test :before (foo bar)
  (format t "dbc2-test :before (foo bar) ~%"))

(defmethod dbc2-test :after (foo bar)
  (format t "dbc2-test :after (foo bar) ~%"))

(defmethod dbc2-test :precondition :testing (foo bar)
  (format t "dbc2-test :precondition :testing (foo bar) ~%")
  t)

(defmethod dbc2-test :invariant :testing (foo bar)
  (format t "dbc2-test :invariant :testing (foo bar) ~%")
  t)

(defmethod dbc2-test :precondition :testing2 (foo bar)
  (format t "dbc2-test :precondition :testing2 (foo bar) ~%")
  t)

(defmethod dbc2-test :precondition :testing3 (foo (bar string))
  (format t "dbc2-test :precondition :testing2 (foo (bar string)) ~%")
  t)

(defmethod dbc2-test :precondition :testing4 ((foo string) bar)
  (format t "dbc2-test :precondition :testing4 ((foo string) bar) ~%")
  t)

(defmethod dbc2-test :precondition :testing5 ((foo integer) bar)
  (format t "dbc2-test :precondition :testing5 ((foo integer) bar) ~%")
  t)

(defmethod dbc2-test :postcondition :testing6 (foo bar)
  (format t "dbc2-test :postcondition :testing6 (foo bar) ~%")
  t)

(defmethod dbc2-test :postcondition :testing7 ((foo string) bar)
  (format t "dbc2-test :postcondition :testing7 ((foo string) bar) ~%")
  t)

@postcondition :testing8
(defmethod dbc2-test ((foo string) bar)
  (format t "dbc2-test :postcondition :testing8 ((foo string) bar) ~%")
  t)


(let ((+inside-contract-p+ t))
  (dbc2-test "foobar" "foobar"))

(dbc2-test "foobar" t)
(dbc2-test t t)
