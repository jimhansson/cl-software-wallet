
(in-package #:cl-software-wallet/tests)

(prove:subtest "test contract"

  (fmakunbound 'foobar)
  
  (defgeneric foobar (a b c)
    (:method-combination dbc))

  (let ((calls '()))
    
    (defmethod foobar :precondition :included (a b c)
	       (push :pre-inc calls) t)
    
    (defmethod foobar :precondition :excluded (a b (c string))
	       (push :pre-exc calls) t)
    
    (defmethod foobar :around (a b c)
	       (push :around-before calls)
	       (multiple-value-bind (a b c)
		   (call-next-method)
		 (push :around-after calls)
		 (values a b (+ c 1))))
    
    (defmethod foobar :before (a b c)
	       (push :before calls))
    
    (defmethod foobar (a b c)
      (push :main calls)
      (values (+ 1 a) b c))
    
    (defmethod foobar :after (a b c)
	       (push :after calls))
    
    (defmethod foobar :postcondition :included (a b c)
	       (push :post-inc calls) t)
    
    (defmethod foobar :postcondition :included2 (a b (c integer))
	       (push :post-inc2 calls) t)
    
    (defmethod foobar :postcondition :excluded (a b (c string))
	       (push :post-exc calls) t)
    
    (multiple-value-bind (a b c)
	(foobar 1 2 3)
      (declare (ignore b))

      (prove:is
       (reverse calls)
       '(:pre-inc :around-before :before :main :after
	 :around-after :post-inc :post-inc2)
       "Call order is correct and we don't get non matching functions")
      
      (prove:is c 4 "around qualifier works")
      (prove:is a 2 "main works"))
    
    (prove:subtest "with-contracts-disabled"
      (setq calls '())
      (with-contracts-disabled ()
	(multiple-value-bind (a b c)
	    (foobar 1 2 3)
	  (declare (ignore b c))
	  (prove:is
	   (reverse calls)
	   '(:around-before :before :main :after :around-after)
	   "No contracts should be called")
	  (prove:is a 2)))
      (finalize)))
  (finalize))
