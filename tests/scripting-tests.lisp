

(in-package #:cl-software-wallet/tests)

(defmacro script-test (script-in desc &body body)
    ""
    `(flet ((last-state (x) (car x))
	    (top-stack (x) (car (cadr x)))
	    (verify-status (x) (cadddr x)))
       (declare (ignore #'last-state
			#'top-stack
			#'verify-status))
       
       (let* ((script ,script-in)
	      (states (execute script))
	      (last-state (last-state states)))
	 (declare (ignorable script
			     last-state))
	 
	 (flet ((finished? () (eq (car last-state) nil)))

	   (subtest (format nil "script: ~a" script)
	     (diag ,desc)
	     
	     ,@body
	     
	     (with-slots (prove.suite:failed) *suite*
	       (unless (eq prove.suite:failed 0)
		 (diag "states:")
		 (dolist (state (nreverse states))
		   (diag (format nil "state: ~a" state))))))))))

(subtest "scripting"
  
  (subtest "serialize"
    (plan 5)
    (is (serialize-p2pkh (make-array 20 :initial-element #xba))
	      #(118 169 20 #xba #xba #xba #xba #xba #xba #xba #xba #xba #xba
		#xba #xba #xba #xba #xba #xba #xba #xba #xba #xba 136 172)
	      :test #'equalp)
    (is (serialize-p2pk (make-array 10 :initial-element #xba))
	      #(10 186 186 186 186 186 186 186 186 186 186 172)
	      :test #'equalp)
    
    ;; testing pushdata<X>
    (flet ((begining-of (s) (subseq s 0 10)))
      
      (subtest "pushdata1"
	
	(let* ((array-76 (make-array 76 :initial-element #xba))
	       (array-255 (make-array 255 :initial-element #xba))
	       (result-76 (serialize-p2pk array-76))
	       (result-255 (serialize-p2pk array-255)))
	  (is (begining-of result-76)
		    (begining-of #(#x4c 76 186 186 186 186 186 186 186 186 186 186 186))
		    :test #'equalp
		    "data over 75 in length should be coded as pushdata1 follow by length")
	  ;; length is pushdata opcode, length bytes, databytes, and checksig opcode
	  (is (length result-76) (+ 1 1 76 1))
	  (is (begining-of result-255)
		    (begining-of #(#x4c 255 186 186 186 186 186 186 186 186 186 186 186))
		    :test #'equalp
		    "data over 75 in length should be coded as pushdata1 follow by length")
	  (is (length result-255) (+ 1 1 255 1))))
      
      (subtest "pushdata2"
	
	(let* ((array-256 (make-array 256 :initial-element #xba))
	       (array-65535 (make-array 65535 :initial-element #xba))
	       (result-256 (serialize-p2pk array-256))
	       (result-65535 (serialize-p2pk array-65535)))
	  (is (begining-of result-256)
		    (begining-of #(#x4d 1 0 186 186 186 186 186 186 186 186 186 186 186))
		    :test #'equalp
		    "data over 255 in length should be coded as pushdata2 follow by length")
	  (is (length result-256) (+ 1 2 256 1))
	  (is (begining-of result-65535)
		    (begining-of #(#x4d 255 255 186 186 186 186 186 186 186 186 186 186 186))
		    :test #'equalp
		    "data over 255 in length should be coded as pushdata2 follow by length")
	  (is (length result-65535) (+ 1 2 65535 1))))
      
      (subtest "pushdata4"
	
	(let* ((array-65536 (make-array 65536 :initial-element #xba))
	       (result-65536 (serialize-p2pk array-65536)))
	  (is (begining-of result-65536)
		    (begining-of #(#x4e 0 1 0 0 186 186 186 186 186 186 186 186 186 186 186))
		    :test #'equalp
		    "data over 65535 in length should be coded as pushdata4 follow by length")
	  (is (length result-65536) (+ 1 4 65536 1)))))
    (finalize))

  (subtest "p2sh"
    (let* ((p2pk (serialize-p2pkh
		  #(#x30 #x30)))
	   (hash (hash160 p2pk)))
      (script-test `( ( #(#x2 #x2) ,p2pk) (:OP_HASH160 ,hash :OP_EQUAL))
	  ""
	(prove:ok (not (finished?))))))
  
  (subtest "parse"
    (is-error (parse-script #(#x01 #x01 #x50)) 'error "We should fail if an unknown opcode is found")
    (ok (parse-script #(#x01 #x50 #x01 #x00)) "but if we find non-existant opcodes in data parts we should not fail"))


  (subtest "p2sh"
    (diag "p2sh needs som special handling. we need to detect a
    specific form, and when that has been found we need to do a
    sub-execute where we take part of the script that looks like data
    and convert into a script and execute that."))
  
  (subtest "execute"
    (script-test '((:OP_1 :OP_1 :OP_ADD :OP_2 :OP_EQUAL))
	"test add and equal, checks that we dont set verify flag on equal"
      (is (finished?) t "finished")
      (is (top-stack last-state) t "top item is true")
      (is (verify-status last-state) nil "verify status not set"))
    
    (script-test '((:OP_1 :OP_1 :OP_EQUALVERIFY))
	"test that we set the verify status flag when executing :OP_EQUALVERIFY"
      (is (finished?) t "finished")
      (is (top-stack last-state) nil "top item is nil, :OP_EQUALVERIFY should consume it")
      (is (verify-status last-state) :OK "verify status set"))

    (script-test '((:OP_2 :OP_1 :OP_1 :OP_1 :OP_EQUALVERIFY :OP_EQUALVERIFY))
	"test that we will set a transaction as :NOTOK on :VERIFY"
      (is (finished?) nil "finished")
      (is (top-stack last-state) nil "top item is nil, :OP_EQUALVERIFY should consume it")
      (is (verify-status last-state) :NOTOK "verify status not set"))

    (script-test '((:OP_1 :OP_1 :OP_IF :OP_1 :OP_ELSE :OP_2 :OP_ENDIF :OP_EQUALVERIFY))
	"test if true case"
      (is (finished?) t "finished")
      (is (top-stack last-state) nil "top item is nil, :OP_EQUALVERIFY should consume it")
      (is (verify-status last-state) :OK "verify status set"))

    (script-test '((:OP_2 :OP_0 :OP_IF :OP_1 :OP_ELSE :OP_2 :OP_ENDIF :OP_EQUALVERIFY))
	"test if false case"
      (is (finished?) t "finished")
      (is (top-stack last-state) nil "top item is nil, :OP_EQUALVERIFY should consume it")
      (is (verify-status last-state) :OK "verify status set"))

    (script-test '((:OP_2 :OP_0 :OP_NOTIF :OP_2 :OP_ELSE :OP_1 :OP_ENDIF :OP_EQUALVERIFY))
	"test notif true case"
      (is (finished?) t "finished")
      (is (top-stack last-state) nil "top item is nil, :OP_EQUALVERIFY should consume it")
      (is (verify-status last-state) :OK "verify status set"))

    (script-test '((:OP_1 :OP_1 :OP_NOTIF :OP_2 :OP_ELSE :OP_1 :OP_ENDIF :OP_EQUALVERIFY))
	"test notif false case"
      (is (finished?) t "finished")
      (is (top-stack last-state) nil "top item is nil, :OP_EQUALVERIFY should consume it")
      (is (verify-status last-state) :OK "verify status set"))

    (subtest "arithmetic"
      
      (defun script-test-arithmetic (script expected-result)
	(handler-bind  ((disabled-operation #'continue))
	  (script-test script
	      (format nil "Test arithmetic, operation ~a" (car (last script)))
	    (is (finished?) t "Finished")
	    (is (top-stack last-state) expected-result
		      (format nil "result is expected to be ~a" expected-result)))))


      ;; we need tests to make sure this is using 32 bit signed integers that can overflow
      (script-test-arithmetic '((:OP_1 :OP_1 :OP_ADD)) 2)
      ;; which one is correct
      (script-test-arithmetic '((:OP_1 :OP_2 :OP_SUB)) 1)
      (script-test-arithmetic '((:OP_1 :OP_1ADD)) 2)
      (script-test-arithmetic '((:OP_2 :OP_1SUB)) 1)
      (script-test-arithmetic '((:OP_2 :OP_2MUL)) 4)
      (script-test-arithmetic '((:OP_2 :OP_2DIV)) 1)
      (script-test-arithmetic '((:OP_1 :OP_NEGATE)) -1)
      (script-test-arithmetic '((:OP_1 :OP_NEGATE :OP_ABS)) 1)
      (script-test-arithmetic '((:OP_1 :OP_NOT)) 0)
      (script-test-arithmetic '((:OP_2 :OP_NOT)) 0)
      (script-test-arithmetic '((:OP_0 :OP_NOT)) 1)
      (script-test-arithmetic '((:OP_1 :OP_0NOTEQUAL)) 1)
      (script-test-arithmetic '((:OP_2 :OP_0NOTEQUAL)) 1)
      (script-test-arithmetic '((:OP_0 :OP_0NOTEQUAL)) 0)
      (script-test-arithmetic '((:OP_2 :OP_2 :OP_MUL)) 4)
      (script-test-arithmetic '((:OP_2 :OP_2 :OP_DIV)) 1)
      (script-test-arithmetic '((:OP_2 :OP_3 :OP_MOD)) 1)
      (script-test-arithmetic '((:OP_1 :OP_1 :OP_LSHIFT)) 2)
      (script-test-arithmetic '((:OP_2 :OP_1 :OP_LSHIFT)) 4)
      (script-test-arithmetic '((:OP_1 :OP_1 :OP_RSHIFT)) 0)
      (script-test-arithmetic '((:OP_1 :OP_2 :OP_RSHIFT)) 1)
      (script-test-arithmetic '((:OP_1 :OP_1 :OP_BOOLAND)) 1)
      (script-test-arithmetic '((:OP_0 :OP_0 :OP_BOOLAND)) 0)
      (script-test-arithmetic '((:OP_1 :OP_2 :OP_BOOLAND)) 1)
      (script-test-arithmetic '((:OP_1 :OP_1 :OP_BOOLOR)) 1)
      (script-test-arithmetic '((:OP_1 :OP_0 :OP_BOOLOR)) 1)
      (script-test-arithmetic '((:OP_0 :OP_1 :OP_BOOLOR)) 1)
      (script-test-arithmetic '((:OP_0 :OP_0 :OP_BOOLOR)) 0)
      (script-test-arithmetic '((:OP_1 :OP_1 :OP_NUMEQUAL)) 1)
      (script-test-arithmetic '((:OP_1 :OP_2 :OP_NUMEQUAL)) 0)
      ;; numequalverify ??
      (script-test-arithmetic '((:OP_1 :OP_1 :OP_NUMNOTEQUAL)) 0)
      (script-test-arithmetic '((:OP_1 :OP_2 :OP_NUMNOTEQUAL)) 1)
      (script-test-arithmetic '((:OP_1 :OP_2 :OP_LESSTHAN)) 0)
      (script-test-arithmetic '((:OP_2 :OP_2 :OP_LESSTHAN)) 0)
      (script-test-arithmetic '((:OP_3 :OP_2 :OP_LESSTHAN)) 1)
      (script-test-arithmetic '((:OP_1 :OP_2 :OP_GREATERTHAN)) 1)
      (script-test-arithmetic '((:OP_2 :OP_2 :OP_GREATERTHAN)) 0)
      (script-test-arithmetic '((:OP_3 :OP_2 :OP_GREATERTHAN)) 0)
      (script-test-arithmetic '((:OP_1 :OP_2 :OP_LESSTHANOREQUAL)) 0)
      (script-test-arithmetic '((:OP_2 :OP_2 :OP_LESSTHANOREQUAL)) 1)
      (script-test-arithmetic '((:OP_3 :OP_2 :OP_LESSTHANOREQUAL)) 1)
      (script-test-arithmetic '((:OP_1 :OP_2 :OP_GREATERTHANOREQUAL)) 1)
      (script-test-arithmetic '((:OP_2 :OP_2 :OP_GREATERTHANOREQUAL)) 1)
      (script-test-arithmetic '((:OP_3 :OP_2 :OP_GREATERTHANOREQUAL)) 0)
      (script-test-arithmetic '((:OP_1 :OP_1 :OP_MIN)) 1)
      (script-test-arithmetic '((:OP_1 :OP_2 :OP_MIN)) 1)
      (script-test-arithmetic '((:OP_2 :OP_1 :OP_MIN)) 1)
      (script-test-arithmetic '((:OP_1 :OP_1 :OP_MAX)) 1)
      (script-test-arithmetic '((:OP_2 :OP_1 :OP_MAX)) 2)
      (script-test-arithmetic '((:OP_1 :OP_2 :OP_MAX)) 2)
      (script-test-arithmetic '((:OP_5 :OP_3 :OP_4 :OP_WITHIN)) 1)
      (script-test-arithmetic '((:OP_5 :OP_3 :OP_6 :OP_WITHIN)) 0)
      (script-test-arithmetic '((:OP_5 :OP_3 :OP_3 :OP_WITHIN)) 1)
      (script-test-arithmetic '((:OP_5 :OP_3 :OP_5 :OP_WITHIN)) 0))
    
    (subtest "disabled operations"
      
      (defun is-disabled (script)
	(subtest (format nil "Script: ~a" script))
	(is-condition (execute (list script)) 'disabled-operation "throws 'disabled-condition on operation")
	(handler-bind ((disabled-operation #'continue))
	  (ok (execute (list script)) "executed with continue")))

      (is-disabled '(:OP_2 :OP_2MUL))
      (is-disabled '(:OP_2 :OP_2DIV))
      (is-disabled '(:OP_2 :OP_2 :OP_MUL))
      (is-disabled '(:OP_2 :OP_2 :OP_DIV))
      (is-disabled '(:OP_3 :OP_2 :OP_MOD))
      (is-disabled '(:OP_1 :OP_1 :OP_LSHIFT))
      (is-disabled '(:OP_1 :OP_1 :OP_RSHIFT)))))


