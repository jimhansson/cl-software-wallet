
(in-package :cl-software-wallet/tests)

(prove:subtest "bech32-tests"

  (prove:subtest "bech32-polymod"
    (prove:plan 2)
    (prove:is (bech32-polymod #(3 2 1)) 35905)
    (prove:is (bech32-polymod #(33 126 127)) 3007)
    (prove:finalize))
  
  (prove:subtest "bech32-hrp-expand"
    (prove:plan 3)
    (prove:is (bech32-hrp-expand "foo") #(3 3 3 0 6 15 15) :test #'equalp)
    (prove:is (bech32-hrp-expand "bc") #(3 3 0 2 3) :test #'equalp)
    (prove:is (bech32-hrp-expand "tb") #(3 3 0 20 2) :test #'equalp)
    (prove:finalize))
  
  (prove:subtest "test-vectors-bip173"

    ;; this subtest mirrors the order of the tests in BIP 173, that to
    ;; make it simple to know what parts we are adhering to and not

    ;; some simplified test wrappers so we get nice output
    (flet (
	   ;; test that checks that the human readable part is within specification
	   (invalid-hrp-test (char data)
	       (prove:is-error (bech32-decode (concatenate 'string `(octets-to-string #(,char)) data)) 'error
			       (concatenate 'string data " : Invalid HRP")))
	   
	   ;; checks that an error is signaled when trying to decode
	   ;; something that does not have a human readable part 
	   (empty-hrp-test (data)
	     (prove:is-error (bech32-decode data) 'error (concatenate 'string data " : Empty HRP")))

	   ;; Checks that we give error when we should
	   (is-error-test (data desc)
	     (prove:is-error (bech32-decode data) 'error
			     (concatenate 'string data " : " desc)))

	   ;; checks that given a wellformed input we give the correct
	   ;; output in the form of a address.
	   (valid-address-test (address expected-data expected-net)
	     (prove:diag address)
	     (let* ((result (bech32-decode address))
		    (net (car result))
		    (data (ironclad:byte-array-to-hex-string (cadr result)))
		    (witness (caddr result))
		    (checksum (cadddr result)))
	       (declare (ignorable witness checksum))
	       (prove:is net expected-net)
	       (prove:is data (subseq expected-data 4) :test #'equalp)
	       ;;(prove:is witness nil)
	       ;;(prove:is checksum nil)
	       )))

      
      (prove:subtest "invalid-addresses"
	(prove:plan 12)
	
	(invalid-hrp-test #x20 "1nwldj5")
	(invalid-hrp-test #x7F "1axkwrx")
	(invalid-hrp-test #x80 "1eym55h")
	(is-error-test "an84characterslonghumanreadablepartthatcontainsthenumber1andtheexcludedcharactersbio1569pvx" "To long string")
	(is-error-test "pzry9x0s0muk" "No seperator character in string")
	(empty-hrp-test "1pzry9x0s0muk")
	(is-error-test "x1b4n0q5v" "Invalid data character")
	(is-error-test "li1dgmt3" "To short checksum")
	(prove:is-error (bech32-decode (concatenate 'string "de1lg7wt" (octets-to-string #(#xFF)))) 'error "Invalid checksum character")
	(is-error-test "A1G7SGD8" "Checksum calculated with upercase form of HRP")
	(empty-hrp-test "10a06t8")
	(empty-hrp-test "1qzzfhee")
	(prove:finalize))

      (prove:subtest "valid-addresses"
	(prove:plan 12)
	(valid-address-test "bc1qw508d6qejxtdg4y5r3zarvary0c5xw7kv8f3t4"
			    "0014751e76e8199196d454941c45d1b3a323f1433bd6"
			    "bc")
	(valid-address-test "tb1qrp33g0q5c5txsp9arysrx4k6zdkfs4nce4xj0gdcccefvpysxf3q0sl5k7"
			    "00201863143c14c5166804bd19203356da136c985678cd4d27a1b8c6329604903262"
			    "tb")
	(valid-address-test "bc1pw508d6qejxtdg4y5r3zarvary0c5xw7kw508d6qejxtdg4y5r3zarvary0c5xw7k7grplx"
			    "5128751e76e8199196d454941c45d1b3a323f1433bd6751e76e8199196d454941c45d1b3a323f1433bd6"
			    "bc")
	(valid-address-test "bc1sw50qa3jx3s"
			    "6002751e"
			    "bc")
	(valid-address-test "bc1zw508d6qejxtdg4y5r3zarvaryvg6kdaj"
			    "5210751e76e8199196d454941c45d1b3a323"
			    "bc")
	(valid-address-test "tb1qqqqqp399et2xygdj5xreqhjjvcmzhxw4aywxecjdzew6hylgvsesrxh6hy"
			    "0020000000c4a5cad46221b2a187905e5266362b99d5e91c6ce24d165dab93e86433"
			    "tb")
	(prove:finalize))
      
      (prove:subtest "invalid-seqwit-addresses"
	(prove:plan 10)
	(invalid-hrp-test #x74 "c1qw508d6qejxtdg4y5r3zarvary0c5xw7kg3g4ty") ; #x74 = t
	(is-error-test "bc1qw508d6qejxtdg4y5r3zarvary0c5xw7kv8f3t5" "Invalid checksum")
	(is-error-test "bc13w508d6qejxtdg4y5r3zarvary0c5xw7kn40wf2" "Invalid witness version")
	(is-error-test "bc1rw5uspcuh" "Invalid program length")
	(is-error-test "bc10w508d6qejxtdg4y5r3zarvary0c5xw7kw508d6qejxtdg4y5r3zarvary0c5xw7kw5rljs90" "Invalid program length")
	;; the one below is not failing for the right reason
	(is-error-test "BC1QR508D6QEJXTDG4Y5R3ZARVARYV98GJ9P" "Invalid program length for witness version 0")
	(is-error-test "tb1qrp33g0q5c5txsp9arysrx4k6zdkfs4nce4xj0gdcccefvpysxf3q0sL5k7" "Mixed case")
	(is-error-test "bc1zw508d6qejxtdg4y5r3zarvaryvqyzf3du" "Zero padding of more than 4 bits")
	(is-error-test "tb1qrp33g0q5c5txsp9arysrx4k6zdkfs4nce4xj0gdcccefvpysxf3pjxtptv" "Non-Zero padding in 8-to-5 conversion")
	(is-error-test "bc1gmk9yu" "Empty data section")
	(prove:finalize))))
  
  (prove:finalize))
