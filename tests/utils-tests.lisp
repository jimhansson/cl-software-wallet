
(in-package #:cl-software-wallet/tests)

(prove:subtest "utils"
  (prove:subtest "parse-hdkey"

    (prove:plan 4)
    (let ((hdkey (parse-hdkey "xprv9s21ZrQH143K2JF8RafpqtKiTbsbaxEeUaMnNHsm5o6wCW3z8ySyH4UxFVSfZ8n7ESu7fgir8imbZKLYVBxFPND1pniTZ81vKfd45EHKX73")))
      ;; had a bug where when parsing we would get a number 0 instead.
      ;; TODO add a test with a non-master key. so we can check the order of the bytes in
      ;; fingerprint, and depth.
      (prove:is (length (hdkey-master-fingerprint hdkey)) 4)
      (prove:is (hdkey-master-fingerprint hdkey) #(#x0 #x0 #x0 #x0)
		:test #'equalp)))

  (prove:subtest "base58-encode/decode"
    
    (let* ((test-case "xprv9s21ZrQH143K2JF8RafpqtKiTbsbaxEeUaMnNHsm5o6wCW3z8ySyH4UxFVSfZ8n7ESu7fgir8imbZKLYVBxFPND1pniTZ81vKfd45EHKX73")
	   (decoded (base58-decode test-case))
	   (encoded (base58-encode decoded)))
      (prove:is encoded test-case "testing that decoding and reencoding ends up with the same as we started")))

  (prove:subtest "publickey->address"
    
    ;; test-case from bip32.org
    (let* ((hdkey (parse-hdkey "xprv9s21ZrQH143K2JF8RafpqtKiTbsbaxEeUaMnNHsm5o6wCW3z8ySyH4UxFVSfZ8n7ESu7fgir8imbZKLYVBxFPND1pniTZ81vKfd45EHKX73"))
	   (pk (getpublickey hdkey)))
      (prove:is "023E4740D0BA639E28963F3476157B7CF2FB7C6FDF4254F97099CF8670B505EA59" (octets-to-hex-string pk))
      (prove:is "128RdrAkJDmqasgvfRf6MC5VcX4HKqH4mR" (publickey->address pk)))
    
    ;; test-case from https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses
    (let ((pk (ironclad:hex-string-to-byte-array "0250863ad64a87ae8a2fe83c1af1a8403cb53f53e486d8511dad8a04887e5b2352")))
      (prove:is "1PMycacnJaSqwwJqjawXBErnLsZ7RkXUAs" (publickey->address pk)))
    (prove:finalize))
  (prove:subtest "sha256sum functions"

    (let* ((data (make-byte-array 4))
	   (sum (sha256sum-calculate data))
	   (tot (sha256sum-append data)))
      (prove:is sum #(140 185 1 37) :test #'equalp)
      (prove:is tot (concatenate 'var-byte-array data sum) :test #'equalp)
      (prove:is (subseq tot 0 (length data)) data :test #'equalp))
    (let* ((data (make-byte-array 8))
	   (sum (sha256sum-calculate data))
	   (tot (sha256sum-append data)))
      (prove:is sum #(126 240 202 98) :test #'equalp)
      (prove:is tot (concatenate 'var-byte-array data sum) :test #'equalp)
      (prove:is (subseq tot 0 (length data)) data :test #'equalp))
    (let* ((data (make-byte-array 16))
	   (sum (sha256sum-calculate data))
	   (tot (sha256sum-append data)))
      (prove:is sum #(129 252 73 37) :test #'equalp)
      (prove:is tot (concatenate 'var-byte-array data sum) :test #'equalp)
      (prove:is (subseq tot 0 (length data)) data :test #'equalp)))
  
  (prove:subtest "serialize-compact-integer"
    ""
    (prove:plan 5)
    ;; this is a edge case in ironclads inplementation of integer-to-octets.
    (prove:is (serialize-compact-integer 0) '#(0) :test #'equalp)
    (prove:is (serialize-compact-integer 3) '#(3) :test #'equalp)
    (prove:is (serialize-compact-integer 255) '#(#xFD 255) :test #'equalp)
    (prove:is (serialize-compact-integer (* 256 256)) '#(#xFE 1 0 0) :test #'equalp)
    (prove:is (serialize-compact-integer (* 256 256 256 256)) '#(#xFF 1 0 0 0 0) :test #'equalp)
    (prove:finalize))

  (prove:subtest "read-compact-integer"
    (prove:plan 3)
    (flet ((byte-array (&rest rest)
	     (make-array (length rest)
			 :initial-contents rest
			 :element-type '(unsigned-byte 8))))
      (prove:is-values (read-compact-integer #(23)) '(23 1))
      (prove:is-values (read-compact-integer #(23 32)) '(23 1))
      (prove:is-values (read-compact-integer (byte-array #xFD #xFD #xFD)) '(#xFDFD 3))
      (prove:finalize)))

  (prove:subtest "serialize and deserialize-compact-integer"
    (prove:is (read-compact-integer (serialize-compact-integer 32000)) 32000))
  
  (prove:subtest "bech32-encode"
  (let ((test-case "bc1sw50qa3jx3s"))
    (prove:is (bech32-encode "bc" #(0 25 25 30)) "")
    (multiple-value-bind (hrp data foo checksum)
        (bech32-decode test-case)
      (format t "~a ~a ~a~%" data foo checksum)
      (prove:is hrp "bc")
      (prove:is (bech32-encode "bc" data) test-case)))))

