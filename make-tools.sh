touch cl-software-wallet.asd
rm -rf bin
mkdir bin
sbcl --eval "(asdf:operate :build-op :cl-software-wallet/simple-tool)"
cp util/simple-tool bin
rm util/simple-tool
sbcl --eval "(asdf:operate :build-op :cl-software-wallet/shamir-tool)"
cp util/shamir bin
rm util/shamir
