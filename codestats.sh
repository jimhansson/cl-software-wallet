rm codestats.txt
find src   -name '*.lisp' | xargs wc | tail -n1 >> codestats.txt
find tests -name '*.lisp' | xargs wc | tail -n1 >> codestats.txt
find util  -name '*.lisp' | xargs wc | tail -n1 >> codestats.txt
